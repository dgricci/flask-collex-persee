# Authors #

* Équipe projet promotion 2018-2019 filière 3ième année Ingénieurs ENSG et M2
  Technologies des Systèmes d'Information U-PEM (prototype initial) :
    * Antonin FONDÈRE ;
    * Corentin CRAPART ;
    * Erwan VIEGAS ;
    * Max PERMALNAICK ;
    * Sinda THAALBI ;
    * Tristan HARLING ;
    * Wiltold PODLEJSKI.
* Responsable pédagogique (refonte du code, ajout de Flask, tests,
  documentation) :
    * Didier RICHARD.

