#!/usr/bin/env python
# -*- coding: utf-8 -*-
from application import create_app, run_app
from dotenv import load_dotenv

load_dotenv('.env')
app = create_app()

# never used when flask run, so use python directly :
if __name__ == "__main__":
    run_app(app)

