#!/usr/bin/env python 
# -*- coding: utf-8 -*-
from flask import current_app, request, jsonify, redirect, render_template, make_response, copy_current_request_context, url_for
from application.main import bp
from application.errors.responses import error_response
from application.xmlrdf.rdfparser import RdfParser
from application.xmlrdf.xmltogeojson import concept_to_feature, id_from_uri, set_properties_i18n
from anytree import Node as TreeNode
from anytree.search import find
from anytree.exporter import JsonExporter
from anytree.exporter import DotExporter
from datetime import datetime
from rdflib import Namespace
from rdflib.namespace import RDF, SKOS, DC, DCTERMS
from xml.dom.minidom import Node, parseString
import asyncio
import json
import logging
import os
# Cf. http://python-requests.org/
import requests as web_client
import xml.dom
import xmltodict

unknown_thesorus_id = "Unknown thesausus identifier '{}'"
current_release = "{} has been released at {}"
a_release = "{} has a release at {}"
new_release_avail = "new release is available for {}"
new_release = "{} got new release at {}"
release_uptodate = "{} is up-to-date"
exception_tmpl = "{}"
headers = {
    "User-Agent" : "Inha/Invisu+ENSG"
}

OPENTHESO = Namespace("http://purl.org/umu/uneskos#")
GEO = Namespace("http://www.w3.org/2003/01/geo/wgs84_pos#")

def __get_thesorus(thid):
    """
    Retourne le thésaurus dont l'identifiant est fourni.

    :param thid: l'identifiant du thésaurus
    :type thid: str
    :return: le thésaurus ou None s'il n'est pas retrouvé
    :rtype: dict
    """
    if thid is None:
        return None

    thesorii = current_app.config["THESOS"]
    if thesorii is None or not thid in thesorii:
        return None

    return thesorii[thid]

def __get_proxies():
    """
    Retourne la configuration du ou des proxies.

    :return: la configuration demandée
    :rtype: str (un proxy) ou dict (plusieurs)
    """
    if "PROXIES" in current_app.config:
        return current_app.config["PROXIES"]
    return None

def __get_thesaurus_lastupdate(th):
    """
    Retourne la date de mise à jour du thésaurus via une requête sur le
    service RDF.
    :param th: le thésaurus
    :type th: dict
    :return: un message d'erreur en cas d'échec, une date sinon
    :rtype: str | datetime
    """
    try:
        url = "{}opentheso/api/info/lastupdate?theso={}".format(th["url"], th["th"])
        current_app.logger.debug("lastupdate url= {}".format(url))
        response = web_client.get(url, headers=headers, timeout=2, proxies=__get_proxies())
        response.raise_for_status()
        json_response = response.json()
        current_app.logger.debug("json {}".format(json_response))
        # still lastApdate in response ... changed on the 2nd of dec, 2019
        last_update = datetime.strptime(json_response["lastUpdate"],"%Y-%m-%d")
        return last_update
    except Exception as err:
        current_app.logger.debug("***** (1) exception encountered {} *****".format(err))
        return exception_tmpl.format(err)

def __preload_concepts(th, xmlstr):
    """
    Charge l'ensemble des "skos:Concept" en mémoire pour deux raisons:

    - il est impossible de les récupérer entièrement par SPARQL, en
      particulier les attributs RDF/XML ;
    - on est obligé de lire via le gazetier les données pour chaque concept
      (une requête HTTPS par "skos:Concept" à lire).

    :param th: le thésausus à mettre en cache
    :type th: dict
    :param xmlstr: le contenu du RDF/XML du gazetier
    :param xmlstr: str
    :return: True si les objets ont été chargés, une exception ValueError sinon
    :rtype: bool
    """
    try:
        xdoc = parseString(xmlstr)
        concepts = {
            'HDL2ID':{},
            'ID2HDL':{},
            'OBJECT':{}
        }
        for xn in xdoc.getElementsByTagNameNS(str(RDF), "Description"):
            xstr = xn.toxml()
            elems = xn.getElementsByTagNameNS(str(RDF), "type")
            if not elems or elems.length != 1:
                raise ValueError("No 'rdf:type' found in {}".format(xstr))
            attr = elems.item(0).getAttributeNodeNS(str(RDF), "resource")
            if not attr or attr.value != str(SKOS)+"Concept":
                raise ValueError("Expected '{}Concept' for 'rdf:type' in {}".format(str(SKOS), xstr))
            # handle
            attr = xn.getAttributeNodeNS(str(RDF),"about")
            if not attr:
                raise ValueError("No 'rdf:about' on 'rdf:Description' found in {}".format(xstr))
            uri = attr.value
            elems = xn.getElementsByTagNameNS(str(DCTERMS), "identifier")
            if not elems or elems.length !=1:
                raise ValueError("No 'dcterms:identifier' in 'rdf:Description' found in {}".format(xstr))
            telem = elems.item(0).firstChild
            if telem.nodeType != Node.TEXT_NODE:
                raise ValueError("Expected 'dcterms:identifier' to be a text node in {}".format(xstr))
            cid = telem.data
            # make complete RDF/XML string
            xdstr = "{}\n<rdf:RDF {} {} {} {} {} {}>\n{}\n</rdf:RDF>\n".format(
                    '<?xml version="1.0" encoding="UTF-8"?>',
                    'xmlns:rdf="'+str(RDF)+'"',
                    'xmlns:opentheso="'+str(OPENTHESO)+'"',
                    'xmlns:skos="'+str(SKOS)+'"',
                    'xmlns:dc="'+str(DC)+'"',
                    'xmlns:dcterms="'+str(DCTERMS)+'"',
                    'xmlns:geo="'+str(GEO)+'"',
                    xstr
            )
            o = xmltodict.parse(xdstr, process_namespaces=True, namespaces={
                str(RDF)       : 'rdf',
                str(SKOS)      : 'skos',
                str(OPENTHESO) : 'opentheso',
                str(DC)        : 'dc',
                str(DCTERMS)   : 'terms',
                str(GEO)       : 'geo'
            })
            #current_app.logger.debug("uri={} cid={}".format(uri, cid))
            concepts['HDL2ID'][uri] = cid
            concepts['ID2HDL'][cid] = uri
            concepts['OBJECT'][uri] = o

        current_app.config["CACHES"][th["th"]] = { "concepts" : concepts }
        if bool(concepts):
            return True
        raise ValueError("No 'rdf:Description' found in {}".format(th["source"]))
    except Exception as err:
        current_app.logger.debug("***** (2) exception encountered {} *****".format(err))
        raise

def __load_thesaurus(th):
    """
    Charge un thésaurus et le sauvegarde dans un fichier xml utilisé comme
    cache pour de subséquentes requêtes.

    :param th: le thésausus à mettre en cache
    :type th: dict
    :return: un message d'erreur en cas d'échec, une date sinon
    :rtype: str | datetime
    """
    try:
        # xml:
        url = "{}opentheso/api/all/group?id={}&theso={}".format(th["url"], th["idg"], th["th"])
        current_app.logger.debug("all url= {}".format(url))
        response = web_client.get(url, headers=headers, timeout=5, proxies=__get_proxies())
        response.raise_for_status()
        with open(th["source"], "w") as fxml:
            fxml.write(response.text)

        # load concepts
        __preload_concepts(th, response.text)

        # date:
        if th["last-update"] is None:
            return __get_thesaurus_lastupdate(th)
        else:
            return th["last-update"]

    except Exception as err:
        current_app.logger.debug("***** (3) exception encountered {} *****".format(err))
        return exception_tmpl.format(err)

def __save_thesaurus_config():
    """
    Sérialize la configuration des thésaurus en cache.
    """
    thesos = current_app.config["THESOS"]
    ths = {}
    for th in thesos:
        # as datetime is not jsonisable and source is not serialized, make
        # a deep copy of the dictionnary :
        theso = thesos[th].copy()
        if theso["last-update"] is None:
            theso["last-update"] = ""
        else:
            theso["last-update"] = theso["last-update"].strftime('%Y-%m-%d')
        # remove unecessary keys :
        try:
            del theso["source"]
        except KeyError:
            pass
        try:
            del theso["parser"]
        except KeyError:
            pass

        ths[th] = theso

    with open(current_app.config["CACHE_DICT"], "w") as f:
        js = json.dumps(ths, indent=2)
        f.write(js)

async def __once_job():
    """
    Exécute une tâche une seule fois.
    On charge les thésaurus stockés en cache dans la configuration.
    """
    current_app.logger.debug("load {}".format(current_app.config["CACHE_DICT"]))
    try:
        current_app.config["THESOS"] = {}
        current_app.config["CACHES"] = {}
        with open(current_app.config["CACHE_DICT"], "r") as f:
            ths = json.load(f)

        for th in ths:
            theso = ths[th]
            # set last-update date:
            try:
                theso["last-update"] = datetime.strptime(theso["last-update"],"%Y-%m-%d")
                current_app.logger.debug(current_release.format(theso["th"], theso["last-update"]))
            except Exception as err:
                theso["last-update"] = None
            # build xml source file name for this thesaurus:
            theso["source"] = os.path.join(current_app.config["CACHE_DIR"], theso["idg"]+"_"+theso["th"]+".xml")
            # if xml file does not exist, last-update must be unknown:
            if not os.path.isfile(theso["source"]):
                theso["last-update"] = None

            if theso["last-update"] is None:
                mOd = __load_thesaurus(theso)
                if isinstance(mOd, str):
                    current_app.logger.error(mOd)
                else:
                    theso["last-update"] = mOd
                    current_app.logger.debug(a_release.format(theso["th"], mOd.strftime('%Y-%m-%d')))
            else:
                d = __get_thesaurus_lastupdate(theso)
                if isinstance(d, datetime) and theso['last-update'] < d:
                    current_app.logger.debug(new_release_avail.format(theso["th"]))
                    mOd = __load_thesaurus(theso)
                    if isinstance(mOd, str):
                        current_app.logger.error(mOd)
                    else:
                        theso["last-update"] = d
                        current_app.logger.debug(new_release.format(theso["th"], d.strftime('%Y-%m-%d')))
                else:
                    current_app.logger.debug("reading {}".format(theso["source"]))
                    with open(theso["source"], "r") as f:
                        xmlstr = f.read()
                        __preload_concepts(theso, xmlstr)
                    current_app.logger.debug(release_uptodate.format(theso["th"]))

            current_app.config["THESOS"][th] = theso
            #current_app.logger.debug("CACHES[{}](HDL2ID)={}".format(th, current_app.config["CACHES"][th]["concepts"]["HDL2ID"]))
            #current_app.logger.debug("CACHES[{}](ID2HDL)={}".format(th, current_app.config["CACHES"][th]["concepts"]["ID2HDL"]))

        __save_thesaurus_config()

        current_app.logger.info("THESOS={}".format(current_app.config["THESOS"]))
        return current_app.config["THESOS"]
    except Exception as err:
        current_app.logger.error(exception_tmpl.format(err))
        current_app.config["THESOS"] = None
        return None

@bp.before_app_first_request
def activate_job():
    """
    Initialise l'application en chargeant les thésaurii.
    """
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    loop.run_until_complete(__once_job())
    loop.close()

@bp.before_app_request
def prepare(): 
    """
    """
    if current_app.config["LOGGING_LEVEL"] == logging.DEBUG:
        current_app.logger.debug("method : {}".format(request.method))
        current_app.logger.debug("url : {}".format(request.url))
        current_app.logger.debug("base_url : {}".format(request.base_url))
        current_app.logger.debug("url_root : {}".format(request.url_root))
        current_app.logger.debug("url_rule : {}".format(request.url_rule))
        current_app.logger.debug("host_url : {}".format(request.host_url))
        current_app.logger.debug("host : {}".format(request.host))
        current_app.logger.debug("script_root : {}".format(request.script_root))
        current_app.logger.debug("path : {}".format(request.path))
        current_app.logger.debug("full_path : {}".format(request.full_path))
        current_app.logger.debug("args : {}".format(request.args))
        for h, v in request.headers:
            current_app.logger.debug("{} : {}".format(h, v))

@bp.route("/")
def banner():
    """
    Renvoie une bannière pour une requête sur '/'.

    :return: une réponse en JSON:
             200, {"banner":"Collex-Persée - accès au thésaurus"}
    """
    return jsonify({"banner":"Collex-Persée - accès au thésaurus"}), 200

@bp.route("/ping")
def ping():
    """
    Un chemin '/ping' pour tester si l'application fonctionne.

    :return: une réponse en JSON:
             200 {"ping":"pong"}
    """
    return jsonify({"ping":"pong"}), 200

@bp.route("/gazeteers")
def get_gazeteers_metadata():
    """
    Retourne les thésaurus accessibles via cette application.

    :return: une réponse en JSON:
             200 [{
                "idg": "identifiant du gazetier",
                "thid": "identifiant du thésaurus",
                "name": "nom du gazetier",
                "url": "point d'accès OpenTheso du gazetier",
                "last-update": "date de mise-à-jour dans l'application YYYY-MM-JJ"
             },{
                ...
             }]
    """
    jresp = []
    for th in current_app.config["THESOS"]:
        th_metadata = {
            'idg'        : current_app.config["THESOS"][th]['idg'],
            'thid'       : current_app.config["THESOS"][th]['th'],
            'name'       : current_app.config["THESOS"][th]['name'],
            'url'        : current_app.config["THESOS"][th]['url'],
            'last-update': current_app.config["THESOS"][th]['last-update'].strftime('%Y-%m-%d')
        }
        jresp.append(th_metadata)

    return jsonify(jresp), 200

@bp.route("/checknup/<thid>", methods = ['GET'])
def check_update(thid):
    """
    Vérifie si un thésaurus en cache et à jour et s'il ne l'est pas, le met à
    jour via '/checknup/<thid>.

    :param thid: identifiant du thésorus où effectuer la recherche.
    :type thid: str
    :return: une réponse en JSON:
             200 {<thid>:"updated","date":"%Y-%m-%d"} si une mise-à-jour a eu lieu à la date indiquée,
             200 {<thid>:"up-to-date","date":"%Y-%m-%d"} si aucune mise-à-jour depuis la date indiquée,
             404 et une exception en cas d'erreur
    """
    thesorus = __get_thesorus(thid)
    if thesorus is None:
        return error_response(404, unknown_thesorus_id.format(thid or ''))

    current_app.logger.debug("{}={}".format(thid, thesorus))
    d = __get_thesaurus_lastupdate(thesorus)
    if isinstance(d, datetime) and thesorus['last-update'] < d:
        current_app.logger.debug(new_release_avail.format(thesorus["th"]))
        mOd = __load_thesaurus(thesorus)
        if isinstance(mOd, str):
            return error_response(404, mOd)
        thesorus["last-update"] = d
        sd = d.strftime('%Y-%m-%d')
        current_app.logger.debug(new_release.format(thesorus["th"], sd))
        __save_thesaurus_config()

        return jsonify({thid:"updated","date":sd}), 200

    sd = thesorus['last-update'].strftime('%Y-%m-%d')
    current_app.logger.debug(release_uptodate.format(sd))
    return jsonify({thid:"up-to-date","date":sd}), 200

def __get_concept_by_uri(thesorus, oid, lang=None):
    """
    Retrouve un "objet" par son identifiant dans le thésaurus.
    On peut ajouter le paramètre 'lang' pour n'avoir que les champs
    (propriétés) tagguées dans cette langue.

    :param thesorus: les métadonnées du thésorus chargées en mémoire
    :type thesorus: dict
    :param oid: l'identifiant de l'objet (``dcterms:identifier`` ou handle ``rdf:about``)
    :type oid: str
    :param lang: la langue filtrant les champs. Par défaut, None (aucun filtre).
    :type lang: str
    :return: un JSON contenant l'objet (lieu).
    :rtype: dict
    """
    try:
        #current_app.logger.debug("{} given".format(oid))
        concepts = current_app.config["CACHES"][thesorus["th"]]["concepts"]
        handles = concepts['HDL2ID']
        identifiers = concepts['ID2HDL']
        # search in cache :
        # oid is a dcterms:identifier
        if oid in identifiers:
            o = concepts['OBJECT'].get(identifiers.get(oid))
            loid = oid
        else:
            if oid in handles:
                # weird enough use str ...
                o = concepts['OBJECT'].get(oid)
                loid = handles.get(oid)
            else:
                o = None
                loid = oid

        if o is not None:
            #current_app.logger.debug("o={}".format(o))
            return o

        # not in cache (probably an error in rdf:RDF - check with /validate/<thid> ?)
        url = "{}opentheso/api/{}.{}.rdf".format(thesorus["url"], thesorus["th"], loid)
        current_app.logger.debug("concept get url= {}".format(url))
        response = web_client.get(url, headers=headers, timeout=5, proxies=__get_proxies())
        response.raise_for_status()
        o = xmltodict.parse(response.text, process_namespaces=True, namespaces={
            str(RDF)       : 'rdf',
            str(SKOS)      : 'skos',
            str(OPENTHESO) : 'opentheso',
            str(DC)        : 'dc',
            str(DCTERMS)   : 'terms',
            str(GEO)       : 'geo'
        })
        return o
    except Exception as err:
        current_app.logger.debug("***** (4) exception encountered {} *****".format(err))
        raise

def __from_uris_to_features(thesorus, uris):
    """
    Fabrique des ``feature`` à partir des URI des ``skos:Concept``

    :param thesorus: le thésausus contenant les objets à traduire
    :type th: dict
    :param uris: la liste des objets à traduire via leur URI (``rdf:about``)
    :type uris: list
    :return: le (Geo)JSON des objets
    :rtype: dict
    """
    try:
        #current_app.logger.debug("uris={}".format(uris))
        if len(uris) == 0:
            return {
                "type": "FeatureCollection",
                "features": []
            }

        geojson = {
            "type": "FeatureCollection",
            "bbox": [180,90,-180,-90],
            "features": []
        }
        places = geojson["features"]
        bbox = geojson["bbox"]
        for uri in uris:
            # FIXME: return feature in all languages ...
            #current_app.logger.debug("uri={} id={}".format(uri, id_from_uri(uri)))
            co = __get_concept_by_uri(thesorus, id_from_uri(uri), lang=None)
            #current_app.logger.debug("co={}".format(co))
            jf = concept_to_feature(co, lang=None)
            # change isA, related from handle to identifier:
            handles = current_app.config["CACHES"][thesorus["th"]]["concepts"]['HDL2ID']
            jf["properties"]["isA"] = ";".join([handles[u] for u in jf["properties"]["isA"].split(";")])
            if jf["properties"]["related"] != "":
                jf["properties"]["related"] = ";".join([handles[u] for u in jf["properties"]["related"].split(";")])
            #current_app.logger.debug("jf={}".format(jf))
            lon = jf["geometry"]["coordinates"][0]
            lat = jf["geometry"]["coordinates"][1]
            if lon < bbox[0]:
                bbox[0] = lon
            if lon > bbox[2]:
                bbox[2] = lon
            if lat < bbox[1]:
                bbox[1] = lat
            if lat > bbox[3]:
                bbox[3] = lat
            places.append(jf)

        return geojson
    except Exception as err:
        current_app.logger.debug("***** (5) exception encountered {} *****".format(err))
        raise

async def __save_to_geojson_file(th, data):
    """
    Build GeoJSON source file name for this thesaurus and save the features to
    that file.

    :param th: le thésausus contenant les objets à sauvegarder
    :type th: dict
    :param data: objets à sauvegarder
    :type data: json
    """
    geojson_source = os.path.join(current_app.config["CACHE_DIR"], th["idg"]+"_"+th["th"]+".geojson")
    current_app.logger.debug("writing {}".format(geojson_source))
    with open(geojson_source, "w") as fjson:
        fjson.write(json.dumps(data, indent=2))
    th["geojson-source"] = geojson_source

@bp.route("/search/<thid>", methods = ['GET', 'POST'])
def search(thid):
    """
    Recherche les lieux qui correspondent aux critères de recherche via
    '/search/<thid>?[directory=<value>&][keyword=<value>&][lon=<value>&lat=<value>&radius=<value>&][lang=(fr|en|iso|ala|ar|mul)&]'
    le premier critère 'directory' permet de chercher des types de lieux. La
    valeur spéciale "root" les sélectionne tous.
    le second critère 'keyword' permet de chercher un mot-clef dans les
    descriptions de lieu.
    les critères 'lon', 'lat', 'radius' permettent de chercher les lieux dans
    un cercle de centre 'lon', 'lat' et de rayon 'radius' (en kilomètres).
    Un critère 'lang' permet d'interroger selon une langue parmi
    (fr,en,iso,ala,ar,mul) ; par défaut, c'est le français (fr).

    :param thid: identifiant du thésorus où effectuer la recherche.
    :type thid: str
    :return: une chaîne GeoJSON contenant d'aucune à plusieurs lieux.
    :rtype: Response
    """
    thesorus = __get_thesorus(thid)
    if thesorus is None:
        return error_response(404, unknown_thesorus_id.format(thid or ''))

    try:
        a = request.values.get("lang") or "fr"
        d = request.values.get("directory")
        k = request.values.get("keyword") if d != "root" else None
        l = request.values.get("lon") if d != "root" else None
        if l is not None:
            l = float(l)
        p = request.values.get("lat") if d != "root" else None
        if p is not None:
            p = float(p)
        r = request.values.get("radius") if d != "root" else None
        if r is not None:
            r = float(r)
        current_app.logger.debug("source={} lang={} directory={} keyword={} [{}, {}]({})".format(thesorus["source"],a,d,k,l,p,r))
        if "parser" not in thesorus:
            thesorus["parser"] = RdfParser(thesorus["source"], thid, lang=a)
        else:
            if thesorus["parser"].identifier != thid:
                thesorus["parser"] = RdfParser(thesorus["source"], thid, lang=a)
        uris = thesorus["parser"].find_places(lang=a, directory=d, keyword=k, lon=l, lat=p, radius=r)
        # python >= 2.5 :
        current_app.logger.debug("{} feature{} found".format(len(uris), "s" if len(uris) > 1 else ""))
        # python < 2.5 :
        #current_app.logger.debug("{} feature{} found".format(len(uris), (lambda: 's', lambda: '')[len(uris) > 1]())

        geojson = __from_uris_to_features(thesorus, uris)

        if d == "root":
            loop = asyncio.new_event_loop()
            asyncio.set_event_loop(loop)
            loop.run_until_complete(asyncio.gather(__save_to_geojson_file(thesorus, geojson)))
            loop.close()

        return jsonify(geojson), 200
    except Exception as err :
        current_app.logger.debug("***** (6) exception encountered {} *****".format(err))
        return error_response(404, message=exception_tmpl.format(err))

#@bp.route("/gqrdf/<thid>", methods = ['POST'])
#def gqrdf(thid):
#    """
#    """
#    thesorus = __get_thesorus(thid)
#    if thesorus is None:
#        return error_response(404, unknown_thesorus_id.format(thid or ''))
#    if thesorus["last-update"] is None:
#        return error_response(404, "No data found for {}".format(thid))
#
#    proxies = __get_proxies()
#
#    try:
#        sparql_query = request.values.get("sqarqltext")
#        if sparql_query is None:
#            return error_response(404, "SPARQL query expected (use 'sqarqltext' parameter)")
#        res = thesorus["parser"].graph.query(sparql_query)
#        resp = make_response( res.serialize(format="json") )
#        resp.headers['Content-Type'] = 'application/json'
#        return resp, 200
#    except Exception as err :
#        current_app.logger.debug("***** (7) exception encountered {} *****".format(err))
#        return error_response(404, message=exception_tmpl.format(err))

@bp.route("/feature/<thid>/<oid>", methods = ['GET'])
def get_feature_by_id(thid, oid):
    """
    Retrouve un "objet" par son identifiant dans le thésaurus via
    '/feature/<thid>/<oid>[?lang=(fr|en|iso|ala|ar|mul)&]'.
    On peut ajouter le paramètre 'lang' pour n'avoir que les champs
    (propriétés) tagguées dans cette langue.

    :param thid: identifiant du thésorus où effectuer la recherche.
    :type thid: str
    :param oid: identifiant de l'objet à retrouver.
    :type oid: str
    :return: une chaîne GeoJSON contenant l'objet (lieu).
    :rtype: Response
    """
    thesorus = __get_thesorus(thid)
    if thesorus is None:
        return error_response(404, unknown_thesorus_id.format(thid or ''))
    if thesorus["last-update"] is None:
        return error_response(404, "No data found for {}".format(thid))

    try:
        l = request.values.get("lang")
        c = __get_concept_by_uri(thesorus, oid, lang=l)
        f = concept_to_feature(c, lang=l)
        # change isA, related from handle to identifier:
        handles = current_app.config["CACHES"][thesorus["th"]]["concepts"]['HDL2ID']
        f["properties"]["isA"] = ";".join([handles[str(u)] for u in f["properties"]["isA"].split(";")])
        if f["properties"]["related"] != "":
            f["properties"]["related"] = ";".join([handles[str(u)] for u in f["properties"]["related"].split(";")])
        return jsonify(f), 200
    except Exception as err :
        current_app.logger.debug("***** (8) exception encountered {} *****".format(err))
        return error_response(404, message=exception_tmpl.format(err))

@bp.route("/concepts/<thid>", methods = ['GET'])
def get_all_concepts(thid):
    """
    Retourne l'arbre des concepts sémantiques du thésaurus via
    '/concepts/<thid>'.

    :param thid: identifiant du thésorus où effectuer la recherche.
    :type thid: str
    :return: une chaîne GeoJSON contenant l'arbre qui pourra être dé-sérialisé
             via anytree.importer.jsonimporter.JsonImporter
    :rtype: Response
    """
    thesorus = __get_thesorus(thid)
    if thesorus is None:
        return error_response(404, unknown_thesorus_id.format(thid or ''))
    if thesorus["last-update"] is None:
        return error_response(404, "No data found for {}".format(thid))

    if "parser" not in thesorus:
        thesorus["parser"] = RdfParser(thesorus["source"], thid)
    else:
        if thesorus["parser"].identifier != thid:
            thesorus["parser"] = RdfParser(thesorus["source"], thid)

    try:
        concepts = thesorus["parser"].get_categories()
        #current_app.logger.debug("{}={}".format(thid, concepts))
        for k in concepts:
            acc = concepts[k]
            #current_app.logger.debug("tree[{}]={}".format(k, acc))
            for cc in acc["children"]:
                #current_app.logger.debug("cc={}".format(cc))
                c = __get_concept_by_uri(thesorus, cc["hdl"], lang=None)
                #current_app.logger.debug("c={}".format(c))
                ac = c["rdf:RDF"]["rdf:Description"]
                #current_app.logger.debug("ac={}".format(ac))
                set_properties_i18n(cc["label"], "label", ac, "skos:prefLabel", None)
        #current_app.logger.debug("{}={}".format(thid, concepts))

        # build tree
        tree_concepts = TreeNode(thesorus["idg"])
        handles = current_app.config["CACHES"][thesorus["th"]]["concepts"]['HDL2ID']
        # create parents :
        for k in concepts[thid]["children"]:
            #current_app.logger.debug("add to {} : {}".format(thesorus["idg"], k["hdl"]))
            # handle -> id because TreeNode path use /
            parent = TreeNode(handles[k["hdl"]], parent= tree_concepts, label=k["label"])
        del concepts[thesorus["th"]]
        while bool(concepts):
            to_del = []
            for k in concepts:
                acc = concepts[k]
                # handle -> id because TreeNode path use /
                node = find(tree_concepts, lambda node: node.name == handles[acc["hdl"]])
                if node is not None:
                    # found, add children :
                    for cc in acc["children"]:
                        # handle -> id because TreeNode path use /
                        #current_app.logger.debug("add to {} : {}".format(node.path, cc))
                        child = TreeNode(handles[cc["hdl"]], parent= node, label=cc["label"])
                    to_del.append(k)
                else:
                    # parent not found
                    pass
            for k in to_del:
                del concepts[k]

        if current_app.config["LOGGING_LEVEL"] == logging.DEBUG:
            def attrfunc(n):
                a = n.__dict__.get("label")
                if a is not None and "label:fr" in a:
                    return 'label="%s"' % a["label:fr"]
                else:
                    return 'label="%s"' % n.name
            dotfile_name = os.path.abspath(os.path.join(current_app.config["BASE_DIR"],"cache","{}_{}.dot".format(thesorus["idg"], thesorus["th"])))
            current_app.logger.debug("generating dot for graphiz : {}".format(dotfile_name))
            DotExporter(tree_concepts, options=["ranksep=2.0;", "nodesep=0.4;"], nodeattrfunc=attrfunc).to_dotfile(dotfile_name)

        resp = make_response( JsonExporter(sort_keys=True).export(tree_concepts) )
        resp.headers['Content-Type'] = 'application/json'
        return resp, 200
    except Exception as err :
        current_app.logger.debug("***** (9) exception encountered {} *****".format(err))
        return error_response(404, message=exception_tmpl.format(err))

@bp.route("/validate/<thid>", methods = [ 'GET' ])
def validate(thid):
    """
    Vérifie si tous les concepts du gazettier sont correctement référencés via
    '/validate/<thid>'.

    :param thid: identifiant du thésorus où effectuer la recherche.
    :type thid: str
    :return: une chaîne JSON contenant la liste des concepts référencés, mais
             non trouvés ('ref_concepts') et la liste des concepts sans
             géométrie ('no_geom').
    :rtype: Response
    """
    thesorus = __get_thesorus(thid)
    if thesorus is None:
        return error_response(404, unknown_thesorus_id.format(thid or ''))
    if thesorus["last-update"] is None:
        return error_response(404, "No data found for {}".format(thid))

    if "parser" not in thesorus:
        thesorus["parser"] = RdfParser(thesorus["source"], thid)
    else:
        if thesorus["parser"].identifier != thid:
            thesorus["parser"] = RdfParser(thesorus["source"], thid)

    concepts = thesorus["parser"].check_store()
    return jsonify(concepts), 200

@bp.route("/geojson/<thid>", methods = [ 'GET' ])
def get_all_features_geojson(thid):
    """
    Retourne les données en GeoJSON correspondant aux ``skos:Concept``
    présentant des coordonnées géographiques (``geo:long``, ``geo:lat``).
    Un critère 'lang' permet d'interroger selon une langue parmi
    (fr,en,iso,ala,ar,mul) ; par défaut, c'est le français (fr).

    :param thid: identifiant du thésorus où effectuer la recherche.
    :type thid: str
    :return: une chaîne GeoJSON contenant les objets.
    :rtype: Response
    """
    thesorus = __get_thesorus(thid)
    if thesorus is None:
        return error_response(404, unknown_thesorus_id.format(thid or ''))
    if thesorus["last-update"] is None:
        return error_response(404, "No data found for {}".format(thid))

    try:
        if "geojson-source" not in thesorus:
            a = request.values.get("lang") or "fr"
            # pas encore sauvegardé, on le calcule :
            if "parser" not in thesorus:
                thesorus["parser"] = RdfParser(thesorus["source"], thid, lang=a)
            else:
                if thesorus["parser"].identifier != thid:
                    thesorus["parser"] = RdfParser(thesorus["source"], thid, lang=a)
            uris = thesorus["parser"].find_places(lang=a, directory="root", keyword=None, lon=None, lat=None, radius=None)
            # python >= 2.5 :
            current_app.logger.debug("{} feature{} found".format(len(uris), "s" if len(uris) > 1 else ""))
            # python < 2.5 :
            #current_app.logger.debug("{} feature{} found".format(len(uris), (lambda: 's', lambda: '')[len(uris) > 1]())

            geojson = __from_uris_to_features(thesorus, uris)

            loop = asyncio.new_event_loop()
            asyncio.set_event_loop(loop)
            loop.run_until_complete(asyncio.gather(__save_to_geojson_file(thesorus, geojson)))
            loop.close()

            return jsonify(geojson), 200

        with open(thesorus["geojson-source"], "r") as f:
            resp = make_response( f.read() )
            resp.headers['Content-Type'] = 'application/geo+json'
            return resp, 200

    except Exception as err :
        current_app.logger.debug("***** (10) exception encountered {} *****".format(err))
        return error_response(404, message=exception_tmpl.format(err))

@bp.route("/perseeids/<thid>/<oid>", methods = [ 'GET' ])
def get_persee_ids(thid, oid):
    """
    Retourne les identifiants dans la perséïde d'un objet du thésaurus.
    Cette identifiant permet d'obtenir des informations complémentaires sur
    l'objet, informations absentes du thésaurus et gérées sur l'éco-système
    Persée.

    For instance, querying ``/persee/42/28775`` gives :

    [
        {
          "type":"main",
          "authority":"persee",
          "identifier":"853445",
          "uri":null
        },{
          "type":"external",
          "authority":"athar",
          "identifier":"28775",
          "uri":"https://hdl.handle.net/20.500.11942/crt1pv73yya17"
        }
    ]
   
    :param thid: identifiant du thésorus auquel appartient l'objet.
    :type thid: str
    :param oid: l'identifiant de l'objet (``dcterms:identifier`` ou handle ``rdf:about``)
    :type oid: str
    :return: une chaîne JSON contenant les identifiants Persée.
    :rtype: Response
    """
    thesorus = __get_thesorus(thid)
    if thesorus is None:
        return error_response(404, unknown_thesorus_id.format(thid or ''))
    if thesorus["last-update"] is None:
        return error_response(404, "No data found for {}".format(thid))

    try:
        c = __get_concept_by_uri(thesorus, oid)
        theso_id = c["rdf:RDF"]["rdf:Description"]["terms:identifier"]
        # Cf. http://info.persee.fr/wp-content/uploads/2017/12/PERSEE_webservice_autorit%C3%A9s.pdf
        url = "http://ws.persee.fr/authority/athar/{}/id".format(theso_id)
        current_app.logger.debug("persee athar url= {}".format(url))
        persee_headers = headers.copy()
        persee_headers['Accept'] = 'application/json'
        response = web_client.get(url, headers=persee_headers, timeout=2, proxies=__get_proxies())
        response.raise_for_status()
        json_response = response.json()
        current_app.logger.debug("json {}".format(json_response))

        # recordCount vaut 0 quant l'identifiant n'a pas été trouvé,
        # body vaut null dans ce cas.
        if json_response['header']['recordCount'] == 0:
            return error_response(404, "No Persée identifier found for {} of thesaurus {}".format(oid, thid))

        return jsonify(json_response['body']['records']), 200

    except Exception as err :
        current_app.logger.debug("***** (11) exception encountered {} *****".format(err))
        return error_response(404, message=exception_tmpl.format(err))

@bp.route("/apispecs", methods = [ 'GET' ])
def openapi_specs():
    try:
        params = {
            "tos"  : current_app.config["PUBLIC_HOST_URL"] + "/doc/terms/",
            "burl" : current_app.config["PUBLIC_HOST_URL"] + current_app.config["PUBLIC_BASE_APP"],
            "stage": current_app.config["ENV"] + " service",
            "root" : "",
            "doc"  : current_app.config["PUBLIC_HOST_URL"] + "/doc"
        }
        resp = make_response( render_template('service_api.json', prms=params) )
        resp.headers['Content-Type'] = 'application/json'
        return resp

    except Exception as err :
        current_app.logger.debug("***** (12) exception encountered {} *****".format(err))
        return error_response(404, message=exception_tmpl.format(err))

