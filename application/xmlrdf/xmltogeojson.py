#!/usr/bin/env python
# -*- coding: utf-8 -*-

langs = ["ala", "ar", "en", "fr", "iso", "mul"]
wlang = None

def __identity_cb(p):
    """
    Identity callback.

    :param p: a property
    :type p: str
    :return: the given parameter
    :rtype: str
    """
    return p

def id_from_uri(uri):
    """
    URI to ID callback.

    :param uri: a uniform ressource locator
    :type uri: str
    :return: an identifier extracted from the locator
    :rtype: str
    """
    return str(uri)

def __set_property_from_property_and_callback(c, n, s="\t", p="@rdf:resource", f=__identity_cb):
    """
    Transform a property from another.

    :param c: the XML-RDF concept
    :type c: dict
    :param n: the name of the XML-RDF property to transform
    :type n: str
    :param s: the separator to use when property is a list. Defaults to '\t'
    :type s: str
    :param p: the attribute where to get the property's value. Defaults to '@rdf:resource'
    :type p: str
    :param f: the callback to use for transforming the value
    :type f: function
    :return: the new computed property
    :rtype: str
    """
    if n in c:
        if isinstance(c[n], list):
            return s.join([f(r[p]) for r in c[n]])
        else:
            return f(c[n][p])
    return ""

def set_properties_i18n(p, a, c, n, lang, s="\t", sl="_"):
    """
    Compute a property in different languages.

    :param p: the properties where to add i18n properties
    :type p: dict
    :param a: the name of the i18n property to add
    :type a: str
    :param c: the XML-RDF concept
    :type c: dict
    :param n: the name of the XML-RDF property to insert
    :type n: str
    :param lang: languages to use. May be None for all languages, a language
                 (e.g. 'fr'), a list of languages (e.g. 'en', 'fr')
    :param lang: list
    :param s: the separator to use when property is a list. Defaults to '\t'
    :type s: str
    :param sl: the language separator to use. Defaults to '_'
    :type sl: str
    """
    if lang is None:
        wlangs = langs
    else:
        if isinstance(lang, list):
            wlangs = lang
        else:
            wlangs = [lang]

    for l in wlangs:
        p[a+sl+l] = ""
    if n in c:
        if isinstance(c[n], list):
            for l in wlangs:
                p[a+sl+l] = s.join([s.join(r["#text"].split("\n ")) for r in c[n] if r["@http://www.w3.org/XML/1998/namespace:lang"] == l])
        else:
            p[a+sl+c[n]["@http://www.w3.org/XML/1998/namespace:lang"]] = s.join(c[n]["#text"].split("\n "))
    return

def __set_property_from_uri(c, n, s="\t"):
    """
    Wrapper on '__set_property_from_property_and_callback' with callback set
    to 'id_from_uri'.

    :param c: the XML-RDF concept
    :type c: dict
    :param n: the name of the XML-RDF property to transform
    :type n: str
    :param s: the separator to use when property is a list. Defaults to '\t'
    :type s: str
    """
    return __set_property_from_property_and_callback(c, n, s=s, f=id_from_uri)

def concept_to_feature(jc, lang=None):
    """
    Transform an XML-RDF object into a GeoJSON object.

    :param jc: the XML-RDF object to transform
    :type jc: dict
    :param lang: the language to use for i18n properties. If None, use all
                 languages (["ala", "ar", "en", "fr", "iso", "mul"])
    :param lang: str
    """
    c = jc["rdf:RDF"]["rdf:Description"]
    if "geo:long" in c:
        lon = float(c["geo:long"]["#text"])
    else:
        lon = 0.0
    if "geo:lat" in c:
        lat = float(c["geo:lat"]["#text"])
    else:
        lat = 0.0
    f = {
        "type": "Feature",
        "geometry": {
            "type": "Point",
            "coordinates": [lon, lat]
        },
        "properties": {
            "id": c["terms:identifier"],
            "isA": "",
            "uri": str(c["@rdf:about"]),
            "related": "",
            "created": c["terms:created"]["#text"],
            "modified": c["terms:modified"]["#text"],
            "exactMatch": "",
            "relatedMatch": "",
            "broadMatch": ""
        }
    }
    p = f["properties"]
    p["isA"] = __set_property_from_uri(c, "skos:broader", s=";") # still an handle
    p["related"] = __set_property_from_uri(c, "skos:related", s=";")
    p["exactMatch"] = __set_property_from_property_and_callback(c, "skos:exactMatch")
    p["relatedMatch"] = __set_property_from_property_and_callback(c, "skos:relatedMatch")
    p["broadMatch"] = __set_property_from_property_and_callback(c, "skos:broadMatch")
    set_properties_i18n(p, "label", c, "skos:prefLabel", lang, s=";")
    set_properties_i18n(p, "altLabel", c, "skos:altLabel", lang)
    set_properties_i18n(p, "historyNote", c, "skos:historyNote", lang)
    set_properties_i18n(p, "editorialNote", c, "skos:editorialNote", lang)
    set_properties_i18n(p, "description", c, "skos:definition", lang, s=";")
    set_properties_i18n(p, "note", c, "skos:note", lang)
    set_properties_i18n(p, "scopeNote", c, "skos:scopeNote", lang)

    return f

