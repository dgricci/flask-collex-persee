#!/usr/bin/env python
# -*- coding: utf-8 -*-
import math
import os
from rdflib import Graph
# usefull when running this file (see __main__):
try:
    from application.xmlrdf.xmltogeojson import id_from_uri
except:
    def id_from_uri(u):
        return str(u)

class RdfParser:

    def __init__(self, source, identifier, lang="fr"):
        """
        Initialize une base RDF pour rdflib.

        :param source: un fichier XML servant de base RDF
        :type source: str
        :param identifier: identifiant attaché à la source
        :type identifier: str
        :param lang: la langue de sélection des données lors des recherches.
        Par défaut, c'est "fr".
        :type lang: str
        """
        if not os.path.isfile(source):
            raise ValueError('Cannot find thesaurus file {}'.format(source))
        self.identifier= identifier 
        self.graph = Graph(identifier=identifier)
        self.graph.parse(source, format='application/rdf+xml')
        self.lang = lang

    @property
    def identifier(self):
        """
        Retourne l'identifiant du thésaurus
        """
        return self.__identifier

    @identifier.setter
    def identifier(self, identifier):
        """
        Assigne l'identifiant du thésaurus
        """
        self.__identifier = identifier

    @property
    def graph(self):
        """
        Retourne le graphe rdflib.graph.Graph associé au thésaurus
        """
        return self.__graph

    @graph.setter
    def graph(self, graph):
        """
        Assigne le graphe rdflib.graph.Graph
        """
        self.__graph = graph

    def __get_from_directory(self, directory, lang):
        """
        Recherche par catégorie de lieux.

        :param directory: the place's category to search
        :type directory: str
        :return: the places list of this category
        :rtype: list
        """
        if directory == "root":
            return self.__get_all_places()
        l = lang if lang is not None else self.lang
        ths = directory.split(",")
        places = set()
        for th in ths:
            sparql = """
                SELECT DISTINCT ?uri
                WHERE {
                    ?uri a                  skos:Concept;
                         geo:lat            ?lat;
                         geo:long           ?long.
                    ?uri skos:broader+      ?b.
                    ?b   skos:prefLabel     ?blab.
                    FILTER (REGEX(?blab, "%s", "i") && LANG(?blab) = "%s")
                } GROUP BY ?uri""" % (
                    th,
                    l
                )
            thplaces = self.__process_sparql_get_places(sparql)
            if len(places) == 0:
                places = thplaces
            else:
                if len(thplaces) > 0:
                    for thplace in thplaces:
                        if not str(thplace) in places:
                            places.append(thplace)

        return places

    def __get_from_keyword(self, keyword, lang):
        """
        Query for the place with a label that contain the input comma separated keywords

        :param keyword: the keyword to search in labels
        :return the result of the request
        """
        l = lang if lang is not None else self.lang
        kws = keyword.split(",")
        places = set()
        for kw in kws:
            sparql = """
                SELECT DISTINCT ?uri
                WHERE {
                    {
                        ?uri a              skos:Concept;
                             geo:lat        ?lat;
                             geo:long       ?long;
                             skos:prefLabel ?label.
                     FILTER (REGEX(?label, "%s", "i") && LANG(?label) = "%s")
                    } UNION {
                        ?uri a              skos:Concept;
                             geo:lat        ?lat;
                             geo:long       ?long;
                             skos:altLabel+ ?label.
                        FILTER (BOUND(?label) && REGEX(?label, "%s", "i") && LANG(?label) = "%s")
                    }
                } GROUP BY ?uri""" % (
                    kw,
                    l,
                    kw,
                    l
                )
            kwplaces = self.__process_sparql_get_places(sparql)
            if len(places) == 0:
                places = kwplaces
            else:
                if len(kwplaces) > 0:
                    for kwplace in kwplaces:
                        if not str(kwplace) in places:
                            places.append(kwplace)

        return places

    def __get_from_location(self, lon, lat, radius):
        """
        Query for the place within the radius distance
        of the location of (longitude, latitude). There is
        an approximation of 0.5% on the compute of the distance.

        :param lon: the longitude of the starting location
        :param lat: the latitude of the starting location
        :param radius: the radius of the buffer in kilometres
        :return the result of the request
        """
        a = 111.1
        b = a * math.cos(lat * math.pi/180)
        a_carre = a**2
        b_carre = b**2
        radius_carre = radius**2
        sparql = """
            SELECT DISTINCT ?uri
            WHERE {
                ?uri a          skos:Concept;
                     geo:lat    ?lat;
                     geo:long   ?long.
                FILTER( ((?lat - %12.7f) * (?lat - %12.7f)) * %12.7f + ((?long - %12.7f) * (?long - %12.7f)) * %12.7f < %12.7f)
            } GROUP BY ?uri""" % (
                lat, lat, a_carre, lon, lon, b_carre, radius_carre
            )

        return self.__process_sparql_get_places(sparql)

    def __get_all_places(self):
        """
        Renvoie tous les lieux présents dans le thésaurus.

        :return: the places list of the entire content of the gazetteer
        :rtype: list
        """
        sparql = """
            SELECT DISTINCT ?uri
            WHERE {
                ?uri a          skos:Concept;
                     geo:lat    ?lat;
                     geo:long   ?long.
            } GROUP BY ?uri"""

        return self.__process_sparql_get_places(sparql)

    def find_places(self, lang=None, directory=None, keyword=None, lon=None, lat=None, radius=None):
        """
        The main request that takes all parameters (each can be 'None') to
        find places from gazetteer.

        :param directory: the category of places we looking for
        :type directory: str
        :param keyword: the keyword to search into labels
        :type keyword: str
        :param lon: the longitude of the center of the circle
        :type lon: float
        :param lat: the latitude of the center of the circle
        :type lat: float
        :param radius: the distance max from the middle of the circle
        :type radius: float
        :return: the list of places
        """
        places = set()

        if directory is not None:
            places = self.__get_from_directory(directory, lang)

        if keyword is not None:
            kwplaces = self.__get_from_keyword(keyword, lang)
            if len(places) == 0:
                places = kwplaces
            else:
                if len(kwplaces) == 0:
                    # no common place between searches
                    return set()

                labels = set(str(place) for place in places)
                places = [place for place in kwplaces if str(place) in labels]

        if radius is not None and lon is not None and lat is not None:
            llplaces = self.__get_from_location(lon, lat, radius)
            if len(places) == 0:
                places = llplaces
            else:
                if len(llplaces) == 0:
                    # no common place between searches
                    return set()

                labels = set(str(place) for place in places)
                places = [place for place in llplaces if str(place) in labels]

        return places

    def __process_sparql_get_places(self, query):
        """
        Exécute une requête SPARQL pour trouver des lieux.

        :param query: le texte de la requête SPARQL à exécuter.
        :type query: str
        :return: liste des lieux trouvés
        :rtype: list
        """
        res = self.graph.query(query)
        return self.to_places(res)

    def to_places(self, r):
        """
        Fabrique la liste des lieux retrouvés dans la réponse.
        :param r: the result of the SPARQL query (rdflib.graph.Graph.query()).
        :type r: rdflib.query.Result
        :return: la liste des lieux via leur URI 
        :rtype: list
        """
        uris = []
        for row in r.bindings:
            if "uri" in row:
                uris.append(str(row["uri"]))

        return uris

    def get_categories(self):
        """
        Calcule l'arbre sémantique du thésaurus.
        :return: the entire node tree with all directories
        :rtype: dict
        """
        sparql = """
            SELECT DISTINCT ?uri
            WHERE {
                ?uri    a   skos:Concept;
                FILTER NOT EXISTS {?uri geo:long        ?long.}.
                FILTER NOT EXISTS {?uri geo:lat         ?lat. }.
                FILTER NOT EXISTS {?uri skos:broader    ?d.   }.
            }
        """
        res = self.graph.query(sparql)
        dico = {}
        dico[self.identifier] = {
            "hdl": self.identifier, #mind it !
            "label":{},
            "children":[]
        }
        for row in res.bindings:
            if "uri" in row:
                uri = str(row["uri"])
                child = {
                    "hdl": uri,
                    "label": {}
                }
                dico[self.identifier]["children"].append(child)
                dico[uri] = {
                    "hdl": uri,
                    "label": {},
                    "children":[]
                }

        sparql = """
            SELECT DISTINCT ?puri
                            ?curi
            WHERE {
                ?puri   a              skos:Concept;
                        skos:narrower  ?curi.
                FILTER NOT EXISTS {?puri geo:long      ?long.}.
                FILTER NOT EXISTS {?puri geo:lat       ?lat. }.
                ?curi   a              skos:Concept.
                FILTER NOT EXISTS {?curi geo:long      ?long.}.
                FILTER NOT EXISTS {?curi geo:lat       ?lat. }.
                FILTER     EXISTS {?curi skos:narrower ?d.   }.
            }"""
        res = self.graph.query(sparql)
        for row in res.bindings:
            if "puri" in row and "curi" in row:
                puri = str(row["puri"])
                curi = str(row["curi"])
                cid = id_from_uri(curi)
                parent = id_from_uri(puri)
                child = {
                    "hdl"     : cid,
                    "label"   : {}
                }

                if parent not in dico:
                    dico[parent] = {
                        "hdl"     : parent,
                        "label"   : {},
                        "children": [child]
                    }
                else:
                    dico[parent]["children"].append(child)

                if cid not in dico:
                    dico[cid] = {
                        "hdl"      : cid,
                        "label"    : {},
                        "children" : []
                    }


        final_dico = {}
        # remove children without child :
        for k in dico:
            if len(dico[k]["children"]) != 0:
                final_dico[k] = dico[k]

        return final_dico

    def check_store(self):
        """
        Vérifie s'il existe des skos:Concept référencés dans skos:broader,
        skos:narrower, skos:related mais jamais instanciés.
        Vérifié s'il existe des skos:Concept sans skos:narrower et sans
        geo:lat, geo:long.
        """
        sparql = """
            SELECT DISTINCT ?uri
            WHERE {
                {
                    ?buri a skos:Concept.
                    ?buri skos:broader ?uri.
                    FILTER NOT EXISTS {?uri a skos:Concept.}
                } UNION {
                    ?buri a skos:Concept.
                    ?buri skos:narrower ?uri.
                    FILTER NOT EXISTS {?uri a skos:Concept.}
                } UNION {
                    ?buri a skos:Concept.
                    ?buri skos:related ?uri.
                    FILTER NOT EXISTS {?uri a skos:Concept.}
                }
            } GROUP BY ?uri"""
        res = self.graph.query(sparql)
        missing_concepts = []
        for row in res.bindings:
            if "uri" in row:
                missing_concepts.append(id_from_uri(str(row["uri"])))

        sparql = """
            SELECT DISTINCT ?uri
            WHERE {
                {
                    ?uri  a skos:Concept.
                    ?curi a skos:Concept.
                    ?curi skos:broader ?uri.
                    FILTER NOT EXISTS {?uri skos:narrower ?curi.}
                } UNION {
                    ?uri  a skos:Concept.
                    ?curi a skos:Concept.
                    ?curi skos:narrower ?uri.
                    FILTER NOT EXISTS {?uri skos:broader ?curi.}
                }
            } GROUP BY ?uri"""
        res = self.graph.query(sparql)
        missing_link = []
        for row in res.bindings:
            if "uri" in row:
                missing_link.append(id_from_uri(str(row["uri"])))

        sparql = """
            SELECT DISTINCT ?uri
            WHERE {
                ?uri a skos:Concept.
                FILTER NOT EXISTS {?uri skos:narrower ?curi.}
                FILTER NOT EXISTS {?uri geo:lat       ?lat.}
                FILTER NOT EXISTS {?uri geo:long      ?long.}
            } GROUP BY ?uri"""
        res = self.graph.query(sparql)
        missing_geometry = []
        for row in res.bindings:
            if "uri" in row:
                missing_geometry.append(id_from_uri(str(row["uri"])))

        return {
            "unref_concepts": missing_concepts,
            "missing_broader_or_narrower": missing_link,
            "no_geom": missing_geometry
        }

