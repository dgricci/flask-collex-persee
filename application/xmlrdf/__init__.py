#!/usr/bin/env python 
# -*- coding: utf-8 -*-
from flask import Blueprint

bp = Blueprint('xmlrdf', __name__)

from application.xmlrdf import rdfparser

