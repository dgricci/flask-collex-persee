from flask import current_app, make_response, render_template
from werkzeug.http import HTTP_STATUS_CODES

def error_response(status_code, locator=None, text=None, message=None):
    """
    Render in JSON the HTTP error.

    :Example:

    >>> {
    >>>     "Exception": {
    >>>         "code" : status_code,
    >>>         "locator" : "locator",
    >>>         "text" : "text",
    >>>         "message" : "message"
    >>>     }
    >>> }

    :param status_code: the HTTP code
    :type status_code: int
    :param locator: where the error was raised. Defaults to application's name
    :type locator: str
    :param text: error's text (cause). Defaults to "Non information"
    :type text: str
    :param message: error's message (reason). Defaults to "No tip"
    :type message: str
    :return: formatted response (See application.errors.responses.error_response)
    :rtype: Response
    """
    resp = make_response(
            render_template('exception.json', exceptionCode = status_code, locator=locator or current_app.name, exceptionText= text or "No information", message=message or "No tip")
    )
    resp.status_code = status_code
    resp.headers['Content-Type'] = 'application/json'
    return resp


def bad_request_response(message):
    """
    Wrapper for 500 HTTP error.

    :param message: the error's message
    :type message: str
    :return: formatted response (See application.errors.responses.error_response)
    :rtype: Response
    """
    return error_response(500, message=message)

