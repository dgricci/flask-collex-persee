#!/usr/bin/env python 
# -*- coding: utf-8 -*-
from application.errors import bp
from application.errors.responses import error_response, bad_request_response
    
@bp.app_errorhandler(404)
def not_found_error(error):
    """
    Handle for 404 HTTP code.

    :param error: the error message
    :type error: str
    :return: formatted response (See application.errors.responses.error_response)
    """
    return error_response(404,message=error)

@bp.app_errorhandler(500)
def internal_error(error):
    """
    Handle for 500 HTTP code.

    :param error: the error message
    :type error: str
    :return: formatted response (See application.errors.responses.error_response)
    """
    return bad_request_response(message=error)

