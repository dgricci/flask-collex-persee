#!/usr/bin/env python
# -*- coding: utf-8 -*-
import logging
from logging.handlers import RotatingFileHandler
import os
from flask import Flask
from werkzeug.utils import import_string
import requests

_config = {
    "development":"config.DevelopmentConfig",
    "testing"    :"config.TestingConfig",
    "production" :"config.ProductionConfig",
    # indirect key :
    "default"    :"development"
}

def _check_dirs(appcnf, key, defVal, rootVal=None):
    """
    Check whether appcnf[key] exists or not. In the latter case, set it to
    the default value. Then, check whether the directory is an absolute path
    or not. In the second case, make it absolute by prepending the root value.
    :param appcnf: the application configuration
    :type appcnf: dict
    :param key: the configuration key to check
    :type key: string
    :param defVal: the default value if configuration key does not exist
    :type defVal: string
    :param rootVal: the absolute path to prepend to the value if the latter is not an absolute path
    :type rootVal: string
    :return: the absolute path which is the value of the configuration key (updated)
    :rtype: string
    """
    val = appcnf.get(key)
    if val is None:
        val = os.path.abspath(defVal)
        appcnf[key] = val
    if not os.path.isabs(val):
        if rootVal is None:
            val = os.path.abspath(val)
        else:
            val = os.path.abspath(os.path.join(rootVal, val))
        appcnf[key] = val
    return val

def _configure_app(app):
    """
    Configure the application using object-based configuration, then
    json-based configuration
    :param app: the flask application to configure
    :type app: Object
    """
    config_name = os.environ.get("FLASK_CONFIGURATION", "default") or "default"
    if config_name == "default":
        config_name = _config["default"]
    else:
        if not config_name in _config:
            config_name = _config["default"]
    app.config["ENV"] = app.config["ENVIRONMENT"] = config_name

    cfg = import_string(_config[config_name])()
    app.config.from_object(cfg)

    # check for values and assigns default values if necessary :
    service_name = app.config.get("SERVICE_NAME")
    if service_name is None:
        service_name = "rdf-service"
        app.config["SERVICE_NAME"] = service_name

    server_name = app.config.get("SERVER_NAME")
    if server_name is None:
        server_name = "127.0.0.1:3000"
        app.config["SERVER_NAME"] = server_name

    app_dir = _check_dirs(app.config, "BASE_DIR", os.path.join(os.path.dirname(__file__),".."))

    cnf_dir = _check_dirs(app.config, "CONF_DIR", "conf", app_dir)
    if not os.path.exists(cnf_dir):
        raise FileNotFoundError(cnf_dir)

    # update configuration as per environment :
    # get abspath of this file (being run) joined with conf/{development|testing|production}/config.json
    config_json = os.path.join(cnf_dir, config_name, "config.json")
    try:
        app.config.from_json(config_json)
        app.config["CFG"] = config_json
    except:
        pass


    # thesaurii configuration will be found in the conf/{development|testing|production}/ folder as well :
    app.config["CACHE_DICT"] = os.path.join(cnf_dir, config_name, "thesaurii.json")

    # folder where the RDF-XML is stored :
    cache_dir = _check_dirs(app.config, "CACHE_DIR", "cache", app_dir)
    if not os.path.exists(cache_dir):
        raise FileNotFoundError(cache_dir)

    logging_level = logging.INFO
    if app.config["ENVIRONMENT"] == "production":
        logging_level = logging.WARNING if not app.debug else logging.DEBUG
    else:
        if app.debug:
            logging_level = logging.DEBUG
    app.config["LOGGING_LEVEL"] = logging_level

    if not app.testing:
        logs_dir = _check_dirs(app.config, "LOG_DIR", "logs", app_dir)
        if not os.path.exists(logs_dir):
            raise FileNotFoundError(logs_dir)

        file_handler = RotatingFileHandler(os.path.join(logs_dir,service_name+".log"), maxBytes=10240, backupCount=10)
        file_handler.setFormatter(logging.Formatter("%(asctime)s %(levelname)s: %(message)s [in %(pathname)s:%(lineno)d]"))
        file_handler.setLevel(logging_level)
        app.logger.addHandler(file_handler)

    app.logger.setLevel(app.config["LOGGING_LEVEL"])

def create_app():
    """
    Initialize the core application.
    :param config_class: la classe de configuration (Cf. config.py)
    :type config_class: Object
    """
    app = Flask(__name__ + " Collex-Persée")

    _configure_app(app)

    with app.app_context():
        from application.errors import bp as errors_bp
        app.register_blueprint(errors_bp)
        from application.main import bp as main_bp
        app.register_blueprint(main_bp)
        from application.xmlrdf import bp as xmlrdf_bp
        app.register_blueprint(xmlrdf_bp)

        app.logger.debug("config from {}".format(app.config["CFG"]))
        app.logger.info("{} starting up on {}".format(app.config["SERVICE_NAME"], app.config["SERVER_NAME"]))
        app.logger.debug(app.config)

    return app

def run_app(app):
    """
    Launch the Flask application
    :param app: the application
    :type app: Object
    """
    app.run()
