#!/usr/bin/env python
# -*- coding: utf-8 -*-
import unittest
import json
from flask import url_for
from tests import app

class RdfLibQueriesTest(unittest.TestCase):


    # code that is executed before all tests in one test run
    @classmethod
    def setUpClass(cls):
        cls.app = app

    # code that is executed after all tests in one test run
    @classmethod
    def tearDownClass(cls):
        pass

    # code that is executed before each test
    def setUp(self):
        self.client = RdfLibQueriesTest.app.test_client()
        self.app_context = RdfLibQueriesTest.app.app_context()
        self.app_context.push()

    # code that is executed after each test
    def tearDown(self):
        self.app_context.pop()

    #@unittest.skip("while debugging")
    def test_directory_palais(self):
        # http://127.0.0.1:3000/search/42?directory=palais
        resp = self.client.get(url_for('main.search', thid='42'), query_string={'directory':'palais'})
        self.assertEqual(resp.status_code, 200)
        json_data = json.loads(resp.data) # non valid JSON => JSONDecodeError
        self.assertGreater(len(json_data["features"]), 0)
        for f in json_data["features"]:
            self.assertRegex(f["properties"]["isA"],";?3467;?")

    #@unittest.skip("while debugging")
    def test_keyword_palais(self):
        # http://127.0.0.1:3000/search/42?keyword=palais
        resp = self.client.get(url_for('main.search', thid='42'), query_string={'keyword':'palais'})
        self.assertEqual(resp.status_code, 200)
        json_data = json.loads(resp.data) # non valid JSON => JSONDecodeError
        self.assertGreater(len(json_data["features"]), 0)

    #@unittest.skip("while debugging")
    def test_radius_1(self):
        # http://127.0.0.1:3000/search/42?lon=31.22469&lat=30.05692&radius=0.1
        resp = self.client.get(url_for('main.search', thid='42'), query_string={'lon':31.22469,'lat':30.05692,'radius':0.1})
        self.assertEqual(resp.status_code, 200)
        json_data = json.loads(resp.data) # non valid JSON => JSONDecodeError
        self.assertEqual(len(json_data["features"]), 1)
        feature = json_data["features"][0]
        coords = feature["geometry"]["coordinates"]
        self.assertAlmostEqual(coords[0], 31.22469, 6)
        self.assertAlmostEqual(coords[1], 30.05692, 6)
        properties = feature["properties"]
        self.assertEqual(properties["id"], "29798")
        self.assertEqual(properties["label_fr"], "palais de Guézireh")

    #@unittest.skip("while debugging")
    def test_directory_root(self):
        # http://127.0.0.1:3000/search/42?directory=root
        resp = self.client.get(url_for('main.search', thid='42'), query_string={'directory':'root'})
        self.assertEqual(resp.status_code, 200)
        json_data = json.loads(resp.data) # non valid JSON => JSONDecodeError
        self.assertGreater(len(json_data["features"]), 0)

    #@unittest.skip("while debugging")
    def test_keyword_musee_and_radius_10(self):
        # http://127.0.0.1:3000/search/42?keyword=mus%C3%A9e&lon=31.32&lat=30.1&radius=10&lang=fr
        resp = self.client.get(url_for('main.search', thid='42'), query_string={'keyword':'musée', 'lon':31.32, 'lat':30.1, 'radius':10})
        self.assertEqual(resp.status_code, 200)
        json_data = json.loads(resp.data) # non valid JSON => JSONDecodeError
        self.assertEqual(len(json_data["features"]), 2)
        feature = json_data["features"][0]
        properties = feature["properties"]
        self.assertIn(properties["id"], ("44309", "3277"))
        feature = json_data["features"][1]
        properties = feature["properties"]
        self.assertIn(properties["id"], ("44309", "3277"))

    #@unittest.skip("while debugging")
    def test_keyword_musee_and_radius_1(self):
        # http://127.0.0.1:3000/search/42?keyword=mus%C3%A9e&lon=31.32&lat=30.1&radius=1&lang=fr
        resp = self.client.get(url_for('main.search', thid='42'), query_string={'keyword':'musée', 'lon':31.32, 'lat':30.1, 'radius':1})
        self.assertEqual(resp.status_code, 200)
        json_data = json.loads(resp.data) # non valid JSON => JSONDecodeError
        self.assertEqual(len(json_data["features"]), 0)

    #@unittest.skip("while debugging")
    def test_keywords_palais_rue(self):
        # http://127.0.0.1:3000/search/42?keyword=palais,rue
        resp = self.client.get(url_for('main.search', thid='42'), query_string={'keyword':'palais,rue'})
        self.assertEqual(resp.status_code, 200)
        json_data = json.loads(resp.data) # non valid JSON => JSONDecodeError
        self.assertGreater(len(json_data["features"]), 0)

    #@unittest.skip("while debugging")
    def test_directory_theatres_cinemas(self):
        # http://127.0.0.1:3000/search/42?directory=théâtres,cinémas
        resp = self.client.get(url_for('main.search', thid='42'), query_string={'directory':'théâtres,cinémas'})
        self.assertEqual(resp.status_code, 200)
        json_data = json.loads(resp.data) # non valid JSON => JSONDecodeError
        self.assertGreater(len(json_data["features"]), 0)

if __name__ == '__main__':
    unittest.main(verbosity=2)

