#!/usr/bin/env python
# -*- coding: utf-8 -*-
import unittest
import json
from flask import url_for
from tests import app

class EndPointsTest(unittest.TestCase):


    # code that is executed before all tests in one test run
    @classmethod
    def setUpClass(cls):
        cls.app = app

    # code that is executed after all tests in one test run
    @classmethod
    def tearDownClass(cls):
        pass

    # code that is executed before each test
    def setUp(self):
        self.client = EndPointsTest.app.test_client()
        self.app_context = EndPointsTest.app.app_context()
        self.app_context.push()

    # code that is executed after each test
    def tearDown(self):
        self.app_context.pop()

    #@unittest.skip("while debugging")
    def test_root(self):
        # http://127.0.0.1:3000/
        resp = self.client.get(url_for('main.banner'))
        self.assertEqual(resp.status_code, 200)
        json_data = json.loads(resp.data) # non valid JSON => JSONDecodeError
        self.assertEqual(json_data["banner"], u'Collex-Pers\u00e9e - acc\u00e8s au th\u00e9saurus')

    #@unittest.skip("while debugging")
    def test_ping(self):
        # http://127.0.0.1:3000/ping
        resp = self.client.get(url_for('main.ping'))
        self.assertEqual(resp.status_code, 200)
        json_data = json.loads(resp.data) # non valid JSON => JSONDecodeError
        self.assertEqual(json_data["ping"], "pong")

    #@unittest.skip("while debugging")
    def test_gazeteers(self):
        # http://127.0.0.1:3000/gazeteers
        resp = self.client.get(url_for('main.get_gazeteers_metadata'))
        self.assertEqual(resp.status_code, 200)
        json_data = json.loads(resp.data) # non valid JSON => JSONDecodeError
        self.assertEqual(len(json_data), 3)
        for th in json_data:
            if th['thid'] == '42':
                self.assertEqual(th['idg'], "MT62")
                self.assertEqual(th['name'], "Modern Cairo Gazetteer")
                self.assertEqual(th['url'], "https://opentheso.huma-num.fr/")
            elif th['thid'] == '28':
                self.assertEqual(th['idg'], "8BA5Oa3I7l")
                self.assertEqual(th['name'], "Gazetier d'Alger")
                self.assertEqual(th['url'], "https://opentheso.huma-num.fr/")
            else:
                self.assertEqual(th['thid'], "TH_1")
                self.assertEqual(th['idg'], "1")
                self.assertEqual(th['name'], "Peuples")
                self.assertEqual(th['url'], "https://pactols.frantiq.fr/")

    #@unittest.skip("while debugging")
    def test_check(self):
        # http://127.0.0.1:3000/checknup/42
        resp = self.client.get(url_for('main.check_update', thid='42'))
        self.assertEqual(resp.status_code, 200)
        json_data = json.loads(resp.data) # non valid JSON => JSONDecodeError
        self.assertIn(json_data["42"], ["updated", "up-to-date"])
        self.assertIsNotNone(json_data["date"])
        self.assertRegex(json_data["date"], "^\d{4}-\d{2}-\d{2}$")

if __name__ == '__main__':
    unittest.main(verbosity=2)

