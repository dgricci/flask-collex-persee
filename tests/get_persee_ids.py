#!/usr/bin/env python
# -*- coding: utf-8 -*-
import unittest
import json
from flask import url_for
from tests import app

class PerseeIdsTest(unittest.TestCase):

    # code that is executed before all tests in one test run
    @classmethod
    def setUpClass(cls):
        cls.app = app

    # code that is executed after all tests in one test run
    @classmethod
    def tearDownClass(cls):
        pass

    # code that is executed before each test
    def setUp(self):
        self.client = PerseeIdsTest.app.test_client()
        self.app_context = PerseeIdsTest.app.app_context()
        self.app_context.push()

    # code that is executed after each test
    def tearDown(self):
        self.app_context.pop()

    #@unittest.skip("while debugging")
    def test_feature28775(self):
        # http://127.0.0.1:3000/perseeids/42/28775
        # http://ws.persee.fr/authority/athar/28775/id
        resp = self.client.get(url_for('main.get_persee_ids', thid='42', oid='28775'))
        self.assertEqual(resp.status_code, 200)
        json_data = json.loads(resp.data) # non valid JSON => JSONDecodeError
        self.assertEqual(len(json_data), 2)
        for idp in json_data:
            if idp["authority"] == "persee":
                self.assertEqual(idp,
                                 {"authority":"persee","identifier":"853445","type":"main",    "uri":None})
            else:
                self.assertEqual(idp,
                                 {"authority":"athar", "identifier":"28775", "type":"external","uri":"https://hdl.handle.net/20.500.11942/crt1pv73yya17"})

if __name__ == '__main__':
    unittest.main(verbosity=2)

