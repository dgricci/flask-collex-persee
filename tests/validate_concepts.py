#!/usr/bin/env python
# -*- coding: utf-8 -*-
import unittest
import json
from flask import url_for
from tests import app

class ValidateConceptsTest(unittest.TestCase):


    # code that is executed before all tests in one test run
    @classmethod
    def setUpClass(cls):
        cls.app = app

    # code that is executed after all tests in one test run
    @classmethod
    def tearDownClass(cls):
        pass

    # code that is executed before each test
    def setUp(self):
        self.client = ValidateConceptsTest.app.test_client()
        self.app_context = ValidateConceptsTest.app.app_context()
        self.app_context.push()

    # code that is executed after each test
    def tearDown(self):
        self.app_context.pop()

    #@unittest.skip("while debugging")
    def test_validate_concepts(self):
        # http://127.0.0.1:3000/validate/42
        resp = self.client.get(url_for('main.validate', thid='42'))
        self.assertEqual(resp.status_code, 200)
        json_data = json.loads(resp.data) # non valid JSON => JSONDecodeError
        self.assertIn("unref_concepts",json_data)
        self.assertIn("missing_broader_or_narrower",json_data)
        self.assertIn("no_geom",json_data)

if __name__ == '__main__':
    unittest.main(verbosity=2)

