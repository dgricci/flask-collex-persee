#!/usr/bin/env python
# -*- coding: utf-8 -*-

from application import create_app

# make this avail for all modules:
app = create_app()

