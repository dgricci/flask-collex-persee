#!/usr/bin/env python
# -*- coding: utf-8 -*-

import unittest

from tests.endpoints import EndPointsTest
from tests.places import RdfLibQueriesTest
from tests.get_feature_by_id import CollexFeaturesTest
from tests.validate_concepts import ValidateConceptsTest
from tests.tree_concepts import TreeConceptsTest
from tests.get_persee_ids import PerseeIdsTest

def setUpModule():
    pass

def tearDownModule():
    pass

if __name__ == '__main__':
    suite_loader = unittest.TestLoader()
    suite_0 = suite_loader.loadTestsFromTestCase(EndPointsTest)
    suite_1 = suite_loader.loadTestsFromTestCase(RdfLibQueriesTest)
    suite_2 = suite_loader.loadTestsFromTestCase(CollexFeaturesTest)
    suite_3 = suite_loader.loadTestsFromTestCase(ValidateConceptsTest)
    suite_4 = suite_loader.loadTestsFromTestCase(TreeConceptsTest)
    suite_5 = suite_loader.loadTestsFromTestCase(PerseeIdsTest)

    suite = unittest.TestSuite([suite_0, suite_1, suite_2, suite_3, suite_4, suite_5])
    unittest.TextTestRunner(verbosity=2).run(suite)

