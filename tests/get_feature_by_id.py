#!/usr/bin/env python
# -*- coding: utf-8 -*-
import unittest
import json
from flask import url_for
from tests import app

class CollexFeaturesTest(unittest.TestCase):

    # code that is executed before all tests in one test run
    @classmethod
    def setUpClass(cls):
        cls.app = app

    # code that is executed after all tests in one test run
    @classmethod
    def tearDownClass(cls):
        pass

    # code that is executed before each test
    def setUp(self):
        self.client = CollexFeaturesTest.app.test_client()
        self.app_context = CollexFeaturesTest.app.app_context()
        self.app_context.push()

    # code that is executed after each test
    def tearDown(self):
        self.app_context.pop()

    #@unittest.skip("while debugging")
    def test_feature13894(self):
        # http://127.0.0.1:3000/feature/42/13894
        # https://opentheso.huma-num.fr/opentheso/api/42.13894.rdf
        resp = self.client.get(url_for('main.get_feature_by_id', thid='42', oid='13894'))
        self.assertEqual(resp.status_code, 200)
        json_data = json.loads(resp.data) # non valid JSON => JSONDecodeError
        geom = json_data["geometry"]
        prop = json_data["properties"]
        self.assertEqual(json_data["type"], "Feature")
        self.assertEqual(prop["id"], "13894")
        self.assertEqual(prop["isA"], "6978")
        self.assertEqual(prop["created"], "2019-06-06")
        self.assertEqual(prop["modified"], "2019-06-06")
        self.assertEqual(prop["label_fr"], "casernes de Qasr al-Nil")
        self.assertEqual(prop["altLabel_fr"], "Champ de Mars")

    #@unittest.skip("while debugging")
    def test_feature28739(self):
        # http://127.0.0.1:3000/feature/42/28739
        # https://opentheso.huma-num.fr/opentheso/api/42.28739.rdf
        resp = self.client.get(url_for('main.get_feature_by_id', thid='42', oid='28739'))
        self.assertEqual(resp.status_code, 200)
        json_data = json.loads(resp.data) # non valid JSON => JSONDecodeError
        geom = json_data["geometry"]
        prop = json_data["properties"]
        self.assertEqual(geom["type"], "Point")
        self.assertAlmostEqual(geom["coordinates"][0], 31.25361, delta=0.00001)
        self.assertAlmostEqual(geom["coordinates"][1], 30.04582, delta=0.00001)
        self.assertEqual(prop["id"], "28739")
        self.assertEqual(prop["isA"], "28736;3217")
        self.assertEqual(prop["related"], "28751;28752;29628;29697")
        self.assertEqual(prop["uri"], "https://hdl.handle.net/20.500.11942/crtaq94qqdjhg")
        self.assertEqual(prop["exactMatch"], "https://catalogue.bnf.fr/ark:/12148/cb16716822c	https://data.bnf.fr/16716822/el-khalig__canal__egypte_/	https://www.geonames.org/12060425/port-said-street.html")

    #@unittest.skip("while debugging")
    def test_feature28738(self):
        # http://127.0.0.1:3000/feature/42/28738
        # https://opentheso.huma-num.fr/opentheso/api/42.28738.rdf
        resp = self.client.get(url_for('main.get_feature_by_id', thid='42', oid='28738'))
        self.assertEqual(resp.status_code, 200)
        json_data = json.loads(resp.data) # non valid JSON => JSONDecodeError
        geom = json_data["geometry"]
        prop = json_data["properties"]
        self.assertEqual(prop["id"], "28738")
        self.assertEqual(prop["isA"], "3237;3467")
        self.assertEqual(prop["description_fr"], "Palais construit à Shubra entre 1808 et 1821 (AH 1223-1236) par Muhammad 'Ali pacha. Numéro d'inventaire : 602. (Source : Cairo Gazetteer)")
        self.assertEqual(prop["historyNote_fr"], "1808-1821 (construction)")
        self.assertEqual(prop["note_fr"], "https://gallica.bnf.fr/iiif/ark:/12148/btv1b10546945n/f4/4313.767857142857,215.1499639238626,2801.6785714285716,1737.7500000000002/2802,1738/0/native.jpg")
        self.assertEqual(prop["scopeNote_fr"], "Muhammad 'Ali pacha (commanditaire)	Pascal Coste (architecte)")

    #@unittest.skip("while debugging")
    def test_feature28776(self):
        # http://127.0.0.1:3000/feature/42/28776
        # https://opentheso.huma-num.fr/opentheso/api/42.28776.rdf
        resp = self.client.get(url_for('main.get_feature_by_id', thid='42', oid='28776'))
        self.assertEqual(resp.status_code, 200)
        json_data = json.loads(resp.data) # non valid JSON => JSONDecodeError
        geom = json_data["geometry"]
        prop = json_data["properties"]
        self.assertEqual(prop["id"], "28776")
        self.assertEqual(prop["isA"], "3753")
        self.assertEqual(prop["label_ala"], "al-maʿhad al-ʿilmī al-faransī li-l-āthār al-sharqīyaẗ")
        self.assertEqual(prop["label_ar"], "المعهد العلمي الفرنسي للآثار الشرقية")
        self.assertEqual(prop["label_en"], "")
        self.assertEqual(prop["label_fr"], "Institut français d'archéologie orientale")
        self.assertEqual(prop["label_iso"], "al-maʿhad al-ʿilmī al-faransī li-l-āṯār al-šarqīyaẗ")
        self.assertEqual(prop["label_mul"], "")
        self.assertEqual(prop["relatedMatch"], "http://dbpedia.org/page/Institut_Fran%C3%A7ais_d'Arch%C3%A9ologie_Orientale	https://catalogue.bnf.fr/ark:/12148/cb118644589	https://data.bnf.fr/fr/11864458/institut_francais_d_archeologie_orientale_le_caire/	https://www.geonames.org/7922869/al-ma-had-al-faransi.html	https://www.wikidata.org/wiki/Q1472454")

if __name__ == '__main__':
    unittest.main(verbosity=2)

