#!/usr/bin/env python
# -*- coding: utf-8 -*-
import unittest
from anytree.importer import JsonImporter
from flask import url_for
from tests import app

class TreeConceptsTest(unittest.TestCase):


    # code that is executed before all tests in one test run
    @classmethod
    def setUpClass(cls):
        cls.app = app

    # code that is executed after all tests in one test run
    @classmethod
    def tearDownClass(cls):
        pass

    # code that is executed before each test
    def setUp(self):
        self.client = TreeConceptsTest.app.test_client()
        self.app_context = TreeConceptsTest.app.app_context()
        self.app_context.push()

    # code that is executed after each test
    def tearDown(self):
        self.app_context.pop()

    #@unittest.skip("while debugging")
    def test_tree_concepts(self):
        # http://127.0.0.1:3000/concepts/42
        resp = self.client.get(url_for('main.get_all_concepts', thid='42'))
        self.assertEqual(resp.status_code, 200)
        importer = JsonImporter()
        tree = importer.import_(resp.data) # non valid JSON => JSONDecodeError
        self.assertTrue(bool(tree))
        self.assertTrue(tree.is_root)
        self.assertEqual(tree.name, "MT62")
        self.assertGreater(len(tree.children), 0)

if __name__ == '__main__':
    unittest.main(verbosity=2)

