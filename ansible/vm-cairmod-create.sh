#!/bin/bash
[ -d ./.vagrant ] && {
    echo >&2 "Sure to have cleaned the VM and ./.vagrant to create a new VM ?"
    echo >&2 "If not, use vm-cairmod-remove.sh"
    echo >&2 "Aborted."
    exit 1
}
vagrant up
rc=$?
[ ${rc} -ne 0 ] && {
    exit ${rc}
}
sleep 10
./vm-cairmod-ansible.sh
exit $?

