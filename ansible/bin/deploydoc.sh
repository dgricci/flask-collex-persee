#!/bin/sh
sleep 10
HOMEDIR=/home/invisu
DOCSUFFIX=docs
REPODIR="${HOMEDIR}/${DOCSUFFIX}"
ARCHIVESDIR="${HOMEDIR}/archives/${DOCSUFFIX}"
DOCDIR=/var/www/html/
CURDOC=doc
NEWDOC="new-${CURDOC}"
OLDDOC="old-${CURDOC}"
cd "${REPODIR}"
f="$(ls -1rt htmlsite-collex-persee-*.tgz 2>/dev/null | tail -1)"
[ -z "${f}" -o ! -f "${f}" ] && exit 0
mv "${f}" "${ARCHIVESDIR}"
cd "${DOCDIR}"
mkdir "./${NEWDOC}"
tar -xzf "${ARCHIVESDIR}/${f}" -C "./${NEWDOC}"
[ -d "./${CURDOC}" ] && mv "./${CURDOC}" "./${OLDDOC}"
mv "./${NEWDOC}" "./${CURDOC}"
rm -fr "./${OLDDOC}"
exit 0

