#!/bin/sh
sleep 10
HOMEDIR=/home/invisu
APPSUFFIX=app
REPODIR="${HOMEDIR}/${APPSUFFIX}"
ARCHIVESDIR="${HOMEDIR}/archives/${APPSUFFIX}"
APPDIR=/var/www/collexpersee
CURAPP=app
NEWAPP="new-${CURAPP}"
OLDAPP="old-${CURAPP}"
cd "${REPODIR}"
f="$(ls -1rt flask-collex-persee-*.tgz 2>/dev/null | tail -1)"
[ -z "${f}" -o ! -f "${f}" ] && exit 0
mv "${f}" "${ARCHIVESDIR}"
[ -z "$(command -v python3)" ] && {
    >&2 echo "Missing python3 aborted"
    exit 2
}
pv="$(python3 --version | sed -e 's:^Python ::')"
cd "${APPDIR}"
mkdir "${NEWAPP}"
cd "./${NEWAPP}"
PIPENV_VENV_IN_PROJECT=1 ${HOMEDIR}/.local/bin/pipenv --three
tar -xzf "${ARCHIVESDIR}/${f}"
sed -i -e 's/^\(python_version = "\)[^"]*"$/\1'${pv}'"/' ./Pipfile
${HOMEDIR}/.local/bin/pipenv sync
for fl in $(grep -r "${NEWAPP}" .venv | cut -d: -f1) ; do sed -i -e 's/'${NEWAPP}'/'${CURAPP}'/g' "${fl}" ; done
# use make PROD_CONFIG=<DIR>/ package on development platform where <DIR>/ contains the config.json to be deployed !
cd ..
[ -d "./${CURAPP}" ] && mv "./${CURAPP}" "./${OLDAPP}"
mv "./${NEWAPP}" "./${CURAPP}"
rm -fr "./${OLDAPP}"
# signal for systemd : something as changed !
touch "./${CURAPP}/.ready"
exit 0

