#!/bin/bash
[ ! -d ./.vagrant ] && {
    echo >&2 "Not sure the VM has been created ..."
    echo >&2 "Use vm-cairmod-create.sh"
    echo >&2 "Aborted."
    exit 1
}
vagrant destroy
rc=$?
rm -fr ./.vagrant
exit ${rc}

