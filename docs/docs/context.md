CollEx-Persée - Accès au Thésaurus
==================================

Contexte
--------

Unité de service et de recherche du CNRS, le laboratoire InVisu conduit une
réflexion sur l'adaptation de l'histoire de l'art à l'ère du numérique et
s'attache à la reconstitution de corpus visuels et textuels dispersés. Son
activité s'articule autour de trois pôles que sont l'accompagnement aux
humanités numériques, l'expérimentation d'outils pour le partage de données
comprenant la mise à disposition de contenus numériques, et le conseil en
matière de traitement numérique des données. Le terrain d'application de ses
recherches est les arts monumentaux et les arts visuels en Méditerranée
contemporaine. Parmi ses réalisations, InVisu a mené à bien le projet Athar
(BSN5 2014) en partenariat avec Persée et l’Ifao. Ce projet a permis de
numériser et de mettre en ligne l’intégralité des bulletins du Comité de
conservation des monuments de l’art arabe enrichis par un référentiel des
toponymes du Caire. Ce projet a donné naissance à la plateforme éponyme
(Perséide Athar). Ce [site web](https://athar.persee.fr/) en Open Access est
basé sur les technologies du linked open data, développé par Persée offrant
des outils et des services d’exploration des contenus à forte valeur ajoutée.
Dans le domaine du traitement de corpus visuels, le laboratoire a élaboré un
catalogue raisonné de l’œuvre du photographe du Caire,
[Facchinelli](http://facchinelli.huma-num.fr/), disponible
en ligne avec des données enrichies, qui permettent d’étudier la géographie
patrimoniale de la ville.

Géo-visualisation de contenus de la Perséide Athar : le cas du Caire moderne
----------------------------------------------------------------------------

[InVisu](https://invisu.cnrs.fr/) est lauréat de la première vague de l’appel
à projets CollEx-Persée (3 octobre 2018) pour le projet de géo-visualisation
Cairmod  qui se déroulera de novembre 2018 à juin 2020. Ce projet est piloté
par InVisu en partenariat avec l’UMS [Persée](http://info.persee.fr/),
l’[IFAO](https://www.ifao.egnet.net/) et l’[ENSG](https://www.ensg.eu/).
Le projet est mené par Bulle Tuil Leonetti.

Le projet CAIRMOD vise à structurer finement des albums iconographiques, déjà
numérisés par Gallica, documentant la ville moderne du Caire pour les diffuser
sur la [Perséide Athar](http://info.persee.fr/perseides/).
Cette structuration, enrichie par un référentiel des toponymes (avec
OpenTheso), sera effectuée grâce à la plateforme technique de
Persée, et alimentera un module de géo-visualisation. L’outil mis en œuvre
permettra de connecter les données disponibles sur la Perséide à un système
d’information géographique (SIG) développé sur le logiciel libre QGIS et
accessible en ligne. La spatialisation cartographique intègrera une dimension
historique pour rendre compte du renouvellement urbain dans le temps. Le cas
du Caire moderne où cette dimension est particulièrement importante est à ce
titre exemplaire. L’expérimentation se fonde sur un corpus visuel concernant
Le Caire, mais l’outil a vocation à être déployé, à terme, sur un volume plus
large de documents visuels ou sur d’autres lieux. Toutes les données seront
structurées de manière à être réutilisables.

[Plus d'information](https://invisu.cnrs.fr/project/geo-visualisation-de-contenus-de-la-perseide-athar-le-cas-du-caire-moderne/)

Travaux
-------

![OpenTheso](img/logo-OpenTheso.png)

Ce service permet d'accèder à un thésaurus sous
[OpenTheso](https://github.com/miledrousset/opentheso/wiki)
et fournit quelques méthodes pour requêter la copie locale du RDF-XML pour
fabriquer des objets géographiques sous format `GeoJSON`.

Il a été développé via le projet ANR **[CollEx-Persée CAIRMOD](https://www.collexpersee.eu/projet/cairmod/)**
par des étudiants de la promotion 2018-2019 du parcours TSI de l'ENSG (3ième
année de l'école d'ingénieurs et Master 2 co-accrédité par l'Université Paris
Est - Marne-La-Vallée), étudiants pilotés par l'équipe projet InVisu de l'Inha.

### Étudiants ###

* Antoine Fondère
* Corentin Crapart
* Erwan Viegas
* Max Permalnaick
* Sinda Thaalbi
* Tristan Harling
* Wiltold Podlejski

Encadrement technique et pédagogique : Didier Richard (IGN/ENSG)

### InVisu - CollEx-Persée ###

* Bulle Tuil Leonetti - Responsable du projet, Ingénieur de recherche en analyse des sources - InVisu (CNRS/INHA)
* Julie Erismann - Géographe, Ingénieur de recherche - InVisu (CNRS/INHA)
* Pierre Mounier - Développeur et intégrateur d’applications, Ingénieur d'étude - InVisu (CNRS/INHA)
* Hélène Bégnis - Coordination du projet pour Persée - Chargée des partenariats recherche
* ...

### Partenaires ###

![InVisu/INHA](img/logo-Invisu.png)

![Persée](img/logo-Persee.png)

![IFAO](img/logo-IFAO.png)

![IGN/ENSG](img/logo-ENSG.png)

