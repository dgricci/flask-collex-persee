Tests
=====

* Chargement du thésaurus en cache (effectué avant que la première requête ne
  soit exécutée) :

```bash
$ wget -O - "http://127.0.0.1:3000/ping" > /dev/null
--2019-10-28 13:14:58--  http://127.0.0.1:3000/ping
Connexion à 127.0.0.1:3000… connecté.
requête HTTP transmise, en attente de la réponse… 200 OK
Taille : 16 [application/json]
Enregistre : «STDOUT»

-                                       100%[=============================================================================>]      16  --.-KB/s    ds 0s      

2019-10-28 13:15:07 (3,65 MB/s) — envoi vers sortie standard [16/16]
```

soit environ 9s pour initialiser l'application lors de la première requête.

* Recherche des palais dans le thésaurus :

```bash
$ wget -O - "http://127.0.0.1:3000/search/42?directory=palais" > /dev/null
--2019-10-28 13:11:01--  http://127.0.0.1:3000/search/42?directory=palais
Connexion à 127.0.0.1:3000… connecté.
requête HTTP transmise, en attente de la réponse… 200 OK
Taille : 53084 (52K) [application/json]
Enregistre : «STDOUT»

-                                       100%[=============================================================================>]  51,84K  --.-KB/s    ds 0s      

2019-10-28 13:11:02 (1,23 GB/s) — envoi vers sortie standard [53084/53084]
```

soit 28 objets en 1s ...

* Recherche des objets ayant le mot-clef "palais" :

```bash
$ wget -O - "http://127.0.0.1:3000/search/42?keyword=palais" > /dev/null
--2019-10-28 13:11:51--  http://127.0.0.1:3000/search/42?keyword=palais
Connexion à 127.0.0.1:3000… connecté.
requête HTTP transmise, en attente de la réponse… 200 OK
Taille : 59435 (58K) [application/json]
Enregistre : «STDOUT»

-                                       100%[=============================================================================>]  58,04K  --.-KB/s    ds 0s      

2019-10-28 13:11:51 (1,27 GB/s) — envoi vers sortie standard [59435/59435]
²```

soit 31 objets en moins d'une seconde ...

* Recherche des objets autour de coordonnées dans un rayon de 1 km autour des
  coordonnées fournies :

```bash
$ wget -O - "http://127.0.0.1:3000/search/42?lon=31.22469&lat=30.05692&radius=1" > /dev/null
--2019-10-28 13:12:45--  http://127.0.0.1:3000/search/42?lon=31.22469&lat=30.05692&radius=1
Connexion à 127.0.0.1:3000… connecté.
requête HTTP transmise, en attente de la réponse… 200 OK
Taille : 13085 (13K) [application/json]
Enregistre : «STDOUT»

-                                       100%[=============================================================================>]  12,78K  --.-KB/s    ds 0s      

2019-10-28 13:12:45 (865 MB/s) — envoi vers sortie standard [13085/13085]
```

soit 7 objets en moins d'une seconde !

* Récupération de tous les objets :

```bash
$ wget -O - "127.0.0.1:3000/search/42?directory=root" > /dev/null
--2019-10-28 13:13:31--  http://127.0.0.1:3000/search/42?directory=root
Connexion à 127.0.0.1:3000… connecté.
requête HTTP transmise, en attente de la réponse… 200 OK
Taille : 396374 (387K) [application/json]
Enregistre : «STDOUT»

-                                       100%[=============================================================================>] 387,08K  --.-KB/s    ds 0s      

2019-10-28 13:13:31 (1,61 GB/s) — envoi vers sortie standard [396374/396374]
```

soit 212 objets en moins d'une seconde !

