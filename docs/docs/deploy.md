# Déploiement #

## Documentation ##

### Préparation côté machine virtuelle ###

Au démarrage du système, il faut pouvoir savoir si on a déposé une nouvelle
version de la documentation dans le répertoire `/home/invisu/docs/`. Pour
cela, deux unités `systemd` sont ajoutées :

```bash
$ sudo cat <<-EOF > /etc/systemd/system/collexperseedoc.path
[Unit]
Description=Monitor ~invisu/docs (see collexperseedoc.service)

[Path]
PathExistsGlob=/home/invisu/docs/htmlsite-collex-persee-*.tgz
PathChanged=/home/invisu/docs/
Unit=collexperseedoc.service

[Install]
WantedBy=multi-user.target
EOF
```

Ce premier fichier permet de savoir si un fichier a été modifié (créé, mis à
jour) et si c'est le cas, alors le service associé est exécuté :

```bash
$ sudo cat <<-EOF > /etc/systemd/system/collexperseedoc.service
[Unit]
Description=Move documentation to target when ~invisu/docs is changed (see collexperseedoc.path)

[Service]
Type=simple
User=invisu
Group=www-data
ExecStart=/home/invisu/bin/deploydoc.sh
Restart=no

[Install]
WantedBy=multi-user.target
EOF
```

Le script à exécuter lors d'une modification est mis en place :

```bash
$ cat <<-EOF > /home/invisu/bin/deploydoc.sh
#!/bin/sh
sleep 10
cd /home/invisu/docs
f="$(ls -1rt *.tgz | tail -1)"
[ -z "${f}" -o ! -f "${f}" ] && exit 0
mv "${f}" ../archives/docs/
cd /var/www/html/
mkdir new-doc
tar -xzf "/home/invisu/archives/docs/${f}" -C ./new-doc/
[ -d ./doc ] && mv ./doc ./old-doc
mv ./new-doc ./doc
rm -fr ./old-doc/
exit 0
EOF
$ chmod +x /home/invisu/bin/deploydoc.sh
```

Le script vérifie qu'une archive est bien présente. Si c'est le cas, l'archive
est déplacée dans le répertoire de sauvegarde. Puis l'archive est déployée
pour diffusion.

Il faut alors préparer les répertoires qui vont recevoir la documentation du
service. Un premier répertoire pour le dépôt de la documentation sous forme
d'archive et un second associé au serveur web pour la diffusion :

```bash
$ mkdir -p ~/docs ~/archives/docs
$ sudo mkdir /var/www/html/doc/
$ sudo chown invisu:www-data /var/www/html/ /var/www/html/doc/
```

Enfin, le système `systemd` est configuré pour prendre en compte les unités
précédentes :

```bash
$ sudo systemctl enable collexperseedoc.path collexperseedoc.service
$ sudo systemctl start  collexperseedoc.path collexperseedoc.service
```

### Paquetage de la documentation ###

Côté développement, il suffit de lancer la cible `package-doc` pour fabriquer
le site web de documentation à déployer :

```bash
$ make package-doc
```

Puis, il faut copier l'archive ainsi fabriquée vers la machine virtuelle :

```bash
$ scp htmlsite-collex-persee-yyyy-mm-jjThh-mm-ss.sssssssssZ invisu@192.168.99.100:docs/
```

!!! info "Où voir la documentation ?"
    La documentation est automatiquement mise en ligne à l'adresse
    `http://192.168.99.100/doc/` :

    ![Documentation CollEx-Persée SIG](img/htmlsite-doc.png)

#### Diagramme de composants de la documentation ####

![Diagramme de composants de la documentation](img/FlaskAppDocumentationComponentsDiagram.png)


## Application ##

### Pré-requis ###

Il faut vérifier que `python3` est installé :

```bash
$ sudo apt list python3
```

si ce n'est pas le cas :

```bash
$ sudo apt install python3
```

Il faut vérifier que `pip` est installé :

```bash
$ sudo apt list python3-pip
```

si ce n'est pas le cas :

```bash
$ sudo apt install python3-pip
```

`pipenv` est alors installé :

```bash
$ pip3 install --user pipenv
```

`pipenv` est ainsi installé dans `~invisu/.local/bin/pipenv`.
Il faut alors ajouter `pipenv` à `${PATH}` via `~invisu/.bashrc` :

```bash
$ cat <<-EOF >> ~invisu/.bashrc

# pipenv
USERBASE=$HOME/.local/bin
if [ ! -z "$PATH" ] ; then
    if [ `echo $PATH | grep -c "$USERBASE"` -eq 0 ] ;  then
        export PATH=${PATH}:$USERBASE
    fi
else
    export PATH=$USERBASE
fi
unset USERBASE
eval "$(pipenv --completion)"

EOF
$ source ~invisu/.bashrc
```

Le répertoire de l'application, ainsi que celui qui va accueillir les versions
de l'application sont crées dans `~invisu` :

```bash
$ mkdir -p app archives/app
```

### Installation de l'application Flask ###

Le répertoire de l'application en production est alors créé :

```
$ sudo mkdir -p /var/www/collexpersee/app
$ sudo chown -R invisu:www-data /var/www/collexpersee
```

Au démarrage du système, il faut pouvoir savoir si on a déposé une nouvelle
version de l'application dans le répertoire `/home/invisu/app/`. Pour
cela, deux unités `systemd` sont ajoutées :

```bash
$ sudo cat <<-EOF > /etc/systemd/system/collexperseeapp.path
[Unit]
Description=Monitor ~invisu/app (see collexperseeapp.service)

[Path]
PathExistsGlob=/home/invisu/app/flask-collex-persee-*.tgz
PathChanged=/home/invisu/app/
Unit=collexperseeapp.service

[Install]
WantedBy=multi-user.target
EOF
```

Ce premier fichier permet de savoir si un fichier a été modifié (créé, mis à
jour) et si c'est le cas, alors le service associé est exécuté :

```bash
$ sudo cat <<-EOF > /etc/systemd/system/collexperseeapp.service
[Unit]
Description=Move documentation to target when ~invisu/app is changed (see collexperseeapp.path)

[Service]
Type=simple
User=invisu
Group=www-data
ExecStart=/home/invisu/bin/deployapp.sh
Restart=no

[Install]
WantedBy=multi-user.target
EOF
```

Le script à exécuter lors d'une modification est mis en place :

```bash
$ cat <<-EOF > /home/invisu/bin/deployapp.sh
#!/bin/sh
sleep 10
cd /home/invisu/app
f="$(ls -1rt *.tgz | tail -1)"
[ -z "${f}" -o ! -f "${f}" ] && exit 0
mv "${f}" ../archives/app/
[ -z "$(command -v python3)" ] && {
    >&2 echo "Missing python3 aborted"
    exit 2
}
pv="$(python3 --version | sed -e 's:^Python ::')"
cd /var/www/collexpersee/
mkdir new-app
cd ./new-app
PIPENV_VENV_IN_PROJECT=1 /home/invisu/.local/bin/pipenv --three
tar -xzf "/home/invisu/archives/app/${f}"
sed -i -e "s/^\(python_version = \"\)[^\"]*\"$/\1${pv}\"/" ./Pipfile
/home/invisu/.local/bin/pipenv sync
for f in $(grep -r new-app .venv | cut -d: -f1) ; do sed -i -e 's/new-app/app/g' $f ; done
# change production configuration
#cat <<-EOF > ./conf/production/config.json
#{
#    "CACHE_DIR":"/var/www/html/projects/collex-persee",
#    "LOG_DIR":"/var/www/html/logs"
#}
#EOF
# use make PROD_CONFIG=<DIR>/ package on development platform where <DIR>/ contains the config.json to be deployed !
cd ..
[ -d ./app ] && mv ./app ./old-app
mv ./new-app ./app
rm -fr ./old-app/
# signal for systemd : something as changed !
touch ./app/.ready
exit 0
EOF
$ chmod +x /home/invisu/bin/deployapp.sh
$ sudo mkdir /var/www/html/projects/collex-persee
$ sudo chown invisu:www-data /var/www/html/projects/collex-persee
$ sudo chmod g+w /var/www/html/projects/collex-persee
```

Il faut pourvoir lancer l'application Flask au démarrage du système. De
nouveau, une unité `systemd` va le permettre :

```bash
$ sudo cat <<-EOF > /etc/systemd/system/collexperseeflaskapp.service
[Unit]
Description=Flask CollEx-Persée application service
After=network.target

[Service]
User=invisu
Group=www-data
ExecReload=-+/usr/bin/pkill -HUP gunicorn
ExecStartPre=-+/usr/bin/pkill -HUP gunicorn
ExecStart=/var/www/collexpersee/app/.venv/bin/gunicorn \
    --bind=0.0.0.0:3030 \
    --workers=2 \
    --log-level=debug \
    --access-logfile=/var/www/html/logs/access_gunicorn.log \
    --access-logformat="%({x-forwarded-for}i)s %(h)s %(u)s \"%(r)s\" %(s)s \"%(f)s\" \"%(a)s\"" \
    --error-logfile=/var/www/html/logs/error_gunicorn.log \
    rdfservice:app
ExecStop=-+/usr/bin/pkill -TERM gunicorn
Environment="FLASK_CONFIGURATION=production"
WorkingDirectory=/var/www/collexpersee/app/
Restart=always

[Install]
WantedBy=multi-user.target
EOF
```

#### Diagramme de composants de l'application ####

![Diagramme de composants de l'application](img/FlaskAppComponentsDiagram.png)

#### Server web en front en mode production ####

En mode production, le serveur web `gunicorn` lance l'application `Flask`
avec deux tâches (workers) écoute sur le port `3030` sur toutes les IP
entrantes pour interroger l'application `Flask` sur le port `3000`.

Chaque fois que le répertoire `/var/www/collexpersee/app` sera mis à jour, il
faudra relancer l'application. Pour cela, il faut rajouter une unité `systemd`
qui va prévenir `collexperseeflaskapp.service`, cette dernière unité de
service commence par stopper l'exécution de l'application, puis lance la
nouvelle :

```bash
$ sudo cat <<-EOF > /etc/systemd/system/collexperseeflaskapp.path
[Unit]
Description=Monitor /var/www/collexpersee/app (see collexperseeflaskapp.service)

[Path]
PathChanged=/var/www/collexpersee/app/.ready
Unit=collexperseeflaskapp.service

[Install]
WantedBy=multi-user.target
EOF
#
```

Enfin, le système `systemd` est configuré pour prendre en compte les unités
précédentes :

```bash
$ sudo systemctl enable collexperseeapp.path collexperseeapp.service
$ sudo systemctl start  collexperseeapp.path collexperseeapp.service
```

Côté développement, il suffit de lancer la cible `package` pour fabriquer
le site web de documentation à déployer :

```bash
$ make PROD_CONFIG=./var/targetted-server package
```

Le répertoire `./var/targetted-server` contient les fichier `config.json` et
`thesaurii.json` pour la mise en production.

!!! important "Exemple de configuration 1"
    * config.json avec nom du serveur et proxies :

```json
{
    "CACHE_DIR":"/var/www/html/projects/collex-persee",
    "LOG_DIR":"/var/www/html/logs",
    "PUBLIC_HOST_URL":"http://cairmod.ensg.eu",
    "PUBLIC_BASE_APP":"/app",
    "PROXIES":{"http":"http://10.0.4.2:3128/", "https":"https://10.0.4.2:3128/"}
}
```

!!! important "Exemple de configuration 2"
    * thesaurii.json avec deux gazetiers :

```json
{
    "42": {
        "idg": "MT62",
        "th": "42",
        "name": "Modern Cairo Gazetteer",
        "url": "https://opentheso.huma-num.fr/",
        "last-update": "1970-01-01"
    },
    "28": {
        "idg": "8BA5Oa3I7l",
        "th": "28",
        "name": "Gazetier d'Alger",
        "url": "https://opentheso.huma-num.fr/",
        "last-update": "1970-01-01"
    }
}
```

Puis, il faut copier l'archive ainsi fabriquée vers la machine virtuelle :

```bash
$ scp flask-collex-persee-yyyy-mm-jjThh-mm-ss.sssssssssZ invisu@192.168.99.100:app/
```

On peut (enfin) activer l'application :

```bash
$ sudo systemctl enable collexperseeflaskapp.path collexperseeflaskapp.service
$ sudo systemctl start  collexperseeflaskapp.path collexperseeflaskapp.service
```

#### Server web comme proxy à l'application ####

<!--
On rajoute un alias de nom à la machine virtuelle pour distinguer les points
d'entrée du serveur web :

```bash
$ sudo sed -i -e "s/\(\.collexpersee\.fr\)$/\1 data\1/" /etc/hosts
```
  -->

On utilise `nginx` comme proxy pour éviter d'exposer le service à l'extérieur
directement. Le fichier final reprend le contenu de
`/etc/nginx/sites-available/qgisserver` et y ajoute les informations
nécessaires pour rediriger les URLS en `/app/<route>` vers `gunicorn` :

```bash
$ cd /etc/sites-available
$ sudo cat <<-EOF > /etc/nginx/sites-available/webgis.collexpersee.fr
# Extract server name and port from HTTP_HOST, this
# is required because we are behind a VMs mapped port

map $http_host $parsed_server_name {
    default  $host;
    "~(?P<h>[^:]+):(?P<p>.*+)" $h;
}

map $http_host $parsed_server_port {
    default  $server_port;
    "~(?P<h>[^:]+):(?P<p>.*+)" $p;
}

upstream qgis_mapserv_backend {
    ip_hash;
    server unix:/run/qgis_mapserv2.sock;
    server unix:/run/qgis_mapserv1.sock;
}

upstream gunicorn_backend {
    server 127.0.0.1:3030 fail_timeout=0;
}

server {
    listen 80 default_server;
    listen [::]:80 default_server;

    # SSL configuration
    #
    # listen 443 ssl default_server;
    # listen [::]:443 ssl default_server;

    # This is vital
    underscores_in_headers on;

    root /var/www/html;

    server_name webgis.collexpersee.fr;

    index index.html;

    recursive_error_pages on ;
    error_page  403 404         = /40x.html;
    error_page  500 502 503 504 = /50x.html;

    # write access and error logs to /var/www/html/logs
    access_log /var/www/html/logs/webgis.collexpersee.fr_access.log;
    error_log /var/www/html/logs/webgis.collexpersee.fr_error.log;

    location / {
        # First attempt to serve request as file, then
        # as directory, then fall back to displaying a 404.
        try_files $uri $uri/ =404;
    }

    location ~ ^/project/([^/]+)/?(.*)$
    {
      set $qgis_project /var/www/html/projects/$1/$1.qgs;
      rewrite ^/project/(.*)$ /qgisserver/qgis_mapserv.fcgi last;
    }

    location /qgisserver/ {
        gzip           off;
        fastcgi_pass   qgis_mapserv_backend;

        # $http_host contains the original server name and port, such as: "localhost:8080"
        # QGIS Server behind a VM needs this parsed values in order to automatically
        # get the correct values for the online resource URIs

        fastcgi_param SERVER_NAME       $parsed_server_name;
        fastcgi_param SERVER_PORT       $parsed_server_port;

        # Set project file from env var
        fastcgi_param QGIS_PROJECT_FILE $qgis_project;

        include        fastcgi_params;
    }

    location ~ /app(?<target_url>/.*)$ {
        # forward application requests to the gunicorn server mind the port of the flask application !!!!!
        proxy_set_header Host 127.0.0.1:3000;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_redirect off;
        proxy_http_version 1.1;
        proxy_pass http://gunicorn_backend$target_url?$query_string;
    }

}
EOF
$ cd ../sites-enabled
$ sudo chown root:root /etc/nginx/sites-available/webgis.collexpersee.fr
$ sudo ln -s /etc/nginx/sites-available/webgis.collexpersee.fr
$ sudo rm qgisserver
$ sudo systemctl restart nginx
```

Pour tester que l'application `Flask` est interrogeable, il suffit de la
"pinger" depuis la machine virtuelle :

```bash
$ wget -S -O - http://127.0.0.1/app/ping
--2019-11-17 21:53:12--  http://127.0.0.1/app/ping
Connexion à 127.0.0.1:80… connecté.
requête HTTP transmise, en attente de la réponse…
  HTTP/1.1 200 OK
  Server: nginx/1.14.2
  Date: Sun, 17 Nov 2019 20:53:12 GMT
  Content-Type: application/json
  Content-Length: 21
  Connection: keep-alive
Taille : 21 [application/json]
Enregistre : «STDOUT»

-                                         0%[                                                                              ]       0  --.-KB/s               {
  "ping": "pong"
}
-                                       100%[=============================================================================>] 21  --.-KB/s    ds 0s

2019-11-17 21:53:12 (1,65 MB/s) — envoi vers sortie standard [21/21]
```

Puis, il faut tester depuis la machine hôte :

```bash
$ wget -S -O - http://192.168.99.100/app/ping
--2019-11-17 21:53:46--  http://192.168.99.100/app/ping
Connexion à 192.168.99.100:80… connecté.
requête HTTP transmise, en attente de la réponse…
  HTTP/1.1 200 OK
  Server: nginx/1.14.2
  Date: Sun, 17 Nov 2019 20:53:45 GMT
  Content-Type: application/json
  Content-Length: 21
  Connection: keep-alive
Taille : 21 [application/json]
Enregistre : «STDOUT»

-                                         0%[                                                                              ]       0  --.-KB/s               {
  "ping": "pong"
}
-                                       100%[=============================================================================>]      21  --.-KB/s    ds 0s

2019-11-17 21:53:46 (1,40 MB/s) — envoi vers sortie standard [21/21]
```

### Paquetage de l'application ###

Côté développement, il suffit de lancer la cible `package` pour fabriquer
le service web d'interrogation d'OpenTheso à déployer :

```bash
$ make package
```

Puis, il faut copier l'archive ainsi fabriquée vers la machine virtuelle :

```bash
$ scp flask-collex-persee-yyyy-mm-jjThh-mm-ss.sssssssssZ invisu@192.168.99.100:app/
```

!!! tip "Comment interroger l'application ?"
    Le service web alors automatiquement mis en ligne à l'adresse
    `http://192.168.99.100/app/` :

    Ainsi les requêtes suivantes doivent aboutir sans erreur :

    * `wget -S -O - http://192.168.99.100/app/checknup/42` : vérification de la mise à jour du thésaurus ;
    * `wget -S -O - http://192.168.99.100/app/search/42?keyword=palais&lang=fr&` : recherche des palais dans le thésaurus ;
    * `wget -S -O - http://192.168.99.100/app/feature/42/13894` : récupération de l'objet géographique d'identifiant `13894` ;
    * `wget -S -O - http://192.168.99.100/app/concepts/42` : récupération de l'arbre des concepts (recherche sémantique) ;
    * `wget -S -O - http://192.168.99.100/app/validate/42` : vérification de la cohérence du thésaurus ;
    * `wget -S -O - http://192.168.99.100/app/geojson/42` : récupération de tous les objets géographiques en format `GeoJSON`.


### (Re)démarrage manuel de l'application ###

Le plus simple, une fois connecté à la machine virtuelle, et de relancer
`gunicorn` :

```bash
$ sudo systemctl restart collexperseeflaskapp.service
```

Pour obliger l'application à recharger le cache, il suffit, toujours connecté
à la machine virtuelle, de modifier la date du cache dans le fichier
`/var/www/collexpersee/app/conf/production/thesaurii.json` :

```bash
...
    "last-update": "2019-09-27"
...
```

par exemple en reculant la date du cache d'un jour :

```bash
...
    "last-update": "2019-09-26"
...
```

puis de relancer le service manuellement comme indiqué ci-dessus !
Enfin, il faut recharger le cache et fabriquer les données géographiques,
toujours en étant connecté à la machine virtuelle :

```bash
$ wget -S -O - http://127.0.0.1/app/ping
...
$ wget -S -O - http://127.0.0.1/app/geojson/42
...
```


## Exportation / Importation de la VM ##

### Export de la machine virtuelle ###

Une fois ces opérations de configuration terminées, il faut exporter la
machine virtuelle pour la déployer. Tout d'abord, dans le menu "Fichier", on
clique sur "Exporter un appareil virtuel..." :

![VM - exporter - phase 1](./img/vb-export-vm-1.png)

Puis, il faut sélectionner la machine virtuelle à exporter :

![VM - exporter - phase 2](./img/vb-export-vm-2.png)

et on clique sur "Suivant >" :

![VM - exporter - phase 3](./img/vb-export-vm-3.png)

Il faut choisir l'emplacement de l'appareil à exporter et son format comme montrés
ci-dessous :

![VM - exporter - phase 4](./img/vb-export-vm-4.png)

Il suffit de remplir les métadonnées et de cliquer sur "Exporter" :

![VM - exporter - phase 5](./img/vb-export-vm-5.png)

![VM - exporter - phase 6](./img/vb-export-vm-6.png)

L'appareil est ainsi prêt à être déployer !

### Importation de la machine virtuelle via VirtualBox ###

Pour l'import, il suffit dans le menu "Fichier" de cliquer sur "Importer un
appareil virtuel..." :

![VM - importer - phase 1](./img/vb-import-vm-1.png)

Puis il faut choisir l'emplacement de l'appareil à importer (icône à droite),
naviguer et sélectionner le fichier :

![VM - importer - phase 2](./img/vb-import-vm-2.png)

On clique sur "Suivant >" :

![VM - importer - phase 3](./img/vb-import-vm-3.png)

Il faut mettre un nom à la machine virtuelle (par défaut, c'est le nom du
fichier suffixé par `_1`), puis cliquer sur "Importer" :

![VM - importer - phase 4](./img/vb-import-vm-4.png)

![VM - importer - phase 5](./img/vb-import-vm-5.png)

La machine virtuelle est prête à l'utilisation :

![VM - importer - phase 6](./img/vb-import-vm-6.png)


## Projet QGIS ##

### Mise en place du service côté serveur ###

Une fois en mesure de récupérer les objets géographiques depuis le thésaurus,
il est possible de les mettre en cartographie sous [QGIS](https://www.qgis.org/fr/site/).
Au final, le projet QGIS sera déployé via le QGIS Server pour avoir des
services de données géographiques `WMS`, `WFS`.
Il "suffira" alors d'un client Web pour obtenir une cartographie ... web !
Une telle solution consistera à utiliser [`Lizmap`](https://www.lizmap.com/) ou
[`qgis2web`](https://github.com/tomchadwin/qgis2web/wiki) par exemple.

Les données géographiques sont stockées comme spécifié dans le fichier de
configuration `/var/www/collexpersee/app/conf/config.json` dans la rubrique
`CACHE_DIR` :

```json
{
    ...
    "CACHE_DIR":"/var/www/html/projects/collex-persee",
    ...
}
```

le fichier est nommé via les paramètres trouvés dans
`/var/www/collexpersee/app/conf/thesaurus.json` :

```json
{
  "42": {
    "idg": "MT62",
    "th": "42",
    "name": "Modern Cairo Gazetteer",
    "url": "https://opentheso.huma-num.fr/",
    "last-update": "2019-09-27"
  }
}
```

soit : `/var/www/html/projects/collex-persee/MT62_42.geojson` pour les données
géographique.

Dans le même répertoire se trouve le thésaurus en format `RDF/XML` :
`/var/www/html/projects/collex-persee/MT62_42.xml`.


Un projet QGIS a été mis en place pour afficher les données ainsi récupérées.
Il est donc présent là :  `/var/www/html/projects/collex-persee/collex-persee.qgs`.

<!--
La cartographie s'appuie sur un jeu de symbôles SVG fourni par
[Mapbox](https://labs.mapbox.com/maki-icons/) sous licence [CC0](https://creativecommons.org/publicdomain/zero/1.0/).
 -->

![ColleX-Persée sous QGIS](img/host-qgis-project.png)

<!--
Il faut récupérer les [symbôles](https://github.com/mapbox/maki/zipball/master) pour installer
les icônes `mapbox-maki` en SVG dans `/var/www/html/QGIS/QGIS3/profiles/default/svg` :

```bash
$ cd /var/www/html/QGIS/QGIS3/profiles/default/svg
$ sudo wget -O maki.zip "https://github.com/mapbox/maki/zipball/master"
$ sudo unzip -x maki.zip
$ sudo rm maki.zip
$ sudo chown -R www-data:www-data mapbox-maki-*
```

Une fois l'installation des icônes effectuée, il faut passer à celle du projet
QGIS `collex-persee.qgs` dans `/var/www/html/projects/collex-persee` en
changeant le chemin d'accès aux icônes :

```bash
$ orig=$(grep mapbox-maki /var/www/html/projects/collex-persee/collex-persee.qgs | tail -1 | sed -e 's/^.*mapbox-maki-\([^/]*\).*$/\1/')
$ dest=$(ls -1 /var/www/html/QGIS/QGIS3/profiles/default/svg | sed -e 's/^mapbox-maki-\(.*\)$/\1')
$ sed -i -e "s/\(mapbox-maki-\)${orig}/\1${dest}/g" /var/www/html/projects/collex-persee/collex-persee.qgs
```
  -->

!!! note "Sauvegarde des projets QGIS 3.x"
    Par défaut, les projets avec l'extension `.qgz`. Ce n'est qu'une archive
    `zip` qui contient un fichier `.qgs` et un fichier `.qgd`. Seul le premier
    nous intéresse.

### Cartographie en local ###

Il est possible d'utiliser le projet QGIS en local (machine hôte) ou autre
machine avec le logiciel QGIS installé. Il suffit de recopier les données
géographiques en GeoJSON<!-- , d'installer les symbôles `Maki` comme précédemment
De modifier le chemin d'accès aux icônes (Cf. supra) -->., de récupérer le fichier
projet `.qgs` de la machine virtuelle. Données et projet QGIS **doivent** être
dans le même répertoire.

<!--
L'installation des symbôles peut s'effectuer
[directement](https://gis.stackexchange.com/questions/137855/importing-svg-symbols-into-qgis) depuis QGIS.
  -->

La récupération des données géographique via le service web d'interrogation
d'OpenTheso s'effectue ainsi :

```bash
$ scp invisu@192.168.99.100:/var/www/html/projects/collex-persee/MT62_42.geojson .
```

Il est possible de récupérer le projet QGIS de la même manière :

```bash
$ scp invisu@192.168.99.100:/var/www/html/projects/collex-persee/collex-persee.qgs .
```

On peut alors visualiser les données (lancer QGIS, menu "Fichier", sous-menu
"Ouvrir", choisir le fichier collex-persee.qgs) :

![ColleX-Persée sous QGIS](img/host-qgis-project-select.png)

Il est donc possible de modifier la symbologie et de sauvegarder le projet
QGIS. Une fois fait, il faut remettre le projet dans la machine virtuelle en
prenant soin de modifier la version de QGIS pour qu'elle soit identique à
celle de la machine virtuelle :

```bash
$ unzip collex-persee.qgz
```

Éditez le fichier `collex-persee.qgs` pour modifier (si besoin) la version de
QGIS (`3.10.0-A Coruña`) :

```xml
<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis version="3.10.0-A Coruña" projectname="Collex-Persée - les sites">
...
</qgis>
```

puis de copier le projet dans la machine virtuelle :

```bash
$ scp collex-persee.qgs invisu@192.168.99.100:/var/www/html/projects/collex-persee/
```

!!! tip "Modifier le fichier projet pour mettre à jour la cartographie"
    Il faut non seulement modifier la version comme décrit ci-dessus, mais
    aussi les chemins d'accès aux données pour que le fichier
    `collex-persee.qgs` et les données soient dans le même répertoire :
    ```xml
    ...
    <layer-tree-layer checked="Qt::Checked" id="MT62_42_2ac35ed2_706b_42a3_bd53_c666dce0fb93" expanded="0" source="./MT62_42.geojson" name="MT62_42" providerKey="ogr">
    ...
    <datasource>./MT62_42.geojson</datasource>
    ...
    ```

    Et ceux-ci pour toutes les sources de données.

### Cartographie en ligne ###

Il faut tout d'abord vérifier que le service d'accès aux cartes via le
service QGIS server est valide. Pour cela, il suffit de tester l'accès aux
métadonnées :

```bash
$ wget -S -O - "http://192.168.99.100/project/collex-persee/?SERVICE=WMS&REQUEST=GetCapabilities&VERSION=1.3.0&"
```

La commande précédente, lancée depuis une machine pouvant se connecter à la
machine virtuelle en fonctionnement, retourne un fichier XML de capacité du
service.

!!! tip "Les données géographiques doivent avoir été générées"
    Pour que le serveur QGIS retourne correctement les données, il faut avoir,
    au moins une fois, exécuté :

    ```
    $ wget -S -O - http://192.168.99.100/app/checknup/42
    ```

    ```
    $ wget -S -O - http://192.168.99.100/app/geojson/42
    ```

    La première commande vérifie que le cache est à jour et s'il ne l'est pas,
    le met à jour !
    La seconde commande génère les données au format `GeoJSON`.

Il est alors possible de "dessiner" les données via la requête :

```bash
$ wget -S -O map.png "http://192.168.99.100/project/collex-persee/?SERVICE=WMS&REQUEST=GetMap&VERSION=1.3.0&LAYERS=OSM%20Standard,CollEx-Perse&CRS=EPSG:4326&FORMAT=image/png&BBOX=29.9753,31.1326,30.1158,31.3306&WIDTH=800&HEIGHT=600"
```

![map.png](./img/map.png)

!!! todo "Symbologie"
    Sur l'image précédente, la symbologie mériterait d'être améliorée !

#### Accès aux objets ####

En plus du service WMS, QGIS serveur diffuse aussi les données en WFS 1.1.0.

Il faut tout d'abord vérifier que le service d'accès aux données via le
service QGIS server est valide. Pour cela, il suffit de tester l'accès aux
métadonnées :

```bash
$ wget -S -O - "http://192.168.99.100/project/collex-persee/?SERVICE=WFS&REQUEST=GetCapabilities&VERSION=1.1.0&"
```

### Partage de dossiers ###

Le partage de dossiers entre l'hôte et la machine virtuelle s'effectue en
deux étapes :

#### Déclarer le partage dans l'interface graphique ####

L'objectif est de choisir un dossier sur la machine hôte, dossier à
partager avec la machine virtuelle. Un nom sera donné à ce partage.

Un clic droit sur la machine virtuelle non démarrée, puis sélection du menu
"Configuration...", puis du sous-menu "Dossiers partagés" :

![VirtualBox - Partage - 1](img/vb-shared-folder-1.png)

Un clic sur l'icône en haut à droite :

![VirtualBox - Partage - 2](img/vb-shared-folder-2.png)

On choisit un chemin sur l'hôte :

![VirtualBox - Partage - 3](img/vb-shared-folder-3.png)

On n'oublie pas d'activer montage automatique :

![VirtualBox - Partage - 4](img/vb-shared-folder-4.png)

On valide (clic OK) :

![VirtualBox - Partage - 5](img/vb-shared-folder-5.png)

Le partage a pour nom `data`.

#### Monter automatiquement le partage ####

L'objectif est d'associer au partage précédent un dossier dans la machine
virtuelle. Ici, on veut monter le répertoire où les données géographique
et le projet QGIS sont stockés pour les rendre visible sur l'hôte.

Il faut démarrer la machine virtuelle et s'identifier en tant `invisu`.
Il faut bien vérifier que l'utilisateur `invisu` appartient au groupe `vboxsf`
sinon il faut l'ajouter au dit groupe :

```bash
$ [ $(grep invisu /etc/group | grep -c vboxsf) -ne 1 ] && sudo usermod -a -G vboxsf invisu
```

Il faut ensuite créer deux unités pour monter le répertoire
`/var/www/html/projects/collex-persee` avec le montage effectué
précédemment et faire ce montage automatiquement au démarrage :

```bash
$ sudo cat <<-EOF > /etc/systemd/system/var-www-html-projects-collex\x2dpersee.mount
[Unit]
Description=VirtualBox shared "data" folder

[Mount]
What=data
Where=/var/www/html/projects/collex-persee
Type=vboxsf
Options=defaults,noauto,uid=1000,gid=33

[Install]
WantedBy=multi-user.target
EOF
$ sudo cat <<-EOF > /etc/systemd/system/var-www-html-projects-collex\x2dpersee.automount
[Unit]
Description=Auto mount shared "data" folder

[Automount]
Where=/var/www/html/projects/collex-persee
DirectoryMode=0775

[Install]
WantedBy=multi-user.target
EOF
$ sudo systemctl enable var-www-html-projects-collex\x2dpersee.automount
$ sudo systemctl enable var-www-html-projects-collex\x2dpersee.mount
```

!!! note "Nom de l'unité"
    le nom du fichier contenant une unité `.mount` ou `.automount` s'obtient
    en utilisant la commande `systemd-escape` :

     ```
     $ systemd-escape -p /var/www/html/projects/collex-persee
     ```

     ```
     var-www-html-projects-collex\x2dpersee
     ```

Le montage est activé automatiquement dès qu'un accès au répertoire
`/var/www/html/projects/collex-persee` est fait (e.g. `ls
/var/www/html/projects/collex-persee`) ou que la machine virtuelle est
redémarrée !

Une fois fait, le dossier partagé côté hôte est vide. Il faut donc forcer le
chargement du thésaurus et des données géographiques sur la machine hôte :

```bash
$ cd /chemin/vers/dossier/partagé/hôte
$ wget -S -O - http://192.168.99.100/app/checknup/42
$ wget -S -O - http://192.168.99.100/app/geojson/42
$ cp /chemin/hôte/vers/collex-persee.qgs .
$ ls -1
collex-persee.qgs
MT62_42.geojson
MT62_42.xml
```

De cette façon, les données générées par le service d'interrogation sont
disponibles sur la machine hôte !
Il est ainsi possible d'y accéder directement avec le client QGIS.

