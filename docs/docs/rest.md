API REST de l'application
=========================

L'application expose un certain nombre de points d'accès, listés ci-dessous.
En cas d'erreur (`404` ou `50x`), l'application retourne une réponse en JSON
qui représente une exception :

```json
{
    "Exception": {
        "code" : 404|500,
        "locator" : "locator",
        "text" : "text",
        "message" : "message"
    }
}
```

`locator` et `text` sont optionnels. Leurs valeurs par défaut sont,
respectivement :

* nom de l'application Flask, soit `application Collex-Persée` ;
* `No information` .

Le message peut lui valoir `No tip` dans le cas où la cause de l'erreur serait
inconnue (ou que le développeur n'aurait pas donné les informations pour la
comprendre !)

Dans les points d'accès, le paramètre d'URL `{thid}` correspond à
l'identifiant du thésaurus dans le gazetier OpenTheso. Par exemple, `42` pour
le thésaurus du gazetier `Moder Cairo Gazetteer`.
La configuration des identifiants des gazetiers et thésaurus est effectuée
dans `conf/{development|production|testing}/thesaurii.json`.

Ainsi une configuration peut-être :

```json
{
  "42": {
    "idg": "MT62",
    "th": "42",
    "name": "Modern Cairo Gazetteer",
    "url": "https://opentheso.huma-num.fr/",
    "last-update": "2019-09-27"
  }
}
```

Elle indique :

* le thésaurus d'identifiant `42` est défini par :
    * son gazetier : `MT62` ;
    * son identifiant thésaurus : `42` ;
    * son nom de gazetier : `Modern Cairo Gazetteer` ;
    * son URL sur OpenTheso de base : `https://opentheso.huma-num.fr/` ;
    * sa date de dernière mise-à-jour connue.

Avant que la première requête ne soit exécutée, l'application charge les
thésaurus configurés aussi bien en mémoire que sur disque.
Le chargement du thésaurus sur disque consiste à le sauvegarder en cache dans
un fichier dont le nom est composé de l'identifiant du gazetier et de
l'identifiant du thésaurus :

```text
cache/{idg}_{th}.xml
```

En parallèle, les `skos:Concept` présents dans le thésaurus sont chargés en
mémoire pour éviter de les recharger depuis OpenTheso.

![Modèle UML skos:Concept](img/SKOScore.png)

_from https://www.seegrid.csiro.au/wiki/CGIModel/SKOSCoreModel SteveRichard - 2009-11-11_

Points d'accès
--------------

### /apispecs ###

Renvoie les spécifications des points d'accès de cette application
conformément à [OpenAPI](https://github.com/OAI/OpenAPI-Specification/blob/master/versions/3.0.3.md).

La réponse est un JSON.
Les points d'accès sont décrits aussi ci-après sous une forme plus facile à
lire pour l'oeil humain !

### / ###

Renvoie une bannière de l'application. La réponse est un JSON :

```json
{"banner":"Collex-Persée - accès au thésaurus"}
```

### /ping ###

Une requête simple pour vérifier que l'application fonctionne. La réponse est
un JSON :

```json
{"ping":"pong"}
```

### /gazeteers ###

Une requête pour récupérer les informations relatives aux gazetiers
accessibles par cette application. La réponse est un JSON (tableau de
gazetiers) :

```json
[{
    "idg": "identifiant du gazetier",
    "thid": "identifiant du thésaurus",
    "name": "nom du gazetier",
    "url": "point d'accès OpenTheso du gazetier",
    "last-update": "date de mise-à-jour dans l'application YYYY-MM-JJ"
},{
    ...
}]
```

### /checknup/{thid} ###

Vérifie si les données du thésaurus `thid` sont à jour en interrogeant
OpenTheso sur la date de dernière mise-à-jour.
Si les données ne sont pas à jour, alors le thésaurus est rechargé et la
réponse est un JSON :

```json
{<thid>:"updated","date":"%Y-%m-%d"}
```

Si les données sont à jour, la réponse est un JSON :

```json
{<thid>:"up-to-date","date":"%Y-%m-%d"}
```

### /search/{thid}?[directory={value[,value]}&][keyword={value[,value]}&][lon={value}&lat={value}&radius={value}&][lang=fr|en|iso|ala|ar|mul&] ###

Recherche des objets géo-localisés (via leur `skos:Concept`) qui répondent
au·x critère·s de recherche :

* `directory` : la valeur passée est recherchée dans `skos:prefLabel` des
  `skos:Concept` qui n'ont pas de géométrie. La valeur particulière `root`
récupère tous les objets géo-localisés et les sauvegarde en cache sous
`cache/{idg}_{th}.geojson`. La valeur peut être une liste de chaîne de
caractères séparée par une `,` ;
* `keyword` : la valeur passée est recherchée dans `skos:prefLabel` et
`skos:altLabel` des `skos:Concept` qui ont une géométrie. La valeur peut être
une liste de chaîne de caractères séparée par une `,` ;
* `lon`, `lat`, `radius` : les valeurs passées servent à retrouver les
`skos:Concept` qui ont une géométrie comprise dans un cercle de centre `lon`,
`lat` en degrés décimaux et de rayon `radius` en kilomètre ;
* `lang` permet de filtrer les réponses précédentes sur la langue
d'interrogation (utile pour `directory` et `keyword`) et en revoyant que les
propriétés exprimées dans cette langue.

La réponse est un GeoJSON :

```json
{ 
    "type": "FeatureCollection",
    "bbox": [,,,],
    "features": [
        ...
    ]
}
```

L'emprise des données (`bbox`) est calculée à partir des données trouvées.
Les objets géo-localisés sont dans `features`. Un objet géo-localisé est ainsi
codé en GeoJSON :

```json
{
    "geometry":{
        "coordinates":[,],
        "type":"Point"
    },
    "properties":{
        "id":"",
        "uri":"",
        "isA":"",
        "created":"",
        "modified":"",
        "exactMatch":"",
        "broadMatch":"",
        "relatedMatch":"",
        "related":"",
        "label_ala":"",
        "label_ar":"",
        "label_en":"",
        "label_fr":"",
        "label_iso":"",
        "label_mul":"",
        "altLabel_ala":"",
        "altLabel_ar":"",
        "altLabel_en":"",
        "altLabel_fr":"",
        "altLabel_iso":"",
        "altLabel_mul":"",
        "description_ala":"",
        "description_ar":"",
        "description_en":"",
        "description_fr":"",
        "description_iso":"",
        "description_mul":"",
        "editorialNote_ala":"",
        "editorialNote_ar":"",
        "editorialNote_en":"",
        "editorialNote_fr":"",
        "editorialNote_iso":"",
        "editorialNote_mul":"",
        "historyNote_ala":"",
        "historyNote_ar":"",
        "historyNote_en":"",
        "historyNote_fr":"",
        "historyNote_iso":"",
        "historyNote_mul":"",
        "note_ala":"",
        "note_ar":"",
        "note_en":"",
        "note_fr":"",
        "note_iso":"",
        "note_mul":"",
        "scopeNote_ala":"",
        "scopeNote_ar":"",
        "scopeNote_en":"",
        "scopeNote_fr":"",
        "scopeNote_iso":"",
        "scopeNote_mul":""
    },
    "type":"Feature"
}
```

Certaines propriétés peuvent avoir plusieurs valeurs, dans ce cas elles sont
séparées par un point-virgule `;`.

`id` est l'identifiant `dcterms:identifier`, alors que `uri` est l'attribut
`rdf:about` sur `skos:Concept`. `isA` est la concaténation des `skos:Concept`
qui définissent l'objet géo-localisé (`skos:broader`). Par exemple, `3227` signifie
que c'est une `école`.

### /feature/{thid}/{oid}[?lang=fr|en|iso|ala|ar|mul] ###

Récupère un objet géo-localisé par son identifiant `oid`. Le paramètre `lang`
permet de filtrer les attributs `*:{lang}` dans la réponse.

Le paramètre `oid` correspond à la propriété `id` des objets géo-localisés.

La réponse est codée en GeoJSON.

### /concepts/{thid} ###

Retourne l'arbre des `skos:Concept` sans géométrie, i.e. les concepts qui
définissent la sémantique des données liées.

La réponse est un JSON :

```json
{
    "name":"{idg}",
    "children":[
        {
            "name":"",
            "label":{
                "label_ala":"",
                "label_ar":"",
                "label_en":"",
                "label_fr":"",
                "label_iso":"",
                "label_mul":""
            },
            "children":[
                ...
            ]
        },
        ...
    ]
}
```

Cet arbre se termine par des `skos:Concept` qui ne contiennent que des objets
géo-localisés : ils n'ont donc pas de `children`.
Les `name` sont les `dcterms:identifier` des `skos:Concept`.

Cette structure JSON peut être consommée par
[`anytree`](https://anytree.readthedocs.io/en/latest/importer/jsonimporter.html) côté client.

### /validate/{thid} ###

Vérifie que le thésarus est valide. Il arrive en effet que certains
`skos:Concept` soit en géométrie **et** sans `skos:narrower`. Ils sont alors
classés en `no-geom` par leur `skos:identifier`. Il peut arriver aussi que
certains `skos:Concept` soit présents, mais non référencés en `skos:narrower`,
ils sont alors classés en `unref_concepts`. Enfin, il peut arriver que
certains `skos:Concept` soit référencé en `skos:broader` ou `skos:narrower`,
mais que la référence ne les lie pas en retour en `skos:narrower` ou
`skos:broader` respectivement, ils sont alors classés en
`missing_broader_or_narrower`.

La réponse est un JSON :

```json
{
    "missing_broader_or_narrower":[
        "",
        ""
    ],
    "no_geom":[
        "",
        ""
    ],
    "unref_concepts":[
        "",
        ""
    ]
}
```

### /geojson/{thid}[?lang=fr|en|iso|ala|ar|mul] ###

Ce point d'accès est un raccourci sur `/search/{thid}?[directory=root&`.
Il retourne donc le GeoJSON des objets géo-localisés du thésaurus `thid`.
Le type MIME de la réponse est `application/geo+json` conformément à
[IANA](https://www.iana.org/assignments/media-types/media-types.xhtml) .

### /perseeids/{thid}/{oid} ###

Permet d'obtenir les identifiants de l'objet `oid` du thésaurus `thid` dans
les [Perséïdes](http://info.persee.fr/perseides/) pour le corpus **Athar**
et dans [Persée](http://www.persee.fr).

Le paramètre `oid` correspond soit à la propriété `id` des objets
géo-localisés, soit à la propriété `uri`.

!!! note "Correspondance OpenTheso et GeoJSON"
    `rdf:about` qui est le handle OpenTheso correspond à la propriété `uri`
    dans l'objet géolocalisé.

    `terms:identifier` qui porte l'identifiant dans OpenTheso correspond à la
    propriété `id` dans l'objet géolocalisé.

La réponse est un JSON qui décrit les identifiants selon leur autorité
(Persée, Athar, BHE, etc.) :

```json
[
  {
    "type":"main",
    "authority":"persee",
    "identifier":"...",
    "uri":...
  },{
    "type":"external",
    "authority":"athar",
    "identifier":"...",
    "uri":...
  }
]
```

où `uri` peut valoir `null` (dans le cas de l'autorité Persée) ou un handle ;
l'`identifier` prend la valeur de l'identifiant dans l'`authority`.

Avec l'identifiant lié à Persée (`authority` vaut `persee`), il est possible
d'obtenir les informations sur l'objet :

* dans ATHAR via `https://athar.persee.fr/authority/{identifier}` ;
* dans Persée via  `https://www.persee.fr/authority/{identifier}`.

!!! todo "SPARQL endpoint Persée"
    Montrer comment interroger le SPARQL endpoint `data.persee.fr/sparql`

