# QGIS-server #

## Ajout d'une seconde carte réseau ##

Le besoin est de pouvoir dialoguer entre le système hôte et la machine
virtuelle.

Il faut se rendre alors dans la configuration de la machine virtuelle (clic
droit, menu "Configuration...", puis menu "Réseau") :

![VirtualBox - réseau privé hôte-invité](img/vb-conf2-network1.png)

![VirtualBox - réseau internet](img/vb-conf2-network2.png)

La machine virtuelle est démarrée.

Il faut se logguer sur la machine virtuelle en tant que `invisu`.

Puis, se mettre en tant qu'administrateur (`root`) de la machine virtuelle :

```bash
$ sudo -s
```

On donne le mot de passe de l'utilisateur `invisu` puisque ce dernier
appartient au groupe `sudo`.

Il convient alors de configurer ces deux cartes réseau. Le choix se porte
sur une adresse IP fixe pour la première et DCHP pour la seconde :

```bash
$ sudo cat /etc/network/interfaces <<-EOF >> /etc/network/interfaces
auto  enp0s3
iface enp0s3 inet static
address  192.168.99.100
network  192.168.99.0
netmask  255.255.255.0
dns-nameservers  8.8.8.8

allow-hotplug enp0s8
iface enp0s8 inet dhcp

EOF
$ sudo sed -i -e 's/\(nameserver\) .*/\1 8.8.8.8/' /etc/resolv.conf
```

Ici, l' adresse est fixée à `192.168.99.100`. Elle doit être différent de
celle retournée sur l'hôte :

```bash
$ ifconfig vboxnet0
```

!!! note "Adresses privées"
    | __Préfixe__  | __Plage IP__                | __Nombre d'adresses__ |
    |-------------:|----------------------------:|----------------------:|
    |    10.0.0.0/8|   10.0.0.0 – 10.255.255.255 |232-8 = 16 777 216     |
    | 172.16.0.0/12| 172.16.0.0 – 172.31.255.255 |232-12 = 1 048 576     |
    |192.168.0.0/16|192.168.0.0 – 192.168.255.255|232-16 = 65 536        |

La machine virtuelle est arrêtée, puis redémarrée. Il faut recommencer la
phase d'identification (`invisu`, puis `root`).

Il suffit de vérifier :

1. que le réseau est actif :

```bash
$ ping -c 1 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=63 time=11.0 ms

--- 8.8.8.8 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 11.014/11.014/11.014/0.000 ms
```

2. que l'on puisse se connecter à la machine virtuelle :

```bash
me@host:$ ssh invisu@192.168.99.100
The authenticity of host '192.168.99.100 (192.168.99.100)' can't be established.
ECDSA key fingerprint is SHA256:/AlSecs3Zdgd6H7ChDMZ8gHH2YH3grlP48A5i20uTsg.
Are you sure you want to continue connecting (yes/no)? yes
Warning: Permanently added '192.168.99.100' (ECDSA) to the list of known hosts.
invisu@192.168.99.100's password: 
Linux sig 4.19.0-6-amd64 #1 SMP Debian 4.19.67-2+deb10u1 (2019-09-20) x86_64

The programs included with the Debian GNU/Linux system are free software;
the exact distribution terms for each program are described in the
individual files in /usr/share/doc/*/copyright.

Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
permitted by applicable law.
Last login: Wed Oct 30 19:43:30 2019
invisu@sig:~$ 
```

_Nota Bene_: dans les explications qui suivent, il sera plus efficace de créer les
fichiers sur l'hôte, puis de la copier via `scp` sur la machine virtuelle :

```bash
me@host$ scp file invisu@192.168.99.100:/home/invisu
```

Côté machine virtuelle, les fichiers seront alors à déplacer au bon endroit,
et à les faire appartenir au bon utilisateur (`root` en général pour la
configuration du système).

## Préalable à l'installation de qgis-server ##

Pour installer la dernière version de `qgis-server` (buster propose 2.18 par
défaut), il faut rajouter la source `qgis.org` à la configuration d'`apt` de
la machine virtuelle :

```bash
$ sudo cat <<-EOF >/etc/apt/sources.list.d/qgis.list
deb https://qgis.org/debian buster main
deb-src https://qgis.org/debian buster main
EOF
$ sudo apt install gnupg -y
$ sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-key 51F523511C7028C3
$ sudo apt update && apt upgrade -y
```

La procédure est détaillée [là](https://qgis.org/en/site/forusers/alldownloads.html#debian-ubuntu).

Le serveur qgis aura besoin d'un serveur web avec une interface
[`FastCGI`](https://fr.wikipedia.org/wiki/FastCGI) pour dialoguer entre le
serveur web et `qgis-server` et d'un serveur d'affichage (dit serveur `X`)
pour permettre à `qgis-server` de procéder aux affichages demandés.

La procédure est décrite aussi
[là](https://docs.qgis.org/3.4/en/docs/user_manual/working_with_ogc/server/getting_started.html#installation-on-debian-based-systems)

### Installation d'un serveur web ###

Le choix se porte sur [nginx](https://nginx.org), serveur léger et facilement configurable.
L'installation est simple :

```bash
$ sudo apt install -y nginx
```
Une fois installé, un serveur web fonctionne sur `192.168.99.100` :

![Machine virtuelle - serveur web](img/vb-nginx-ok.png)

Pour faciliter la lecture de la configuration du serveur web, il est possible
d'ajouter un nom de serveur :

```bash
$ sudo sed -i -e "s/\(sig\)$/\1 webgis.collexpersee.fr/" /etc/hosts
```

### Installation de l'interface FastCGI ###

Le choix se porte sur
[`spawn-fcgi`](https://redmine.lighttpd.net/projects/spawn-fcgi/wiki) dans la
mesure où on ne souhaite pas que la configuration des projets exposés par
`qgis-server` soit relue à chaque requête :

```bash
$ sudo apt install -y spawn-fcgi
```

On ajoute le point d'accès à `qgis-server` dans la configuration de `nginx`,
désactive le poind d'accès par défaut, active le point d'accès `qgisserver` et
relance `nginx` :

```bash
$ cd /etc/nginx/sites-available
$ sudo cat <<-EOF > qgisserver
# Extract server name and port from HTTP_HOST, this
# is required because we are behind a VMs mapped port

map $http_host $parsed_server_name {
    default  $host;
    "~(?P<h>[^:]+):(?P<p>.*+)" $h;
}

map $http_host $parsed_server_port {
    default  $server_port;
    "~(?P<h>[^:]+):(?P<p>.*+)" $p;
}

upstream qgis_mapserv_backend {
    ip_hash;
    server unix:/run/qgis_mapserv2.sock;
    server unix:/run/qgis_mapserv1.sock;
}

server {
    listen 80 default_server;
    listen [::]:80 default_server;

    # SSL configuration
    #
    # listen 443 ssl default_server;
    # listen [::]:443 ssl default_server;

    # This is vital
    underscores_in_headers on;

    root /var/www/html;

    server_name webgis.collexpersee.fr;

    index index.html;

    recursive_error_pages on;
    error_page  403 404         = /40x.html;
    error_page  500 502 503 504 = /50x.html;

    # write access and error logs to /var/www/html/logs
    access_log /var/www/html/logs/qgisserver_access.log;
    error_log /var/www/html/logs/qgisserver_error.log;

    location / {
        # First attempt to serve request as file, then
        # as directory, then fall back to displaying a 404.
        try_files $uri $uri/ =404;
    }

    location ~ ^/project/([^/]+)/?(.*)$
    {
      set $qgis_project /var/www/html/projects/$1/$1.qgs;
      rewrite ^/project/(.*)$ /qgisserver/qgis_mapserv.fcgi last;
    }

    location /qgisserver/ {
        gzip           off;
        fastcgi_pass   qgis_mapserv_backend;

        # $http_host contains the original server name and port, such as: "localhost:8080"
        # QGIS Server behind a VM needs this parsed values in order to automatically
        # get the correct values for the online resource URIs

        fastcgi_param SERVER_NAME       $parsed_server_name;
        fastcgi_param SERVER_PORT       $parsed_server_port;

        # Set project file from env var
        fastcgi_param QGIS_PROJECT_FILE $qgis_project;

        include        fastcgi_params;
    }

}
EOF
$ cd ../sites-enabled
$ sudo rm default
$ sudo ln -s /etc/nginx/sites-available/qgisserver
$ sudo mkdir -p /var/www/html/{logs,projects,plugins}
$ sudo mkdir -p /var/www/html/QGIS/QGIS3/profiles/default/svg
$ sudo chown -R www-data.www-data /var/www/html
$ sudo systemctl restart nginx
```

_Nota Bene_ : l'accès au site web par défaut échoue (403) car le seul fichier
présent dans `/var/www/html` est `index.nginx-debian.html` (qu'il convient
d'ailleurs de retirer !)

Pour éviter que la page d'erreur 403 expose la version de `nginx`, il convient
de modifier `/etc/sites-available/qgisserver`, de détruire
`/var/www/html/index.nginx-debian.html` et d'y placer les fichiers `40x.html`
et `50x.html` :

```bash
$ sudo rm /var/www/html/index.nginx-debian.html
$ sudo cat <<-EOF > /var/www/html/40x.html
<!DOCTYPE html>
<html>
<head>
<title>Error</title>
<style>
    body {
        width: 35em;
        margin: 0 auto;
        font-family: Tahoma, Verdana, Arial, sans-serif;
    }
</style>
</head>
<body>
<h1>An error occurred.</h1>
<p>Sorry, the page you are looking for does currently not exist.<br/>Please try again later.</p>
</body>
</html>
EOF
$ sudo cat <<-EOF > /var/www/html/50x.html
<!DOCTYPE html>
<html>
<head>
<title>Error</title>
<style>
    body {
        width: 35em;
        margin: 0 auto;
        font-family: Tahoma, Verdana, Arial, sans-serif;
    }
</style>
</head>
<body>
<h1>An error occurred.</h1>
<p>Sorry, the page you are looking for is currently unavailable or forbidden.<br/>Please try again later.</p>
</body>
</html>
EOF
$ sudo chown www-data:www-data /var/www/html/*.html
$ sudo systemctl restart nginx
```

Cette fois le serveur web retourne la page voulue :

![Machine virtuelle - serveur web page d'erreur](img/vb-nginx-root.png)


Il faut ajouter une unité (fichier) pour `systemd` qui va démarrer l'interface
`FastCGI` au démarrage de la machine virtuelle
`/etc/systemd/system/qgisserver-fcgi@.socket`. On lancera autant de fois ce
script qu'il ya de coeurs dans la machine virtuelle (soit 2 fois) :
```
$ sudo cat <<-EOF >/etc/systemd/system/qgisserver-fcgi@.socket
# Path: /etc/systemd/system/qgisserver-fcgi@.socket
# systemctl enable qgisserver-fcgi@{1..2}.socket && systemctl start qgisserver-fcgi@{1..2}.socket

[Unit]
Description=QGIS Server FastCGI Socket (instance %i)
[Socket]
SocketUser=www-data
SocketGroup=www-data
SocketMode=0660
ListenStream=/run/qgis_mapserv%i.sock
[Install]
WantedBy=sockets.target
EOF
```

Puis, il faut créer une unité (fichier) pour `systemd` dans
`/etc/systemd/system/qgisserver-fcgi@.service`, puis il faut configurer le
système pour que ce script soit automatiquement lancé au démarrage de la
machine virtuelle (toujours autant de fois que de coeurs) :

```bash
$ sudo cat <<-EOF > /etc/systemd/system/qgisserver-fcgi@.service
# Path: /etc/systemd/system/qgisserver-fcgi@.service
# systemctl start qgisserver-fcgi@{1..4}.service

[Unit]
Description=QGIS Server Tracker FastCGI backend (instance %i)
[Service]
User=www-data
Group=www-data
ExecStart=/usr/lib/cgi-bin/qgis_mapserv.fcgi
StandardInput=socket
StandardOutput=syslog
StandardError=syslog
SyslogIdentifier=qgisserver-fcgi
WorkingDirectory=/tmp
Restart=always
# Environment
Environment="PROJ_LIB=/usr/share/proj"
Environment="GDAL_PATH=/usr/share/gdal"
Environment="QGIS_AUTH_DB_DIR_PATH=/var/www/html/projects"
Environment="QGIS_SERVER_LOG_FILE=/var/www/html/logs/qgisserver-fcgi.log"
Environment="QGIS_SERVER_LOG_LEVEL=0"
Environment="QGIS_DEBUG=1"
Environment="DISPLAY=:42"
Environment="QGIS_PLUGINPATH=/var/www/html/plugins"
Environment="QGIS_OPTIONS_PATH=/var/www/html/QGIS/QGIS3/profiles/default"
Environment="QGIS_CUSTOM_CONFIG_PATH=/var/www/html"

[Install]
WantedBy=multi-user.target
EOF
$ sudo systemctl enable qgisserver-fcgi@{1..2}.socket && systemctl start qgisserver-fcgi@{1..2}.socket
$ sudo systemctl enable qgisserver-fcgi@{1..2}.service && systemctl start qgisserver-fcgi@{1..2}.service
```

Il faut alors intaller un serveur `X` ; le choix se porte sur `xvfb` conseillé
par la communauté QGis :

```bash
$ sudo apt install xvfb -y
```

Il faut alors créer le fichier `/etc/systemd/system/xvfb.service` :

```bash
$ sudo cat <<-EOF > /etc/systemd/system/xvfb.service
[Unit]
Description=X Virtual Frame Buffer Service
Documentation=https://manpages.debian.org/buster/xvfb/Xvfb.1.en.html
After=network.target

[Service]
ExecStart=/usr/bin/Xvfb :42 -screen 0 1024x768x24 -noreset

[Install]
WantedBy=multi-user.target
EOF
$ sudo systemctl enable xvfb.service
$ sudo systemctl start xvfb.service
```

On retrouve le `:42` mis dans la configuration `/etc/systemd/system/qgisserver-fcgi@.service` !

On redémarre `nginx` pour lier le tout :
```bash
$ sudo systemctl restart nginx
```

## Installation de qgis-server ##

Il est temps d'installer le serveur :

```bash
$ sudo apt install qgis-server -y
```

Il est alors possible d'interroger `qgis-server` sur
`192.168.99.100/qgisserver/qgis_mapserv.fcgi?` :

![Machine virtuelle - QGIS serveur - pas de projet](img/vb-qgisserver-noprojects.png)

Ce qui est normal car aucun projet QGIS n'a été exporté !

Il faut donc récupérer le jeu de données d'entrainement de QGIS sur la machine
hôte (63.3 Mo), ne prendre que le jeu de données `world` (15 Mo) et le copier
dans la machine virtuelle :

```bash
$ wget https://github.com/qgis/QGIS-Training-Data/archive/v2.0.zip
$ unzip -C QGIS-Training-Data-2.0.zip "**/world/*"
$ mv QGIS-Training-Data-2.0/exercise_data/world/ .
$ rmdir QGIS-Training-Data-2.0/exercise_data/
$ rmdir QGIS-Training-Data-2.0/
```

Il est nécessaire de remplir les informations pour pouvoir publier la couche.
Pour cela, il faut ouvrir le projet `world.qgs` avec `QGIS`, zoomer sur
l'emprise de la couche, puis aller dans les propriétés du projet (Menu
`Projet` > `Propriétés...`) et remplir les onglets `Métadonnées`, `SRC` (en
choisissant `WGS84`) et `QGIS Server`. Dans ce dernier onglet, cliquer sur
`Utiliser l'emprise actuelle du canvas` pour obtenir l'emprise des données (ce
sera la bonne puisqu'on a zoomé préalablement dessus) après avoir activé
`Advertised extent` dans le bloc `Capacités WMS`. On activera aussi dans les
`Capacités WFS` la couche `continents` :

![QGIS - WFS - continents](img/qgis-wfs-activated-layers.png)

Une fois toutes les informations entrées (on vérifiera que l'onglet
`Validation` de l'onglet `Métadonnées` affiche bien : _OK, cela semble valide selon le schéma QGIS._).
On clique sur "Ok" pour terminer cette phase et fermer les propriétés du
projet.

Ensuite, il est nécessaire de "colorier" la carte. Pour cela, il suffit
d'aller dans les propriétés de la couche, `Symbologie`, choisir `Catégorisé`,
puis pour Valeur choisir le champ `ISO` et cliquer sur `Classer` en bas de
l'écran. On clique sur "Ok" pour terminer cette phase et fermer les propriétés
de la couche `continents`.

Puis, le projet sera sauvegardé (`Projet` > `Enregistrer`).
On obtient une carte équivalente (les couleurs précédentes étant choisies
aléatoirement) :

![QGIS - Affichage de la carte](img/qgis-display-world.png)


Il faut alors transférer le projet dans la machine virtuelle :

```bash
$ zip -r world.zip ./world/
$ rm -fr ./world
$ scp ./world.zip invisu@192.169.99.100:
$ rm ./world.zip
```

Le fichier `world.zip` est maintenant dans `~invisu` de la machine virtuelle.
Il faut retourner dans cette dernière pour finir le déploiement du projet.
Il faut installer `unzip` pour le désarchiver, le déarchiver et détuire
l'archive :

```bash
$ sudo apt install unzip
$ unzip world.zip
$ rm world.zip
$ sudo cp -r world /var/www/html/projects/
$ sudo chown -R www-data:www-data /var/www/html/projects/world
```

### Premiers tests sur les métadonnées ###

* `http://192.168.99.100/qgisserver/qgis_mapserv.fcgi?MAP=/var/www/html/projects/world/world.qgs&SERVICE=WMS&REQUEST=GetCapabilities&VERSION=1.3.0&`

  Les métadonnées sont retournées (elles contiennent les informations entrées
  précédemment dans les propriétés du projet et de la couche) :

```xml
<?xml version="1.0" encoding="utf-8"?>
<WMS_Capabilities xmlns:sld="http://www.opengis.net/sld" xmlns="http://www.opengis.net/wms" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.3.0" xmlns:qgs="http://www.qgis.org/wms" xsi:schemaLocation="http://www.opengis.net/wms http://schemas.opengis.net/wms/1.3.0/capabilities_1_3_0.xsd http://www.opengis.net/sld http://schemas.opengis.net/sld/1.1.0/sld_capabilities.xsd http://www.qgis.org/wms http://192.168.99.100/qgisserver/qgis_mapserv.fcgi?MAP=/var/www/html/projects/world/world.qgs&amp;SERVICE=WMS&amp;REQUEST=GetSchemaExtension">
 <Service>
  <Name>WMS</Name>
  <Title>world</Title>
  <Abstract><![CDATA[test qgisserver]]></Abstract>
  <KeywordList>
   <Keyword vocabulary="ISO">infoMapAccessService</Keyword>
   <Keyword>administrative boundaries</Keyword>
  </KeywordList>
  <OnlineResource xmlns:xlink="http://www.w3.org/1999/xlink" xlink:type="simple" xlink:href="http://192.168.99.100/qgisserver/qgis_mapserv.fcgi?MAP=/var/www/html/projects/world/world.qgs"/>
  <ContactInformation>
   <ContactPersonPrimary>
    <ContactPerson>me</ContactPerson>
    <ContactOrganization>test</ContactOrganization>
    <ContactPosition>distributor</ContactPosition>
   </ContactPersonPrimary>
  </ContactInformation>
  <Fees>no conditions apply</Fees>
  <AccessConstraints>copyright</AccessConstraints>
 </Service>
 <Capability>
  <Request>
   <GetCapabilities>
    <Format>text/xml</Format>
    <DCPType>
     <HTTP>
      <Get>
       <OnlineResource xmlns:xlink="http://www.w3.org/1999/xlink" xlink:type="simple" xlink:href="http://192.168.99.100/qgisserver/qgis_mapserv.fcgi?MAP=/var/www/html/projects/world/world.qgs&amp;"/>
      </Get>
     </HTTP>
    </DCPType>
   </GetCapabilities>
   <GetMap>
    <Format>image/jpeg</Format>
    <Format>image/png</Format>
    <Format>image/png; mode=16bit</Format>
    <Format>image/png; mode=8bit</Format>
    <Format>image/png; mode=1bit</Format>
    <Format>application/dxf</Format>
    <DCPType>
     <HTTP>
      <Get>
       <OnlineResource xmlns:xlink="http://www.w3.org/1999/xlink" xlink:type="simple" xlink:href="http://192.168.99.100/qgisserver/qgis_mapserv.fcgi?MAP=/var/www/html/projects/world/world.qgs&amp;"/>
      </Get>
     </HTTP>
    </DCPType>
   </GetMap>
   <GetFeatureInfo>
    <Format>text/plain</Format>
    <Format>text/html</Format>
    <Format>text/xml</Format>
    <Format>application/vnd.ogc.gml</Format>
    <Format>application/vnd.ogc.gml/3.1.1</Format>
    <Format>application/json</Format>
    <Format>application/geo+json</Format>
    <DCPType>
     <HTTP>
      <Get>
       <OnlineResource xmlns:xlink="http://www.w3.org/1999/xlink" xlink:type="simple" xlink:href="http://192.168.99.100/qgisserver/qgis_mapserv.fcgi?MAP=/var/www/html/projects/world/world.qgs&amp;"/>
      </Get>
     </HTTP>
    </DCPType>
   </GetFeatureInfo>
   <sld:GetLegendGraphic>
    <Format>image/jpeg</Format>
    <Format>image/png</Format>
    <DCPType>
     <HTTP>
      <Get>
       <OnlineResource xmlns:xlink="http://www.w3.org/1999/xlink" xlink:type="simple" xlink:href="http://192.168.99.100/qgisserver/qgis_mapserv.fcgi?MAP=/var/www/html/projects/world/world.qgs&amp;"/>
      </Get>
     </HTTP>
    </DCPType>
   </sld:GetLegendGraphic>
   <sld:DescribeLayer>
    <Format>text/xml</Format>
    <DCPType>
     <HTTP>
      <Get>
       <OnlineResource xmlns:xlink="http://www.w3.org/1999/xlink" xlink:type="simple" xlink:href="http://192.168.99.100/qgisserver/qgis_mapserv.fcgi?MAP=/var/www/html/projects/world/world.qgs&amp;"/>
      </Get>
     </HTTP>
    </DCPType>
   </sld:DescribeLayer>
   <qgs:GetStyles>
    <Format>text/xml</Format>
    <DCPType>
     <HTTP>
      <Get>
       <OnlineResource xmlns:xlink="http://www.w3.org/1999/xlink" xlink:type="simple" xlink:href="http://192.168.99.100/qgisserver/qgis_mapserv.fcgi?MAP=/var/www/html/projects/world/world.qgs&amp;"/>
      </Get>
     </HTTP>
    </DCPType>
   </qgs:GetStyles>
  </Request>
  <Exception>
   <Format>XML</Format>
  </Exception>
  <sld:UserDefinedSymbolization RemoteWCS="0" InlineFeature="0" UserLayer="0" UserStyle="1" SupportSLD="1" RemoteWFS="0"/>
  <Layer queryable="1">
   <Title>world</Title>
   <Abstract>world</Abstract>
   <CRS>CRS:84</CRS>
   <CRS>EPSG:4326</CRS>
   <CRS>EPSG:3857</CRS>
   <EX_GeographicBoundingBox>
    <westBoundLongitude>-189</westBoundLongitude>
    <eastBoundLongitude>189</eastBoundLongitude>
    <southBoundLatitude>-86.5575</southBoundLatitude>
    <northBoundLatitude>114.277</northBoundLatitude>
   </EX_GeographicBoundingBox>
   <BoundingBox minx="-2.00142e+07" maxx="2.00142e+07" miny="-2.23545e+07" maxy="2.60839e+07" CRS="EPSG:3857"/>
   <BoundingBox minx="-86.5575" maxx="114.277" miny="-189" maxy="189" CRS="EPSG:4326"/>
   <Name>world</Name>
   <KeywordList>
    <Keyword vocabulary="ISO">infoMapAccessService</Keyword>
    <Keyword>administrative boundaries</Keyword>
   </KeywordList>
   <Layer queryable="1">
    <Name>continents</Name>
    <Title>countries</Title>
    <Abstract>world administrative boundaries</Abstract>
    <CRS>CRS:84</CRS>
    <CRS>EPSG:4326</CRS>
    <CRS>EPSG:3857</CRS>
    <EX_GeographicBoundingBox>
     <westBoundLongitude>-180</westBoundLongitude>
     <eastBoundLongitude>180</eastBoundLongitude>
     <southBoundLatitude>-55.9148</southBoundLatitude>
     <northBoundLatitude>83.6343</northBoundLatitude>
    </EX_GeographicBoundingBox>
    <BoundingBox minx="-2.00375e+07" maxx="1.9236e+07" miny="-7.54147e+06" maxy="1.84291e+07" CRS="EPSG:3857"/>
    <BoundingBox minx="-55.9148" maxx="83.6343" miny="-180" maxy="180" CRS="EPSG:4326"/>
    <KeywordList>
     <Keyword>administrative</Keyword>
    </KeywordList>
    <Style>
     <Name>défaut</Name>
     <Title>défaut</Title>
     <LegendURL>
      <Format>image/png</Format>
      <OnlineResource xmlns:xlink="http://www.w3.org/1999/xlink" xlink:type="simple" xlink:href="http://192.168.99.100/qgisserver/qgis_mapserv.fcgi?MAP=/var/www/html/projects/world/world.qgs&amp;SERVICE=WMS&amp;VERSION=1.3.0&amp;REQUEST=GetLegendGraphic&amp;LAYER=continents&amp;FORMAT=image/png&amp;STYLE=défaut&amp;SLD_VERSION=1.1.0"/>
     </LegendURL>
    </Style>
   </Layer>
  </Layer>
 </Capability>
</WMS_Capabilities>
```

Cette requête peut aussi s'écrire ainsi :

* `http://192.168.99.100/project/world/?SERVICE=WMS&REQUEST=GetCapabilities&VERSION=1.3.0&`

  gràce à la configuration effectuée dans `nginx`.

### Affichage de la carte des pays ###

* `http://192.168.99.100/qgisserver/qgis_mapserv.fcgi?SERVICE=WMS&REQUEST=GetMap&VERSION=1.3.0&MAP=/var/www/html/projects/world/world.qgs&LAYERS=continents&CRS=CRS:84&FORMAT=image/png&BBOX=-180,-55.9148,180,83.6343&WIDTH=800&HEIGHT=400`

On obtient alors une image png semblable à l'affichage obtenu dans QGIS
précédemment :

![QGIS server - WMS - carte du monde](img/world-continents.png)

Cette requête peut aussi s'écrire ainsi :

* `http://192.168.99.100/project/world/?SERVICE=WMS&REQUEST=GetMap&VERSION=1.3.0&LAYERS=continents&CRS=CRS:84&FORMAT=image/png&BBOX=-180,-55.9148,180,83.6343&WIDTH=800&HEIGHT=400`

Le serveur `qgis-server` est opérationnel !

