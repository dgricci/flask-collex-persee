% Debian Buster VirtualBox  
% Didier Richard - IGN/ENSG/CC TSI  
% 2019/10/12

---

# Fabriquer une VM Debian 10 à partir d'une image ISO #


## Récupération de l'image ISO Debian ##

On se rend sur la [page de téléchargement](https://www.debian.org/CD/http-ftp/#stable)
pour récupérer plutôt une image CD (les images DVD contiennent aussi les
fichiers nécessaires pour monter une VM, mais sont plus lourdes à
télécharger).

On récupère donc [Debian 10.1.0](http://debian.univ-lorraine.fr/debian-cd/10.1.0/amd64/iso-cd/debian-10.1.0-amd64-netinst.iso)
après avoir choisi un [miroir](http://debian.univ-lorraine.fr/debian-cd/), puis une distribution
(ici [amd64, CD, 10.1.0](http://debian.univ-lorraine.fr/debian-cd/10.1.0/amd64/iso-cd/)).


## Création de la VM ##


### Création d'une VM "vierge" ###

On lance [VirtualBox](https://www.virtualbox.org/) :

![VirtualBox écran d'acceuil](./img/vb-accueil.png)

On lance une nouvelle configuration :

![VirtualBox nouvelle configuration](./img/vb-nouvelle.png)

On donne un nom (par exemple, `InVisuCollex`), un système d'exploitation
`Linux` et sa version `Debian (64-bit)` et on passe à l'écran suivant :

![VirtualBox nom et OS](./img/vb-nameos.png)

On met au moins 4G de RAM et on passe à l'écran suivant :

![VirtualBox 4G RAM](./img/vb-ram.png)

Pour la création du disque dur, il faut "Créer un disque dur virtuel
maintenant" et on créé la machine virtuelle en cliquant sur "Créer" :

![VirtualBox Disque dur](./img/vb-virtualdd.png)

On choisit alors l'option "VDI (Image Disque VirtualBox)", puis on clique sur
"Suivant >" :

![VirtualBox VDI](./img/vb-typedd.png)

On choisit "Dynamiquement alloué", puis on clique sur "Suivant >" :

![VirtualBox Allocation dynamique](./img/vb-dynamicdd.png)

Enfin, on choisit l'emplacement du disque dur virtuel en cliquant sur l'icône
de dossier à droite et naviguant (de préférence choisir le dossier créé par
VirtualBox de nom `InVisuCollex` comme dossier), puis on finit le processus de
création en cliquant sur "Créer" :

![VirtualBox Création](./img/vb-locationdd.png)

On obtient alors une machine virtuelle, presque prête à l'emploi :

![VirtualBox nouvellement créée](./img/vb-créée.png)

Il faut alors la configurer plus avant en appuyant sur le bouton droit, sous-menu
"Configuration..." :

![VirtualBox Configuration supplémentaire](./img/vb-conf0.png)

Dans le menu "Système", on désactive la disquette et on vérifie que la mémoire
est bien de 4 Go de RAM :

![VirtualBox Système](./img/vb-carte-mère.png)

Dans le menu "Processeur", on sélectionne 2 CPU (virtual-CPU) si la machine
hôte en dispose de plusieurs (ce qui est couramment le cas) :

![VirtualBox VCPU](./img/vb-proc.png)

Dans le menu "Stockage", on monte le CD iso téléchargé auparavant :

![VirtualBox Stockage - étape 1](./img/vb-stockage1.png)

![VirtualBox Stockage - étape 2](./img/vb-stockage2.png)

![VirtualBox Stockage - étape 3](./img/vb-stockage3.png)

![VirtualBox Stockage - étape 4](./img/vb-stockage4.png)

Dans le menu "Son", on désactive le son, inutile sur un serveur :

![VirtualBox pas de son](./img/vb-nosound.png)

Dans le menu "Réseau", on active un réseau NAT permettant à la machine
virtuelle de sortir sur internet via la machine hôte :

![VirtualBox réseau internet](./img/vb-internet.png)

On appuie alors sur "Ok" pour terminer la seconde phase de configuration.

![VirtualBox Configuration phase 1](./img/vb-conf1.png)


### Installation du système virtualisé ###

On <a name="openvm"></a>lance la machine virtuelle :

![VirtualBox démarrage](./img/vb-démarrage.png)

L'écran d'accueil est alors (au bout d'un certain temps) affiché :

![VirtualBox en fonction - accueil de l'installation](./img/vb-démarrée-accueil-install.png)

On sélectionne "Graphical install" en appuyant sur la touche "Entrée".
On choisit la langue française ("French"), puis on appuie sur "Continue" :

![VirtualBox en fonction - choix de la langue](./img/vb-démarrée-choix-langue.png)

On choisit le pays ("France"), puis on appuie sur "Continuer" :

![VirtualBox en fonction - choix du pays](./img/vb-démarrée-choix-pays.png)

On configure le clavier ("France"), puis on appuie sur "Continuer" :

![VirtualBox en fonction - choix du clavier](./img/vb-démarrée-choix-clavier.png)

On configure le réseau avec le nom de la machine (qui pourra être changé plus
tard). On entre "sig", puis on appuie sur "Continuer" :

![VirtualBox en fonction - nom de la machine](./img/vb-démarrée-nom-machine.png)

On configure le réseau avec le nom du domaine qu'on laisse vide (et qui pourra
être changé plus tard), puis on appuie sur "Continuer" :

![VirtualBox en fonction - nom du domaine](./img/vb-démarrée-nom-domaine.png)

On définit le mot de passe de l'administrateur (utilisateur "root") :

![VirtualBox en fonction - mot de passe administrateur](./img/vb-démarrée-root-passwd.png)

Mettre le mot de passe de l'administrateur dans la description de la VM
(bouton droit, configuration..., Général, onglet Description) :

![VirtualBox en fonction - description administrateur](./img/vb-démarrée-description1.png)

On définit alors le nom complet de l'utilisateur de cette VM ("InVisu SIG"), puis
on appuie sur "Continuer" :

![VirtualBox en fonction - utilisateur](./img/vb-démarrée-utilisateur.png)

On crée alors le compte Unix de cet utilisateur ("invisu"), puis on appuie sur
"Continuer" :

![VirtualBox en fonction - compte utilisateur](./img/vb-démarrée-user.png)

On définit le mot de passe de l'utilisateur, puis on appuie sur "Continuer" :

![VirtualBox en fonction - mot de passe utilisateur](./img/vb-démarrée-user-passwd.png)

Mettre le mot de passe de l'utilisateur dans la description de la VM 
(bouton droit, configuration..., Général, onglet Description) :

![VirtualBox en fonction - description utilisateur](./img/vb-démarrée-description2.png)

L'installation débute ...

On partitionne le disque dur (on choisit "Assisté - utiliser tout un disque
avec LVM". On pourrait aussi choisir "Assisté - utiliser un disque entier"),
puis on appuie sur "Continuer" :

![VirtualBox en fonction - disque entier LVM](./img/vb-démarrée-fulldisk-lvm.png)

L'unique partition est sélectionnée, on appuie sur "Continuer" :

![VirtualBox en fonction - disque entier sda](./img/vb-démarrée-fulldisk-sda.png)

On choisit de tout mettre dans une seule partition, on appuie sur "Continuer" :

![VirtualBox en fonction - disque entier 1 partition](./img/vb-démarrée-fulldisk-1partition.png)

On valide le partitionnement ("Oui"), on appuie sur "Continuer" :

![VirtualBox en fonction - disque entier validation](./img/vb-démarrée-fulldisk-ok.png)

On valide la taille de la partition en appuyant sur "Continuer" :

![VirtualBox en fonction - disque entier taille](./img/vb-démarrée-fulldisk-size.png)

Le disque est partitionnée ...

On sauvegarde le disque ainsi partitionné ("Oui"), on appuie sur "Continuer" :

![VirtualBox en fonction - disque entier validation finale](./img/vb-démarrée-fulldisk-finalok.png)

L'installation continue par le système de base ...

L'installation via `apt` va nécessiter de configuration les entrepôts Debian.
On répond "Non" à l'insertion d'un autre CD/DVD, on appuie sur "Continuer" :

![VirtualBox en fonction - paquet - pas CD/DVD](./img/vb-démarrée-apt-nocd.png)

L'archive Debian est alors proposée ("France"), on appuie sur "Continuer" :

![VirtualBox en fonction - paquet - miroir France](./img/vb-démarrée-apt-france-mirror.png)

On choisit un miroir, on appuie sur "Continuer" :

![VirtualBox en fonction - paquet - choix miroir](./img/vb-démarrée-apt-choix-miroir.png)

Si besoin, on entre le proxy, on appuie sur "Continuer" :

![VirtualBox en fonction - paquet - proxy](./img/vb-démarrée-apt-proxy.png)

La configuration du miroir s'effectue ... _(patience)_

On participe aux statistiques d'utilisation des paquets, on appuie sur "Continuer" :

![VirtualBox en fonction - paquet - statistiques](./img/vb-démarrée-apt-stats.png)

On choisit que des paquets de base, pas de graphique ("serveur SSH" et
"utilitaires usuels du système"), on appuie sur "Continuer" :

![VirtualBox en fonction - paquet - base](./img/vb-démarrée-apt-base.png)

On patiente (il n'y a que 150 paquets environ à installer) ...

On installe alors GRUB,  on appuie sur "Continuer" :

![VirtualBox en fonction - GRUB](./img/vb-démarrée-grub.png)

Puis, on choisir le disque dur pour GRUB,  on appuie sur "Continuer" :

![VirtualBox en fonction - GRUB disque d'amorçage](./img/vb-démarrée-grub-sda.png)

La fin de l'installation des composants est atteinte, on appuie sur "Continuer" :

![VirtualBox en fonction - fin de l'installation](./img/vb-démarrée-fin-install.png)

Le système va redémarrer, le CD ISO sera démonté avant.

![Machine virtuelle prête](./img/vb-ready.png)

On <a name="closevm"></a>éteint alors la VM :

![Machine virtuelle - éteindre 1ère étape ](./img/vb-close1.png)

On choisit "Fichier" > "Fermer ..." et "Éteindre la machine", on appuie sur
"Ok" :

![Machine virtuelle - éteindre 2ième étape ](./img/vb-close2.png)


### Ajout des additions Linux ###

Ces additions sont nécessaires pour partager un dossier entre la machine hôte
et la VM. Si on ajoute un serveur X dans la VM, on pourra aussi intégrer la
souris dans la VM, et copier-coller entre l'hôte et la VM.

On commence donc par chercher dans le [centre de téléchargement](https://download.virtualbox.org/virtualbox/)
de VirtualBox, la version de VirtualBox utilisée (qu'on appellera
<majeur>.<mineur>.<patch>) :

![VirtualBox - version](./img/vb-virtualbox-version.png)

On télécharge le fichier `VBoxGuestAdditions_<majeur>.<mineur>.<patch>.iso`

On procède comme pour le CD ISO de Debian 10.1.0 en montant ce CD, on appuie
sur "Ok" :

![VirtualBox - Additions invitées - insertion du CD](./img/vb-guest-iso.png)

On [démarre](vm0.md#openvm) alors la VM, on se loggue "root" :

![VirtualBox - Additions invitées - root](./img/vb-root-logged.png)

On met à jour la base des paquets et les paquets (c'est une installation toute
fraîche, il ne devrait pas y avoir de mises-à-jour, mais c'est une bonne
pratique) :

```bash
$ apt update && apt upgrade -y
```

On installe le paquet `dkms` qui va entraîner l'installation des en-têtes
linux (`linux-headers-$(uname -r)` et de `build-essential`) :

```bash
$ apt install dkms -y
```

Ça prend un peu de temps, _patience ..._

On monte le CD ISO des additions invités :

```bash
$ mount /media/cdrom
```

Un message indique que `/media/cdrom0` est monté en lecture seule.
On lance alors la compilation des modules `vbox` :

```bash
$ sh /media/cdrom/VBoxLinuxAdditions.run
```

Ça prend un peu de temps !

![VirtualBox - Additions invitées - installation](./img/vb-guest-run.png)

On voit d'ailleurs que le module `vboxsf` n'est pas installé (en fait, om
semble manquer des fonctions VBox*). On se déloggue, [ferme](vm0.md#closevm) la VM,
la [redémarre](vm0.md#openvm), se reloggue "root", remonte le CD ISO des additions
invités et relance la compilation :

![VirtualBox - Additions invitées - ré-installation](./img/vb-guest-rerun.png)

Plus aucune erreur !!
On éteint à nouveau la VM, retire le CD ISO des additions invités et on
redémarre la VM, se loggue "root" et on vérifie que les modules "vbox" sont
bien présents :

![VirtualBox - Additions invitées - installation ok](./img/vb-guest-ok.png)


### Divers ###


#### Ajout des paquets contrib et non-free ###

On profite d'être sous "root" pour ajouter les archives de paquets `contrib`
et `non-free` sur `buster` et `contrib` sur `buster/updates` et
`buster-updates` :

```bash
$ sed -i -e 's:^\(deb.*\)\( buster main\)$:\1\2 contrib non-free:' /etc/apt/sources.list
$ sed -i -e 's:^\(deb.*\)\( buster[-/]updates main\)$:\1\2 contrib:' /etc/apt/sources.list
```

On met à jour l'index des paquets :

```bash
$ apt update
```

#### Ajout de l'utilisateur à certains groupes ####

L'ajout au groupe `sudo` permet à l'utilisateur `invisu` d'avoir les droits
d'administration car le fichier `/etc/sudoers` contient la directive suivante
:

```bash
%sudo   ALL=(ALL:ALL) ALL
```

L'ajout au groupe `www-data` permet à l'utilisateur `invisu` d'avoir des
droits sur les répertoires déployés par les services web.

```bash
$ usermod -a -G vboxsf,www-data,sudo invisu
$ usermod -g www-data invisu
```

#### Désactivation de l'identification par root ####

L'accès par `ssh` a la machine virtuelle doit être interdite pour
l'administrateur de la machine puisque l'accès via `invisu` est autorisé et
que cet utilisateur appartient au groupe `sudo` pour administrer la machine :

```bash
$ sed -i -e "s/^#\(PermitRootLogin\) .*/\1 no/" /etc/ssh/sshd_config
$ systemctl restart sshd
```

#### Installation d'un pare-feu ####

L'objectif est de bloquer tout accès non souhaité sur la machine virtuelle ;
seuls sont (pour l'instant) autorisés :

* `ssh` : identification à distance à la machine virtuelle (tcp/22) ;
* `http` : accès aux services web de la machine virtuelle (tcp/80) ;
* `https` : accès aux services web sécurisés de la machine virtuelle (tcp/443).

Toujours en tant que `root`, le pare-feu est installé et les accès autorisés
mis en place :

```bash
$ apt install ufw -y
$ ufw allow ssh
$ ufw allow http
$ ufw allow 443/tcp
$ ufw --force enable
$ ufw status
```

On peut alors se délogguer et fermer la VM.

