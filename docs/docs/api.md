# API documentation

::: config.Config
    :docstring:
    :members:

::: application.main.routes.banner
    :docstring:

::: application.main.routes.ping
    :docstring:

::: application.main.routes.check_update
    :docstring:

::: application.main.routes.search
    :docstring:

::: application.main.routes.get_feature_by_id
    :docstring:

::: application.main.routes.get_all_concepts
    :docstring:

::: application.main.routes.validate
    :docstring:

::: application.main.routes.get_all_features_geojson
    :docstring:

::: application.xmlrdf.rdfparser.RdfParser
    :docstring:
    :members:

::: application.xmlrdf.xmltogeojson.id_from_uri
    :docstring:

::: application.xmlrdf.xmltogeojson.set_properties_i18n
    :docstring:

::: application.xmlrdf.xmltogeojson.concept_to_feature
    :docstring:

::: application.errors.handlers.not_found_error
    :docstring:

::: application.errors.handlers.internal_error
    :docstring:

::: application.errors.responses.error_response
    :docstring:

::: application.errors.responses.bad_request_response
    :docstring:

