Contenu du gazetier Collex-Persée
=================================

Le thésaurus contient des `skos:Concept` qui représentent :

* soit un classe sémantique avec les règles associées ;
* soit un objet du terrain avec une localisation en coordonnées géographiques.

L'objectif est donc double : obtenir la hiérarchie des sens et les objets
géographiques.

Hiérarchie des sens
-------------------

La hiérarchie débute avec le thésaurus qui contient des informations :

* architecture civile ;
* places ;
* architecture militaire ;
* etc.

Les `places` sont instanciées par des objets. Dans l'exemple ci-dessous, le
concept de `places` et une instanciation géo-localisée :

```xml
<rdf:Description rdf:about="https://hdl.handle.net/20.500.11942/crtzryuppgd6o">
    <rdf:type rdf:resource="http://www.w3.org/2004/02/skos/core#Concept"/>
    <skos:prefLabel xml:lang="fr">places</skos:prefLabel>
    <skos:prefLabel xml:lang="en">squares (open spaces)</skos:prefLabel>
    <skos:narrower rdf:resource="https://hdl.handle.net/20.500.11942/crt1pv73yya17"/>
    <skos:narrower rdf:resource="https://hdl.handle.net/20.500.11942/crtxuqnw23n9m"/>
    <skos:narrower rdf:resource="https://hdl.handle.net/20.500.11942/crtvwxhg9o329"/>
    <skos:narrower rdf:resource="https://hdl.handle.net/20.500.11942/crttwiuxkjhgb"/>
    <skos:narrower rdf:resource="https://hdl.handle.net/20.500.11942/crtrisqtt96fh"/>
    <skos:narrower rdf:resource="https://hdl.handle.net/20.500.11942/crt1wr3uhy7re"/>
    <skos:narrower rdf:resource="https://hdl.handle.net/20.500.11942/crtlanw91njyz"/>
    <skos:narrower rdf:resource="https://hdl.handle.net/20.500.11942/crtoedvlpqulf"/>
    <skos:narrower rdf:resource="https://hdl.handle.net/20.500.11942/crtgfszfb4b8z"/>
    <skos:narrower rdf:resource="https://hdl.handle.net/20.500.11942/crt6eu885msdr"/>
    <skos:narrower rdf:resource="https://hdl.handle.net/20.500.11942/crt7ssurotre4"/>
    <skos:inScheme rdf:resource="https://opentheso.huma-num.fr/opentheso/api/42./42"/>
    <opentheso:memberOf rdf:resource="https://opentheso.huma-num.fr/opentheso/api/42./MT62"/>
    <skos:exactMatch rdf:resource="http://vocab.getty.edu/page/aat/300008304"/>
    <dcterms:created rdf:datatype="http://www.w3.org/2001/XMLSchema#date">2018-11-12</dcterms:created>
    <dcterms:modified rdf:datatype="http://www.w3.org/2001/XMLSchema#date">2018-11-12</dcterms:modified>
    <dcterms:identifier>3221</dcterms:identifier>
    <dcterms:description>3221</dcterms:description>
</rdf:Description>

...

<rdf:Description rdf:about="https://hdl.handle.net/20.500.11942/crt1pv73yya17">
    <rdf:type rdf:resource="http://www.w3.org/2004/02/skos/core#Concept"/>
    <skos:prefLabel xml:lang="en">El-Gomhoreya Square</skos:prefLabel>
    <skos:prefLabel xml:lang="ala">maydān ʿĀbidīn</skos:prefLabel>
    <skos:prefLabel xml:lang="iso">maydān ʿĀbidīn</skos:prefLabel>
    <skos:prefLabel xml:lang="mul">mîdân ʿÂbdîn</skos:prefLabel>
    <skos:prefLabel xml:lang="fr">place d'Abdine</skos:prefLabel>
    <skos:prefLabel xml:lang="ar">ﻢﻳﺩﺎﻧ ﻉﺎﺑﺪﻴﻧ</skos:prefLabel>
    <skos:altLabel xml:lang="mul">Mîdân ʿAbdîn</skos:altLabel>
    <skos:altLabel xml:lang="fr">Place d'Abdin</skos:altLabel>
    <skos:altLabel xml:lang="fr">place d'Abdîn</skos:altLabel>
    <skos:altLabel xml:lang="fr">Place d'Abidin</skos:altLabel>
    <skos:broader rdf:resource="https://hdl.handle.net/20.500.11942/crtzryuppgd6o"/>
    <skos:inScheme rdf:resource="https://opentheso.huma-num.fr/opentheso/api/42./42"/>
    <opentheso:memberOf rdf:resource="https://opentheso.huma-num.fr/opentheso/api/42./MT62"/>
    <skos:exactMatch rdf:resource="https://www.geonames.org/7922574/maydan-abidin.html"/>
    <dcterms:created rdf:datatype="http://www.w3.org/2001/XMLSchema#date">2019-07-15</dcterms:created>
    <dcterms:modified rdf:datatype="http://www.w3.org/2001/XMLSchema#date">2019-07-15</dcterms:modified>
    <dcterms:identifier>28775</dcterms:identifier>
    <dcterms:description>3221##28775</dcterms:description>
    <skos:note xml:lang="fr">https://gallica.bnf.fr/iiif/ark:/12148/btv1b10546945n/f12/1002.0000000000018,288.05434514983426,3195.6033994334275,2265.42776203966/3196,2266/0/native.jpg | https://gallica.bnf.fr/iiif/ark:/12148/btv1b10546945n/f10/1019.7321428571438,3040.4713924952925,2718.9285714285716,1938.7142857142858/2719,1939/0/native.jpg</skos:note>
    <geo:lat rdf:datatype="http://www.w3.org/2001/XMLSchema#double">30.04372</geo:lat>
    <geo:long rdf:datatype="http://www.w3.org/2001/XMLSchema#double">31.24554</geo:long>
</rdf:Description>

...
```

Certains de concepts, comme `architecture civile` contiennent des concepts
plus précis comme `bâtiments officiels` et ainsi de suite jusqu'à trouver des
objets géo-localisés.

Le thésaurus relie ces informations vers d'autres informations ayant un sens
plus large, plus restreint, équivalent, référencées ailleurs (BNF, Persée)
avec des annotations.

![Hiérarchie des concepts](img/MT62_42.png)

L'image ci-dessus est obtenue en requêtant le service sur `/concepts/42` en
mode développeur. Le service génère un fichier `cache/MT62_42.dot` qu'il convient
de fournir au logiciel `dot` de l'éco-système `graphviz` pour calculer l'image :

```bash
$ dot ./cache/MT62_42.dot -Tpng -o ./docs/docs/img/MT62_42.png
$ rm -f ./cache/MT62_42.dot
```

La génération de ce fichier devrait s'effectuer sur une base régulière (e.g.
lors des mises-à-jour des `skos:Concept`) pour bien représenter la hiérarchie
sémantique des objets liés.

