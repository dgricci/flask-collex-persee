Collex-Persée - Accès au Thésaurus
==================================


Introduction
------------

Cette application permet de télécharger le thésaurus en ligne sous
[OpenTheso des Humanités numériques](https://opentheso.huma-num.fr/) via son
[API](https://opentheso.huma-num.fr/opentheso/api/application.wadl) pour le
stocker localement et l'interroger en SPARQL via [RdfLib](https://rdflib.readthedocs.io/en/stable/) ou le mettre à
disposition d'autres services (extension QGIS, application web).

Le thésaurus ainsi récupéré est un fichier RDF-XML.


Dépendances
-----------

Le développement de cette application web repose sur
[Python](https://www.python.org/) et le framework
[Flask](https://pypi.org/project/Flask/).

Il nécessite un gestionnaire de modules, c'est
[PipEnv](https://pipenv.kennethreitz.org/en/latest/) qui a été choisi car
regroupe les fonctionnalités de `npm` ou `yarn` en permettant de tout
regrouper sous une seule commande au lieu de jongler avec `pip`, `virtualenv`,
`setuptools`, etc ...
[Pip](https://pip.pypa.io/en/stable/) est nécessaire pour l'installation de
`PipEnv`.

L'utilisation de `RdfLib` se limite à interroger en mémoire le thésaurus mis
en cache local.

D'autres bibliothèques sont nécessaires au fonctionnement de l'application ;
elles sont listées et donc installées via `PipEnv` automatiquement.

### Développement ###

Le développement utilise aussi [Make](https://www.gnu.org/software/make/) pour
construire, tester, documenter et déployer le service.

L'installation des composants nécessaires au développement a été conduit
selon le modèle ci-dessous, sachant que `python` et `pip` sont pré-installés :

* installation de `pipenv` :

```bash
$ pip install --user pipenv
```

   `pipenv` est ainsi installé dans `${HOME}/.local/bin/pipenv`.
   Il faut alors ajouter `pipenv` à `${PATH}` via `${HOME}/.bashrc` :

```bash
$ cat <<-EOF >> ${HOME}/.bashrc

# pipenv
USERBASE=$HOME/.local/bin
if [ ! -z "$PATH" ] ; then
    if [ `echo $PATH | grep -c "$USERBASE"` -eq 0 ] ;  then
        export PATH=${PATH}:$USERBASE
    fi
else
    export PATH=${PATH}:$USERBASE
fi
unset USERBASE
eval "$(pipenv --completion)"
EOF
```

   Si une mise-à-jour est nécessaire, alors procéder ainsi :

```bash
$ pip install --user --upgrade pipenv
```

* création de l'environnement local au répertoire de développement :

```bash
$ PIPENV_VENV_IN_PROJECT=1 pipenv --three
Creating a virtualenv for this project…
Pipfile: flask_project/Pipfile
Using /usr/bin/python3.6m (3.6.8) to create virtualenv…
⠏ Creating virtual environment...Already using interpreter /usr/bin/python3.6m
Using base prefix '/usr'
New python executable in flask_project/.venv/bin/python3.6m
Also creating executable in flask_project/.venv/bin/python
Installing setuptools, pip, wheel...
done.
Running virtualenv with interpreter /usr/bin/python3.6m

✔ Successfully created virtual environment! 
Virtualenv location: $HOME/path/to/flask_project/.venv
```

* exemple d'installation d'un composant (e.g. Flask) :

```bash
$ pipenv install Flask
Installing Flask…
Adding Flask to Pipfile's [packages]…
✔ Installation Succeeded 
Pipfile.lock (512445) out of date, updating to (fdaeb1)…
Locking [dev-packages] dependencies…
Locking [packages] dependencies…
✔ Success! 
Updated Pipfile.lock (512445)!
Installing dependencies from Pipfile.lock (512445)…
  🐍   ▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉ 6/6 — 00:00:01
To activate this project's virtualenv, run pipenv shell.
Alternatively, run a command inside the virtualenv with pipenv run.
```

Le code de l'application est disponible [là](https://gitlab.com/dgricci/flask-collex-persee).
Pour développer en local, il faut
[cloner](https://gitlab.com/dgricci/flask-collex-persee.git) ce dépôt dans le
répertoire de développement.

Une fois cloné, il faut déployer les paquets nécessaires via la commande :

```bash
$ pipenv sync
```

### Documentation ###

Enfin, [MkDocs](https://github.com/mkdocs/mkdocs/) permet de générer cette documentation.

L'activation de l'extension `codehilite` nécessite de générer la CSS associée
via l'installation de `pygments`, puis la génération de la feuille de styles :

```bash
$ pipenv install pygments
Installing pygments…
Adding pygments to Pipfile's [packages]…
✔ Installation Succeeded 
Pipfile.lock (56f4b3) out of date, updating to (e9a30b)…
Locking [dev-packages] dependencies…
Locking [packages] dependencies…
✔ Success! 
Updated Pipfile.lock (56f4b3)!
Installing dependencies from Pipfile.lock (56f4b3)…
  🐍   ▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉ 31/31 — 00:00:02
To activate this project's virtualenv, run pipenv shell.
Alternatively, run a command inside the virtualenv with pipenv run.
$ mkdir docs/docs/css
$ pipenv run pygmentize -S default -f html -a .codehilite > docs/docs/css/codehilite.css
Loading .env environment variables…
```

L'ajout de l'extension `mkautodoc` nécessite son installation :

```bash
pipenv install mkautodoc
Installing mkautodoc…
Adding mkautodoc to Pipfile's [packages]…
✔ Installation Succeeded 
Pipfile.lock (04b974) out of date, updating to (56f4b3)…
Locking [dev-packages] dependencies…
Locking [packages] dependencies…
✔ Success! 
Updated Pipfile.lock (04b974)!
Installing dependencies from Pipfile.lock (04b974)…
  🐍   ▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉ 32/32 — 00:00:03
To activate this project's virtualenv, run pipenv shell.
Alternatively, run a command inside the virtualenv with pipenv run.
```

Le logiciel [WeasyPrint](https://weasyprint.org/) et l'extension
[pdf-export](https://github.com/zhaoterryy/mkdocs-pdf-export-plugin) pour
`mkdocs` sont utiles pour générer une documentation en PDF du projet :

```bash
$ pipenv install weasyprint
Installing weasyprint…
Adding weasyprint to Pipfile's [packages]…
✔ Installation Succeeded 
Pipfile.lock (5949ef) out of date, updating to (04b974)…
Locking [dev-packages] dependencies…
Locking [packages] dependencies…
✔ Success! 
Updated Pipfile.lock (5949ef)!
Installing dependencies from Pipfile.lock (5949ef)…
  🐍   ▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉ 43/43 — 00:00:04
To activate this project's virtualenv, run pipenv shell.
Alternatively, run a command inside the virtualenv with pipenv run.
```

```bash
$ pipenv install mkdocs-pdf-export-plugin
Installing mkdocs-pdf-export-plugin…
Adding mkdocs-pdf-export-plugin to Pipfile's [packages]…
✔ Installation Succeeded 
Pipfile.lock (5df78d) out of date, updating to (5949ef)…
Locking [dev-packages] dependencies…
Locking [packages] dependencies…
✔ Success! 
Updated Pipfile.lock (5df78d)!
Installing dependencies from Pipfile.lock (5df78d)…
  🐍   ▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉▉ 46/46 — 00:00:04
To activate this project's virtualenv, run pipenv shell.
Alternatively, run a command inside the virtualenv with pipenv run.
```

Pour générer la document avec le PDF associé (cela peut prendre un peu de
temps) :

```bash
$ make build-doc-with-pdf
Loading .env environment variables…
Combined PDF export is enabled
INFO    -  Cleaning site directory 
INFO    -  Building documentation to directory: PROJECT_PATH/flask-collex-persee/docs/site 
...
Converting 10 files to PDF took 22.8s
```

Développement
-------------

Pour lancer le service en mode développeur sur la machine locale, il suffit
sur une console d'entrer :

```sh
$ make run
```

Le service sera alors disponible ici : <http://127.0.0.1:3000/>

Le port (par défaut `3000`) peut être modifié via l'un des fichiers de
configuration trouvés dans `conf/{development|testing|production}/config.json`
en instanciant la variable `SERVER_NAME` :

```python
...
SERVER_NAME = '127.0.0.1:6000'
...
```

ici, le port est affecté à la valeur `6000`, le service sera alors joignable
via : <http://127.0.0.1:6000/>

Il est possible de lancer le service en mode développeur ainsi :

```bash
$ make -DFLASK_CONFIGURATION=development run
```

Pour avoir plus de traces applicatives, il suffit, toujours en mode
développeur d'entrer :

```bash
$ make run-debug
```

Enfin, le service peut être testé en mode production via :

```bash
$ make -DFLASK_CONFIGURATION=production run
```


Construction (build)
--------------------

Pour packager le service, il faut lancer la commande suivante :

```bash
$ make package
```

Le résultat du build sera disponible dans le dossier `./` sous la forme d'une
archive datée `flask-collex-persee-YYYY-MM-DDThh-mm-ss.sssZ.tgz`.

L'archive finale sera prête pour un déploiement. Elle est structurée ainsi :

```text
.
├── .env
├── application
│   ├── errors
│   │   ├── handlers.py
│   │   ├── __init__.py
│   │   ├── responses.py
│   │   └── templates
│   │       └── exception.json
│   ├── __init__.py
│   ├── main
│   │   ├── __init__.py
│   │   ├── routes.py
│   │   └── templates
│   │       └── service_api.json
│   └── xmlrdf
│       ├── __init__.py
│       ├── rdfparser.py
│       └── xmltogeojson.py
├── cache
├── conf
│   ├── development
│   │   ├── config.json
│   │   └── thesaurii.json
│   ├── production
│   │   ├── config.json
│   │   └── thesaurii.json
│   └── testing
│       ├── config.json
│       └── thesaurii.json
├── config.py
├── logs
├── Pipfile
├── Pipfile.lock
└── rdfservice.py
```


Exécution
---------

Sur la machine de développement, les cibles suivantes permettent d'exécuter
l'application sur `127.0.0.1:3000` :

* `make run` : lance l'application en configuration développement ;
* `make run-debug` : lance l'application en configuration développement et mode DEBUG ;
* `make test` : lance les tests en configuration de test et mode DEBUG ;
* `make run-as-prod` : lance l'application en configuration de production.


Documentation
-------------

Pour visualiser l'ensemble de la documentation sur la machine de développement :

```bash
$ make build-doc
```

La documentation sera disponible en ouvrant via le butineur le fichier `site/index.html`.
Un autre moyen est de lancer le serveur web associé à `mkdocs` :

```bash
$ make serve-doc
```

La documentation sera alors accessible ici : <http://127.0.0.1:3001/>

Création d'une VM
-----------------

Si la [documentation](./vm0/) montre comment créer à la main une machine virtuelle pour
faire fonctionner l'application (en 3 étapes), le dossier `ansible` contient
les éléments pour créer cette machine virtuelle par script permettant de
mettre en oeuvre cette application :

```txt
$ tree ./ansible/
./ansible/
├── ansible.cfg
├── bin
│   ├── deployapp.sh
│   └── deploydoc.sh
├── collex-appdoc.yml
├── collex-app.yml
├── collex-base.yml
├── collex-qgisserver.yml
├── collex.yml
├── etc
│   ├── apt
│   │   └── apt.conf.d
│   │       ├── 10periodic
│   │       ├── 20auto-upgrades
│   │       └── 50unattended-upgrades
│   ├── nginx
│   │   └── sites-available
│   │       └── webgis.collexpersee.fr
│   └── systemd
│       └── system
│           ├── collexperseeapp.path.j2
│           ├── collexperseeapp.service.j2
│           ├── collexperseedoc.path.j2
│           ├── collexperseedoc.service.j2
│           ├── collexperseeflaskapp.path
│           ├── collexperseeflaskapp.service
│           ├── qgisserver-fcgi@.service
│           ├── qgisserver-fcgi@.socket
│           └── xvfb.service
├── host_vars
│   ├── hostnames.yml
│   ├── passwords.yml
│   └── users.yml
├── inventory
├── Vagrantfile
├── vagrantinventory
├── var
│   └── www
│       └── html
│           ├── 40x.html
│           ├── 50x.html
│           └── projects
│               └── collex-persee
│                   └── collex-persee.qgs
├── vm-cairmod-ansible.sh
├── vm-cairmod-create.sh
├── vm-cairmod-remove.sh
├── vm-cairmod-start.sh
└── vm-cairmod-stop.sh

14 directories, 35 files
```

Il faut donc installer [Ansible](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html) sur sa machine de devéloppement pour
automatiser cette création.

Pour créer la machine, il suffit de lancer le script `./vm-cairmod-create.sh` :

```bash
$ ./vm-cairmod-create.sh 
Bringing machine 'CollexPersee' up with 'virtualbox' provider...
==> CollexPersee: Importing base box 'debian/buster64'...
==> CollexPersee: Matching MAC address for NAT networking...
==> CollexPersee: Checking if box 'debian/buster64' version '10.3.0' is up to date...
==> CollexPersee: Setting the name of the VM: CairModAP
==> CollexPersee: Clearing any previously set network interfaces...
==> CollexPersee: Preparing network interfaces based on configuration...
    CollexPersee: Adapter 1: nat
==> CollexPersee: Forwarding ports...
    CollexPersee: 22 (guest) => 2222 (host) (adapter 1)
==> CollexPersee: Running 'pre-boot' VM customizations...
==> CollexPersee: Booting VM...
==> CollexPersee: Waiting for machine to boot. This may take a few minutes...
    CollexPersee: SSH address: 127.0.0.1:2222
    CollexPersee: SSH username: vagrant
    CollexPersee: SSH auth method: private key
    CollexPersee: 
    CollexPersee: Vagrant insecure key detected. Vagrant will automatically replace
    CollexPersee: this with a newly generated keypair for better security.
    CollexPersee: 
    CollexPersee: Inserting generated public key within guest...
    CollexPersee: Removing insecure key from the guest if it's present...
    CollexPersee: Key inserted! Disconnecting and reconnecting using new SSH key...
==> CollexPersee: Machine booted and ready!
==> CollexPersee: Checking for guest additions in VM...
==> CollexPersee: Running provisioner: shell...
    CollexPersee: Running: inline script

==> CollexPersee: Machine 'CollexPersee' has a post `vagrant up` message. This is a message
==> CollexPersee: from the creator of the Vagrantfile, and not from Vagrant itself:
==> CollexPersee: 
==> CollexPersee: Vanilla Debian box. See https://app.vagrantup.com/debian for help and bug reports

PLAY [all] ***************************************************************************************************************************************************

...

TASK [debug] *************************************************************************************************************************************************
ok: [CollexPersee] => {
    "msg": [
        "server at 10.0.2.15 set !", 
        "Don't forget to remove vagrant user from the VM by running :", 
        "ANSIBLE_HOST_KEY_CHECKING=false ansible all -i inventory -b -K -u invisu -k -m user -a 'name=vagrant state=absent remove=yes'"
    ]
}

RUNNING HANDLER [restart sshd] *******************************************************************************************************************************
changed: [CollexPersee]

RUNNING HANDLER [restart nginx] ******************************************************************************************************************************
changed: [CollexPersee]

PLAY RECAP ***************************************************************************************************************************************************
CollexPersee               : ok=51   changed=43   unreachable=0    failed=0    skipped=1    rescued=0    ignored=0   

```

Le script se termine en rappelant de détruire le compte `vagrant` :

```bash
$ ANSIBLE_HOST_KEY_CHECKING=false ansible all -i inventory -b -K -u invisu -k -m user -a 'name=vagrant state=absent remove=yes'
SSH password: 
BECOME password[defaults to SSH password]: 
[WARNING]: Platform linux on host CollexPersee is using the discovered Python interpreter at /usr/bin/python, but future installation of another Python
interpreter could change this. See https://docs.ansible.com/ansible/2.9/reference_appendices/interpreter_discovery.html for more information.
CollexPersee | CHANGED => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python"
    }, 
    "changed": true, 
    "force": false, 
    "name": "vagrant", 
    "remove": true, 
    "state": "absent", 
    "stderr": "userdel: vagrant mail spool (/var/mail/vagrant) not found\n", 
    "stderr_lines": [
        "userdel: vagrant mail spool (/var/mail/vagrant) not found"
    ]
}
```

La machine virtuelle est prête à l'utilisation !

