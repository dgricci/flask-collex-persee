# Copyright 2019-2019 Didier Richard
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# Print a * :
S = $(shell printf "*")
# Print two spaces :
T = $(shell printf "  ")

PYTHON              := python3
FLASK_APP           := rdfservice.py
FLASK_DEBUG         ?= 0
FLASK_CONFIGURATION ?= "default"

export FLASK_APP
export FLASK_DEBUG
export FLASK_CONFIGURATION

# Default target (first target not starting with .)
run:
	@pipenv run $(PYTHON) $(FLASK_APP)

run-debug:
	@ FLASK_DEBUG=1 pipenv run $(PYTHON) $(FLASK_APP)

# Use same server name (or IP) and same server port as in config.py or
# subsequent configurations ...
run-as-prod:
	@ S0=$$(grep "SERVER_NAME" config.py | head -n 1 | tr -d [\"\'] | cut -d: -f1 | cut -d= -f2 | tr -d ' ') ; \
		S1=$$(grep "SERVER_NAME" conf/production/config.json | tr -d [\"\'] | cut -d: -f2 | tr -d ' ') ; \
		S=$${S1:-$${S0}} ; \
		P0=$$(grep "SERVER_NAME" config.py | head -n 1 | tr -d [\"\'] | cut -d: -f2 | tr -d ' ') ; \
		P1=$$(grep "SERVER_NAME" conf/production/config.json | tr -d [\"\'] | cut -d: -f3) ; \
		P=$${P1:-$${P0}} ; \
		FLASK_CONFIGURATION=production .venv/bin/gunicorn -b $${S}:$${P} -w 4 rdfservice:app

build-doc:
	@ PYTHONPATH=${PWD} pipenv run mkdocs build --clean -f ./docs/mkdocs.yml

build-doc-with-pdf:
	@ PYTHONPATH=${PWD} ENABLE_PDF_EXPORT=1 pipenv run mkdocs build --clean -f ./docs/mkdocs.yml

serve-doc:
	@pipenv run mkdocs serve -f ./docs/mkdocs.yml --dev-addr=localhost:3001 --livereload

test:
	@ FLASK_CONFIGURATION=testing pipenv run $(PYTHON) -m unittest discover tests

clean: clean-pic clean-log clean-cache clean-site

clean-pic:
	@for d in $$(find . -type d -name "__pycache__" | grep -v -e '^./.venv') ; do rm -fr $${d} ; done

clean-log:
	@rm -f ./logs/*

clean-cache:
	@rm -f ./cache/*.xml

clean-site:
	@rm -fr ./docs/site/

ifeq ($(shell test -d "$(PROD_CONFIG)" && printf "1"),1)
package:
	@$(MAKE) clean-pic
	@now=$$(date -u +"%FT%H-%M-%S.%NZ") ; \
	pn="./flask-collex-persee-$${now}" ; \
	tar --anchored --no-wildcards-match-slash --exclude-from=excludes.txt \
		-cf $${pn}.tar \
		./.env ./application ./cache ./config.py ./logs \
		Pipfile* rdfservice.py ; \
	tar --transform='flags=r;s!^$(PROD_CONFIG)!./conf/production/!' \
		--anchored --no-wildcards-match-slash \
		-rf $${pn}.tar \
		$(PROD_CONFIG)*.json ; \
	gzip $${pn}.tar ; \
	mv $${pn}.tar.gz $${pn}.tgz ; \
	echo "$${pn}.tgz done !"
else
package:
	@$(MAKE) clean-pic
	@now=$$(date -u +"%FT%H-%M-%S.%NZ") ; \
    pn="./flask-collex-persee-$${now}.tgz" ; \
	tar --anchored --no-wildcards-match-slash --exclude-from=excludes.txt \
		-czf $${pn} \
		./.env ./application ./cache ./conf/production ./config.py ./logs \
		Pipfile* rdfservice.py ; \
	echo "$${pn} done !"
endif

package-doc:
	@$(MAKE) clean-site build-doc
	@now=$$(date -u +"%FT%H-%M-%S.%NZ") ; \
	pn="./htmlsite-collex-persee-$${now}.tgz" ; \
	tar -czf $${pn} \
		--directory=./docs/site . ; \
	echo "$${pn} done !"

help:
	$(info $(S) to run this service in development mode:)
	$(info $(T)$$ make [FLASK_CONFIGURATION=default|development|testing|production] run)
	$(info $(S) to run this service in development mode with debug:)
	$(info $(T)$$ make [FLASK_CONFIGURATION=default|development|testing|production] run-debug)
	$(info $(S) to run this service in production mode:)
	$(info $(T)$$ make run-as-prod)
	$(info $(S) to run unit tests:)
	$(info $(T)$$ make test)
	$(info $(S) to build the documentation:)
	$(info $(T)$$ make build-doc)
	$(info $(S) to build the documentation with a combined pdf:)
	$(info $(T)$$ make build-doc-with-pdf)
	$(info $(S) to serve on localhost:3001 the documentation:)
	$(info $(T)$$ make serve-doc)
	$(info $(S) to clean things:)
	$(info $(T)$(S) to remove logs:)
	$(info $(T)$(T)$$ make clean-log)
	$(info $(T)$(S) to remove thesaurus in cache:)
	$(info $(T)$(T)$$ make clean-cache)
	$(info $(T)$(S) to remove python-compiled files:)
	$(info $(T)$(T)$$ make clean-pic)
	$(info $(T)$(S) to remove built site:)
	$(info $(T)$(T)$$ make clean-site)
	$(info $(T)$(S) to clean everything, included documentation site, logs:)
	$(info $(T)$(T)$$ make clean)
	$(info $(S) to package the application :)
	$(info $(T)$(S) Flask application for delivery :)
	$(info $(T)$(T)$$ make package)
	$(info $(T)$(T)$(S) Flask application for delivery with specific production configuration in a DIR containing config.json and thesaurii.json files:)
	$(info $(T)$(T)$(T)$$ make PROD_CONFIG=<DIR>/ package)
	$(info $(T)$(T)$(T)$(T)Use ./var as base directory as it is excluded via ./gitignore)
	$(info $(T)$(T)$(T)$(T)Example 1: `$$ make PROD_CONFIG=./var/default/ package` for a default production configuration, e.g. local VM;)
	$(info $(T)$(T)$(T)$(T)Example 2: `$$ make PROD_CONFIG=./var/cairmod.ensg.eu/ package` for a specific production configuration due to proxy settings;)
	$(info $(T)$(S) HTML Documentation of the application :)
	$(info $(T)$(T)$$ make package-doc)

# always execute these targets when invoked
.PHONY: run test

