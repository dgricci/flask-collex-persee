#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os

class Config(object):
    # application base directory :
    BASE_DIR = os.path.abspath(os.path.dirname(__file__))
    # in case ENV is not set, use default parameter ...
    # in case ENV is set to '', use "or" too !
    ENV = os.environ.get('ENV', default="development") or "development"
    DEBUG = False
    TESTING = False
    SERVICE_NAME = 'rdf-service'
    # put your secret key in .env or .flaskenv
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'the-show-must-go-on!'
    # relative to BASEDIR
    CONF_DIR = './conf'
    # relative to BASEDIR
    CACHE_DIR = './cache'
    SERVER_NAME = '127.0.0.1:3000'
    # relative to BASEDIR
    LOG_DIR = './logs'
    # proxy (example of config)
    #PROXIES = { "http":"http://10.0.4.2:3128/", "https":"https://10.0.4.2:3128" }
    PUBLIC_HOST_URL = 'http://' + SERVER_NAME
    PUBLIC_BASE_APP = '/'

class DevelopmentConfig(Config):
    DEBUG = True

class TestingConfig(DevelopmentConfig):
    TESTING = True

class ProductionConfig(Config):
    ENV = "production"
    CACHE_DIR = '/var/cache/collex'
    LOG_DIR = '/var/log/collex'

