CairMod - Invisu - Collex-Persée - Accès au Thésaurus
=====================================================

Contexte
--------

Unité de service et de recherche du CNRS, le laboratoire InVisu conduit une
réflexion sur l'adaptation de l'histoire de l'art à l'ère du numérique et
s'attache à la reconstitution de corpus visuels et textuels dispersés. Son
activité s'articule autour de trois pôles que sont l'accompagnement aux
humanités numériques, l'expérimentation d'outils pour le partage de données
comprenant la mise à disposition de contenus numériques, et le conseil en
matière de traitement numérique des données. Le terrain d'application de ses
recherches est les arts monumentaux et les arts visuels en Méditerranée
contemporaine. Parmi ses réalisations, InVisu a mené à bien le projet Athar
(BSN5 2014) en partenariat avec Persée et l’Ifao. Ce projet a permis de
numériser et de mettre en ligne l’intégralité des bulletins du Comité de
conservation des monuments de l’art arabe enrichis par un référentiel des
toponymes du Caire. Ce projet a donné naissance à la plateforme éponyme
(Perséide Athar). Ce [site web](https://athar.persee.fr/) en Open Access est
basé sur les technologies du linked open data, développé par Persée offrant
des outils et des services d’exploration des contenus à forte valeur ajoutée.
Dans le domaine du traitement de corpus visuels, le laboratoire a élaboré un
catalogue raisonné de l’œuvre du photographe du Caire,
[Facchinelli](http://facchinelli.huma-num.fr/), disponible
en ligne avec des données enrichies, qui permettent d’étudier la géographie
patrimoniale de la ville.


Travaux
-------

![OpenTheso](./docs/docs/img/logo-OpenTheso.png)

Ce service permet d'accèder à un thésaurus sous
[OpenTheso](https://github.com/miledrousset/opentheso/wiki)
et fournit quelques méthodes pour requêter la copie locale du RDF-XML pour
fabriquer des objets géographiques sous format `GeoJSON`.

Il a été développé via le projet ANR **CollEx-Persée CAIRMOD** par des étudiants de la
promotion 2018-2019 du parcours TSI de l'ENSG (3ième année de l'école
d'ingénieurs et Master 2 co-accrédité par l'Université Paris Est -
Marne-La-Vallée), étudiants pilotés par l'équipe projet InVisu de l'Inha.

### Étudiants ###

* Antoine Fondère
* Corentin Crapart
* Erwan Viegas
* Max Permalnaick
* Sinda Thaalbi
* Tristan Harling
* Wiltold Podlejski

Encadrement technique et pédagogique : Didier Richard (IGN/ENSG)

### InVisu - Collex-Persée ###

* Bulle Tuil Leonetti - Responsable du projet, Ingénieur de recherche en analyse des sources - InVisu (CNRS/INHA)
* Julie Erismann - Géographe, Ingénieur de recherche - InVisu (CNRS/INHA)
* Pierre Mounier - Développeur et intégrateur d’applications, Ingénieur d'étude - InVisu (CNRS/INHA)
* Hélène Bégnis - Coordination du projet pour Persée - Chargée des partenariats recherche
* ...

